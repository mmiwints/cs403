// import React, { useState } from 'react';
// import { Image, Button, Input, Col,Upload} from 'antd';
// import { UploadOutlined } from '@ant-design/icons';
// import { Link } from 'react-router-dom';

// class MyUpload extends React.Component {
//     state = {
//         fileList: [],
//     };

//     handleChange = info => {
//         let fileList = [...info.fileList];

//         // 1. Limit the number of uploaded files
//         // Only to show two recent uploaded files, and old ones will be replaced by the new
//         fileList = fileList.slice(-2);

//         // 2. Read from response and show file link
//         fileList = fileList.map(file => {
//             if (file.response) {
//                 // Component will show file.url as link
//                 file.url = file.response.url;
//             }
//             return file;
//         });

//         this.setState({ fileList });
//     };

//     render() {
//         const props = {
//             action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
//             onChange: this.handleChange,
//             multiple: true,
//         };
//         return (
//             <Upload {...props} fileList={this.state.fileList}>
//                 <Button style={{ width: 280, }} icon={<UploadOutlined />}>Upload</Button>
//             </Upload>
//         );
//     }
// }


// const { TextArea } = Input;
// function item() {
//     return (

//         <div className="itempage">
//             <p className="itemlist-headtext" >รายการขอเพิ่มพัสดุ</p>

//             <div className="item-date-in">
//                 <span>วันที่ขอเพิ่ม : </span> <span>12 มีนาคม 2564</span>
//             </div>
//             <br></br>
//             <div className="itemgrid-confirm">

//                 <div>
//                     <span>ผู้ขอเพิ่มพัสดุ : </span> <span>ธนาภรณ์ ด่านชาญชัย</span>
//                 </div>
//                 <div />

//                 <div>
//                     <span>ตำแหน่ง : </span> <span>บุคลากร</span>
//                 </div>

//                 <div>
//                     <span>เบอร์ติดต่อ : </span> <span>0812345678</span>
//                 </div>

//                 </div>

//                 <br/>

//                 <div className="itemgrid-confirm">


//                 <div>
//                     <span>รายการพัสดุ : </span> <span>กระดาษกาว 1 นิ้ว (สีขาว)</span>
//                 </div>
//                 <div />

//                 <div>
//                     <span>จำนวนที่เบิก : </span> <span>45</span>
//                 </div>

//                 <div className="upimageapprize2">
//                     <span>จำนวนที่จ่าย : </span>  <Col span={10}>
//                         <Input />
//                     </Col>
//                 </div>

               
//             </div>
//             <br />

//             <div >
//                 <p>รูปพัสดุ:</p>
//                 <MyUpload />
//             </div>
//             <br />

//             <div >
//                 <span>หมายเหตุ: </span>
//                 <Col span={8}><TextArea placeholder=""
//                     autoSize={{ minRows: 3, maxRows: 7 }} ></TextArea> </Col>
//             </div>
//             <br />
//             <br/>

//             <div className="spacebotton">

//                 <Link to="/RequistionConfirmFiscalOfficer" ><Button className="buttongreen">อัพเดตสถานะพัสดุ</Button></Link>
//                 <div />


//             </div>

//         </div>
//     )
// }

// export default item;