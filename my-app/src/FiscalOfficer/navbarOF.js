import React from 'react';

import { Link } from 'react-router-dom';
import { Menu, Switch, Divider, Image } from 'antd';
import {
  MailOutlined,
  CalendarOutlined,
  AppstoreOutlined,
  SettingOutlined,
  LinkOutlined,
} from '@ant-design/icons';

const { SubMenu } = Menu;



function navbarOF() {
  return (
    <div className="navbarOF">

      {/* <div className="logonavbar">
        <span ><Image
          width={20}
          src="https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png"
        />  CS.</span><span className="navbarnametext">ABCD</span>
      </div> */}

      <p className="menutext">เมนู</p>

      <Menu style={{ width: 256 }} mode="inline"  >
        {/* <Menu.Item key="1" >
          หน้าหลัก
        </Menu.Item>

        <SubMenu key="sub1" icon={<MailOutlined />} title="พัสดุ">
          <Menu.Item key="1">รายการพัสดุ</Menu.Item>
          <Menu.Item key="2">Option 2</Menu.Item>
          <Menu.Item key="3">Option 3</Menu.Item>
          <Menu.Item key="4">Option 4</Menu.Item>
        </SubMenu> */}



        <Menu.Item key="homepage" ><Link to="/DashboardFiscalOfficer">หน้าหลัก</Link></Menu.Item>

        <Menu.Item key="requistion" ><Link to="/RequistionFiscalOfficer">รายการขอเบิก</Link></Menu.Item>

        <SubMenu key="subItem" title="พัสดุ" icon={<svg xmlns="http://www.w3.org/2000/svg" width="18.655" height="18.283" viewBox="0 0 18.655 18.283">
          <g id="Iconly_Light_Buy" data-name="Iconly/Light/Buy" transform="translate(-1.883 -2.383)">
            <g id="Buy" transform="translate(2.75 3.25)">
              <path id="Stroke_1" data-name="Stroke 1" d="M.7,0A.7.7,0,1,1,0,.7.705.705,0,0,1,.7,0Z" transform="translate(3.599 15.258)" fill="none" stroke="#4b4a4a" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="1.5" />
              <path id="Stroke_3" data-name="Stroke 3" d="M.7,0A.7.7,0,1,1,0,.7.705.705,0,0,1,.7,0Z" transform="translate(13.959 15.258)" fill="none" stroke="#4b4a4a" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="1.5" />
              <path id="Stroke_5" data-name="Stroke 5" d="M0,0,1.915.331,2.8,10.9a1.66,1.66,0,0,0,1.655,1.522H14.5a1.661,1.661,0,0,0,1.644-1.424l.874-6.039A1.235,1.235,0,0,0,15.8,3.544H2.223" transform="translate(0 0)" fill="none" stroke="#4b4a4a" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="1.5" />
              <path id="Stroke_7" data-name="Stroke 7" d="M0,.5H2.553" transform="translate(10.475 6.448)" fill="none" stroke="#4b4a4a" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="1.5" />
            </g>
          </g>
        </svg>
        }>

          <Menu.Item><Link to="/itemlist">รายการพัสดุ</Link>  </Menu.Item>

          <Menu.Item><Link to="/itemout">พัสดุหมด</Link></Menu.Item>

          <Menu.Item><Link to="/itemAlmostout">พัสดุใกล้หมด</Link></Menu.Item>

          <Menu.Item><Link to="/itemMotionless">พัสดุไม่เคลื่อนไหว</Link></Menu.Item>

          <Menu.Item><Link to="/itemDeteriorate">พัสดุเสื่อมสภาพ</Link></Menu.Item>

        </SubMenu>


        <SubMenu key="subHistory" title="ประวัติ" icon={<svg xmlns="http://www.w3.org/2000/svg" width="18.352" height="18.166" viewBox="0 0 18.352 18.166">
          <g id="Iconly_Light_Add_User" data-name="Iconly/Light/Add User" transform="translate(-2 -2)">
            <g id="Add_User" data-name="Add User" transform="translate(2.75 2.75)">
              <path id="Stroke_1" data-name="Stroke 1" d="M6.492,0C2.991,0,0,.529,0,2.65S2.972,5.319,6.492,5.319c3.5,0,6.492-.53,6.492-2.65S10.013,0,6.492,0Z" transform="translate(0 11.347)" fill="none" stroke="#4b4a4a" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="1.5" />
              <path id="Stroke_3" data-name="Stroke 3" d="M4.161,8.322A4.161,4.161,0,1,0,0,4.161,4.147,4.147,0,0,0,4.161,8.322Z" transform="translate(2.331 0)" fill="none" stroke="#4b4a4a" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="1.5" />
              <path id="Stroke_5" data-name="Stroke 5" d="M.5,0V3.653" transform="translate(14.489 5.392)" fill="none" stroke="#4b4a4a" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="1.5" />
              <path id="Stroke_7" data-name="Stroke 7" d="M3.726.5H0" transform="translate(13.126 6.718)" fill="none" stroke="#4b4a4a" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="1.5" />
            </g>
          </g>
        </svg>
        }>

          <Menu.Item>ประวัติการเบิกพัสดุ</Menu.Item>

          <Menu.Item>ประวัติการรับเข้าพัสดุ</Menu.Item>

          <Menu.Item>ประวัติการขอเพิ่มพัสดุ</Menu.Item>

        </SubMenu>

        <Menu.Item key="report" icon={<svg xmlns="http://www.w3.org/2000/svg" width="16.586" height="18.166" viewBox="0 0 16.586 18.166">
          <g id="Iconly_Light_Document" data-name="Iconly/Light/Document" transform="translate(-3.001 -2)">
            <g id="Document" transform="translate(3.751 2.75)">
              <path id="Stroke_1" data-name="Stroke 1" d="M6.536.5H0" transform="translate(4.296 11.697)" fill="none" stroke="#4b4a4a" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="1.5" />
              <path id="Stroke_2" data-name="Stroke 2" d="M6.536.5H0" transform="translate(4.296 7.907)" fill="none" stroke="#4b4a4a" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="1.5" />
              <path id="Stroke_3" data-name="Stroke 3" d="M2.494.5H0" transform="translate(4.296 4.126)" fill="none" stroke="#4b4a4a" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="1.5" />
              <path id="Stroke_4" data-name="Stroke 4" d="M11.006,0,4.046,0A3.848,3.848,0,0,0,0,4.171V12.5a3.851,3.851,0,0,0,4.079,4.171l6.961,0A3.849,3.849,0,0,0,15.086,12.5V4.171A3.852,3.852,0,0,0,11.006,0Z" transform="translate(0)" fill="none" stroke="#4b4a4a" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="1.5" />
            </g>
          </g>
        </svg>
        }>รายงาน</Menu.Item>

        <Menu.Item key="manageUser" icon={<svg xmlns="http://www.w3.org/2000/svg" width="18.352" height="18.166" viewBox="0 0 18.352 18.166">
          <g id="Iconly_Light_Add_User" data-name="Iconly/Light/Add User" transform="translate(-2 -2)">
            <g id="Add_User" data-name="Add User" transform="translate(2.75 2.75)">
              <path id="Stroke_1" data-name="Stroke 1" d="M6.492,0C2.991,0,0,.529,0,2.65S2.972,5.319,6.492,5.319c3.5,0,6.492-.53,6.492-2.65S10.013,0,6.492,0Z" transform="translate(0 11.347)" fill="none" stroke="#4b4a4a" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="1.5" />
              <path id="Stroke_3" data-name="Stroke 3" d="M4.161,8.322A4.161,4.161,0,1,0,0,4.161,4.147,4.147,0,0,0,4.161,8.322Z" transform="translate(2.331 0)" fill="none" stroke="#4b4a4a" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="1.5" />
              <path id="Stroke_5" data-name="Stroke 5" d="M.5,0V3.653" transform="translate(14.489 5.392)" fill="none" stroke="#4b4a4a" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="1.5" />
              <path id="Stroke_7" data-name="Stroke 7" d="M3.726.5H0" transform="translate(13.126 6.718)" fill="none" stroke="#4b4a4a" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="1.5" />
            </g>
          </g>
        </svg>
        }>จัดการบัญชีผู้ใช้</Menu.Item>

        <Menu.Item key="logout" icon={<svg xmlns="http://www.w3.org/2000/svg" width="18.65" height="18.166" viewBox="0 0 18.65 18.166">
          <g id="Iconly_Light_Logout" data-name="Iconly/Light/Logout" transform="translate(-2.022 -2.021)">
            <g id="Logout" transform="translate(2.772 2.772)">
              <path id="Stroke_1" data-name="Stroke 1" d="M11.03,4.16V3.32A3.32,3.32,0,0,0,7.711,0H3.319A3.32,3.32,0,0,0,0,3.32V13.346a3.32,3.32,0,0,0,3.319,3.32h4.4a3.311,3.311,0,0,0,3.311-3.31v-.85" transform="translate(0)" fill="none" stroke="#4b4a4a" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="1.5" />
              <path id="Stroke_3" data-name="Stroke 3" d="M10.847.5H0" transform="translate(6.303 7.833)" fill="none" stroke="#4b4a4a" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="1.5" />
              <path id="Stroke_5" data-name="Stroke 5" d="M0,0,2.638,2.626,0,5.253" transform="translate(14.512 5.707)" fill="none" stroke="#4b4a4a" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="1.5" />
            </g>
          </g>
        </svg>
        }>ออกจากระบบ</Menu.Item>

      </Menu>
    </div>

  )
}
export default navbarOF;


