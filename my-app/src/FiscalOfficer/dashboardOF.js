import React, { useState } from 'react';
import { Button, Modal, Image, Table, Tag } from 'antd';

import tapewhite from '../component/Img/tapewhite.jpg';
import clips from '../component/Img/clips.jpg';
import lq from '../component/Img/liq.png';


import { Link } from 'react-router-dom';

const columns = [
    {
        title: 'รายการ',
        dataIndex: 'name',
        key: 'name',

    },

    {
        title: 'คงเหลือ',
        dataIndex: 'remain',
        key: 'remain',
    },

];

const columns2 = [
    {
        title: 'รายการ',
        dataIndex: 'name',
        key: 'name',
        className: 'table_head'

    },

    {
        title: 'คงเหลือ',
        dataIndex: 'remain',
        key: 'remain',
        className: 'table_head'
    },

];

const dataSource = [


    {
        key: '1',
        name: <div className="table_text">
            <img style={{ width: 35 }} src={tapewhite} />
            <div className="pad1">
                กระดาษกาว 1 นิ้ว (สีขาว)
        </div>
        </div>,

        remain: 0,

    },

    {
        key: '2',
        name: <div className="table_text">
            <img style={{ width: 35 }} src={clips} />
            <div className="pad1">

                ลวดเสียบกระดาษ
        </div>
        </div>,

        remain: 0,

    },

    {
        key: '3',
        name: <div className="table_text">
            <img style={{ width: 35 }} src={lq} />
            <div className="pad1">
                น้ำยาลบคำผิด (แบบแท่ง)
        </div>
        </div>,

        remain: 0,

    },


];

const dataSource2 = [

    {
        key: '1',
        name: 'ยางลบดินสอ',
        remain: 3,
    },

    {
        key: '2',
        name: 'หลอดไฟ LED (ขาว)',
        remain: 1,
    },

    {
        key: '3',
        name: 'แปลงลบกระดาน',
        /* <a className="tag_a">ST0023</a><br></br> */
        remain: 2,
    },

];

const columns3 = [
    {
        title: 'วันที่',
        dataIndex: 'date',
        key: 'date',
        className: 'table_head2'
    },

    {
        title: 'รายการ',
        dataIndex: 'name',
        key: 'name',
        className: 'table_head2'

    },



    {
        title: 'ชื่อผู้เบิก',
        dataIndex: 'staff',
        key: 'staff',
        className: 'table_head2'
    },

    {
        className: 'table_head2',
        title: 'สถานะ',
        key: 'tags',
        dataIndex: 'tags',
        render: tags => (
            <>
                {tags.map(tag => {
                    let color = tag.length > 6 ? 'volcano' : 'green';

                    return (
                        <Tag color={color} key={tag}>
                            {tag.toUpperCase()}
                        </Tag>
                    );
                })}
            </>
        ),
    },

];





const dataSource3 = [

    {
        key: '1',
        date: 'วันนี้',
        name:
            'ยางลบดินสอ',
        staff: 'ธนาภรณ์ ด่านชาญชัย',
        tags: ['รออนุมัติ'],
    },

    {
        key: '2',
        date: '20 เม.ย. 64',
        name:
            'กระดาษกาว  1  นิ้ว (สีขาว)',
        staff: 'รัตนากร จงรวยกลาง',
        tags: ['สำเร็จ'],
    },

    {
        key: '3',
        date: '25 เม.ย. 64',
        name:
            'ลวดเสียบกระดาษ',
        staff: 'ชนิกานต์ ลี้ผดุง',
        tags: ['สำเร็จ'],
    },

];



const Apphomepage = () => {
    const [visible, setVisible] = useState(false);


    return (

        <div className="Appmiw">
            <div className="Head1" >Dashboard</div>
            <br></br>



            <div className="dashbord-grid">

                <div className="DBitemout">
                    <div className="grid-DBitemout">
                        <h2 className="">พัสดุหมด</h2>
                        <Link to="/itemout" className="DBtext2">ดูทั้งหมด</Link>
                    </div>


                    <div className="itemtableout">
                        <Table dataSource={dataSource} columns={columns} pagination={false} />

                    </div>




                </div>  <div />

                <div className="DBitemalmost">
                    <div className="grid-DBitemalmout">
                        <h2 className="table_head">พัสดุใกล้หมด</h2>
                        <Link to="/itemAlmostout" className="DBtext2">ดูทั้งหมด</Link>
                    </div>

                    <div className="itemtableout">
                        <Table dataSource={dataSource2} columns={columns2} pagination={false} />

                    </div>


                </div>






            </div>

            <br></br>
            <br></br>
            <br></br>

            <div>
                <div className="grid-DBitemout">
                    <h2 className="DBtext3">รายการขอเพิ่มพัสดุ</h2>
                    <Link to="/FiscalRequisitionPage" className="DBtext2">ดูทั้งหมด</Link>
                    
                </div>

                <div className="itemtableout">
                    <Table dataSource={dataSource3} columns={columns3} pagination={false}/>;

            </div>
            </div>


        </div>
    )

}

export default Apphomepage;