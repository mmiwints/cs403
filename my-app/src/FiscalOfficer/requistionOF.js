import React, { useState } from 'react';
import { Button, Modal, Image, Table ,Tag} from 'antd';




import { Link } from 'react-router-dom';



const columns3 = [
    {
        title: 'วันที่',
        dataIndex: 'date',
        key: 'date',
    },

    {
        title: 'รายการ',
        dataIndex: 'name',
        key: 'name',

    },



    {
        title: 'ชื่อผู้เบิก',
        dataIndex: 'staff',
        key: 'staff'
    },

    {
        title: 'สถานะ',
        key: 'tags',
        dataIndex: 'tags',
        render: tags => (
            <>
                {tags.map(tag => {
                    let color = tag.length > 6 ? 'volcano' : 'green';

                    return (
                        <Tag color={color} key={tag}>
                            {tag.toUpperCase()}
                        </Tag>
                    );
                })}
            </>
        ),
    },

];


   

const dataSource3 = [

    {
        key: '1',
        date: '12 เม.ย. 64',
        name:<Link to="/RequistionUpdateFiscalOfficer">
             <div className="textcolorblack">
            ยางลบดินสอ</div></Link>,
        staff: 'ธนาภรณ์ ด่านชาญชัย',
        tags: ['รออนุมัติ'],
        
    },

    {
        key: '2',
        date: '20 เม.ย. 64',
        name:
        'กระดาษกาว  1  นิ้ว (สีขาว)',
        staff: 'รัตนากร จงรวยกลาง',
    tags: ['สำเร็จ'],
    },

    {
        key: '3',
        date: '25 เม.ย. 64',
        name:
        'ลวดเสียบกระดาษ',
        staff: 'ชนิกานต์ ลี้ผดุง',
    tags: ['สำเร็จ'],
    },

];



const Apphomepage = () => {
    const [visible, setVisible] = useState(false);


    return (

        <div className="Appmiw">
            <div className="itemList-grid">
                <p className="itemout-headtext" >รายการขอเพิ่มพัสดุ</p>


            </div>
            

        
                <div className="itemtableout">
                    <Table dataSource={dataSource3} columns={columns3} />;

           
            </div>


        </div>
    )

}

export default Apphomepage;