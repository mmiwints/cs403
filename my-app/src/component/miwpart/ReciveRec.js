import './App.css';
import React from "react";
import 'antd/dist/antd.css';
import { Button } from 'antd';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import { Modal, Space } from 'antd';
import { ExclamationCircleOutlined } from '@ant-design/icons';

const { confirm } = Modal;
function showDeleteConfirm() {
    confirm({
        title: 'คุณแน่ใจหรือไม่ว่าต้องการลบรายการ',
        icon: <ExclamationCircleOutlined />,
        // content: 'รายการนี้จะถูกลบถาวร',
        okText: 'ลบ',
        okType: 'danger',
        cancelText: 'ยกเลิก',
        onOk() {
            console.log('OK');
        },
        onCancel() {
            console.log('Cancel');
        },
    });
}

function ReciveRecPage() {
    return (
        <div className="Appmiw">
            <div className="Head2">ประวัติการรับเข้าพัสดุ</div>
            <div className="grid_container4">
                <div className="grid_container6">
                    <div className="Reg_text2">วันที่รับเข้า :</div>
                    <div className="Reg_text3">19 สิงหาคม 2021</div>
                </div>
            </div>
            <div className="grid_container4">
                <div className="grid_container6">
                    <div className="Reg_text2">ผู้รับเข้าพัสดุ :</div>
                    <div className="Reg_text3">สมสุข ลิ้มอิ่ม</div>

                </div>

                <div className="grid_container2">
                    <div className="Reg_text2">ตำแหน่ง : </div>
                    <div className="Reg_text3">เจ้าหน้าที่พัสดุ</div>
                </div>
                <div></div>

                <div className="grid_container2">
                    <div className="Reg_text2">เบอร์ติดต่อ :</div>
                    <div className="Reg_text3">0812345678</div>
                </div>
            </div>
            <div className="line"></div>

            {/* ใต้เส้น */}
            <br></br>
            <br></br>
            <div className="grid_container4">
                <div className="grid_container6">
                    <div className="Reg_text2">รายการพัสดุ :</div>
                    <div className="Reg_text3">น้ำยาลบคำผิด (แบบแท่ง)</div>
                </div>

                <div className="grid_container5">
                    <div className="Reg_text2">Lot :</div>
                    <div className="Reg_text3">A0045</div>
                </div>

                <div className="grid_container5">
                    <div className="Reg_text2">จำนวนที่เบิก :</div>
                    <div className="Reg_text3">2</div>
                </div>

                <div className="grid_container5">
                    <div className="Reg_text2">จำนวนที่จ่าย :</div>
                    <div className="Reg_text3">2</div>
                </div>

                <div className="grid_container5">
                    <div className="Reg_text2">หน่วย :</div>
                    <div className="Reg_text3">ชิ้น</div>
                </div>
            </div>

            <div className="grid_container4">
                <div className="grid_container6">
                    <div className="Reg_text2">รายการพัสดุ :</div>
                    <div className="Reg_text3">ลวดเสียบกระดาษ</div>

                </div>

                <div className="grid_container5">
                    <div className="Reg_text2">Lot :</div>
                    <div className="Reg_text3">A0005</div>
                </div>

                <div className="grid_container5">
                    <div className="Reg_text2">จำนวนที่เบิก :</div>
                    <div className="Reg_text3">3</div>
                </div>

                <div className="grid_container5">
                    <div className="Reg_text2">จำนวนที่จ่าย :</div>
                    <div className="Reg_text3">3</div>
                </div>

                <div className="grid_container5">
                    <div className="Reg_text2">หน่วย :</div>
                    <div className="Reg_text3">กล่อง</div>
                </div>
            </div>

            <div className="grid_container4">
                <div className="grid_container6">
                    <div className="Reg_text2">หมายเหตุ :</div>
                    <div className="Reg_text3">จำนวนพัสดุไม่พอ</div>
                </div>
            </div>

            <div className="Date_text">บันทึกเมื่อวันที่ 20 สิงหาคม 2564</div>

            <Link to="/ReciveEditPage"><Button type="primary"> แก้ไขข้อมูล </Button></Link>
            <div className="pad2"> </div>

            <Space wrap>
                <Button onClick={showDeleteConfirm}>
                ลบรายการ
                </Button>
                
            </Space>
            {/* <Button>ลบรายการ</Button> */}

        </div>
    )
}

export default ReciveRecPage;