import './App.css';
import { useState } from 'react';
import React from "react";
import 'antd/dist/antd.css';
import { Button } from 'antd';
import { Input } from 'antd';
import { Upload } from 'antd';
import { UploadOutlined } from '@ant-design/icons';
import { Modal} from 'antd';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import { Image } from 'antd';
import tapepic from './img/tape.jpeg'

function ImageDemo() {
    return (
      <Image
        width={150}
        src={tapepic}
      />
    );
  }

const App = () => {
    const [isModalVisible, setIsModalVisible] = useState(false);

    const showModal = () => {
        setIsModalVisible(true);
    };

    const handleOk = () => {
        setIsModalVisible(false);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };

    return (
        <>
            <Button type="primary"  onClick={showModal}>
            อัพเดทสถานะพัสดุ
            </Button>
            <div className="font">
                <Modal footer={null} title="รายการขอเพิ่มพัสดุ" visible={isModalVisible} onOk={handleOk} onCancel={handleCancel} width={1000}>
                    <div className="grid_popup">
                        <div className="grid_container6">
                            <div className="Reg_text2">วันที่ขอเพิ่ม :</div>
                            <div className="Reg_text3">19 สิงหาคม 2021</div>
                        </div>

                    </div>
                    <div className="grid_popup">
                        <div className="grid_container6">
                            <div className="Reg_text2">ผู้ขอเบิกพัสดุ :</div>
                            <div className="Reg_text3">ธนาภรณ์ ด่านชาญชัย</div>

                        </div>

                        <div className="grid_container5">
                            <div className="Reg_text2">ตำแหน่ง : </div>
                            <div className="Reg_text3">เจ้าหน้าที่พัสดุ</div>
                        </div>

                        <div className="grid_container5">
                            <div className="Reg_text2">เบอร์ติดต่อ :</div>
                            <div className="Reg_text3">0812345678</div>
                        </div>
                    </div>

                    <div className="grid_popup">
                        <div className="grid_container6">
                            <div className="Reg_text2">รายการพัสดุ :</div>
                            <div className="Reg_text3">กระดาษกาว 1 นิ้ว (สีขาว)</div>
                        </div>

                        <div className="grid_container5">
                            <div className="Reg_text3" style={{ color: '#32A05F' }}>X 45 ชิ้น</div>
                        </div>


                    </div>

                    <div className="grid_popup">
                        <div className="grid_container6">
                            <div className="Reg_text2">หมายเหตุ :</div>
                            <div className="Reg_text3">ในการทำเอกสาร</div>
                        </div>
                    </div>
                    <br></br>
                    <div className="grid_popup">
                        <div className="grid_container6">
                            <div className="Reg_text2">รูปพัสดุ :</div>
                            <ImageDemo />
                        </div>
                    </div>
                    <br></br>
                    <div className="button_center">
                    
                        <Button style={{ width :120 }}> ยกเลิก </Button>
                        <div className="pad2"> </div>
                        <Link to="/FiscalSuccessPage"><Button type="primary" style={{ width :120 }}> อัพเดทสถานะ </Button></Link>
                        
                    </div>
                    <br></br>


                    

                </Modal>
            </div>

        </>
    );
};


const { TextArea } = Input;

class MyUpload extends React.Component {
    state = {
        fileList: [],
    };

    handleChange = info => {
        let fileList = [...info.fileList];

        // 1. Limit the number of uploaded files
        // Only to show two recent uploaded files, and old ones will be replaced by the new
        fileList = fileList.slice(-2);

        // 2. Read from response and show file link
        fileList = fileList.map(file => {
            if (file.response) {
                // Component will show file.url as link
                file.url = file.response.url;
            }
            return file;
        });

        this.setState({ fileList });
    };

    render() {
        const props = {
            action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
            onChange: this.handleChange,
            multiple: true,
        };
        return (
            <Upload {...props} fileList={this.state.fileList}>
                <Button icon={<UploadOutlined />}>Upload</Button>
            </Upload>
        );
    }
}

function StatusPage() {
    return (
        <div className="Appmiw">
            <div className="Head2">ประวัติการขอเพิ่มพัสดุ</div>

            <div className="grid_container7">
                <div className="grid_container6">
                    <div className="Reg_text2">วันที่ขอเพิ่ม :</div>
                    <div className="Reg_text3">19 สิงหาคม 2021</div>
                </div>
            </div>
            <div className="grid_container7">
                <div className="grid_container6">
                    <div className="Reg_text2">ผู้ขอเบิกพัสดุ :</div>
                    <div className="Reg_text3">ธนาภรณ์ ด่านชาญชัย</div>

                </div>

                <div className="grid_container5">
                    <div className="Reg_text2">ตำแหน่ง : </div>
                    <div className="Reg_text3">เจ้าหน้าที่พัสดุ</div>
                </div>

                <div className="grid_container5">
                    <div className="Reg_text2">เบอร์ติดต่อ :</div>
                    <div className="Reg_text3">0812345678</div>
                </div>
            </div>

            <div className="grid_container7">
                <div className="grid_container6">
                    <div className="Reg_text2">รายการพัสดุ :</div>
                    <div className="Reg_text3">น้ำยาลบคำผิด (แบบแท่ง)</div>

                </div>

                <div className="grid_container5">
                    <div className="Reg_text2">จำนวนที่เบิก :</div>
                    <div className="Reg_text3">20</div>
                </div>

                <div className="grid_container5">
                    <div className="Reg_text2">จำนวนที่จ่าย :</div>
                    <div className="Reg_text3">20</div>
                </div>

                <div className="grid_container5">
                    <div className="Reg_text2">หน่วย :</div>
                    <div className="Reg_text3">ชิ้น</div>
                </div>
            </div>

            <div className="grid_container7">
                <div className="grid_container6">
                    <div className="Reg_text2">หมายเหตุ :</div>
                    <div className="Reg_text3">ในการทำเอกสาร</div>
                </div>
            </div>
            <br></br>


            <div className="grid_container7">
                <div className="grid_container6">
                    <div className="Reg_text4">แนบรูปพัสดุ :</div>
                    <MyUpload />

                </div>
            </div>
            <br></br>
            <div className="grid_container7">
                <div className="grid_container6">
                    <div className="Reg_text4">หมายเหตุ :</div>
                    <TextArea rows={4} />
                    <div></div>
                </div>
                <div></div>
            </div>
            <br></br>

            <App />
            {/* <Button type="primary" > อัพเดทสถานะพัสดุ </Button> */}





        </div>
    )
}

export default StatusPage;