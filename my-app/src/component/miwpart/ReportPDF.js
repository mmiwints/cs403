import './App.css';
import { AutoComplete } from 'antd';
import React, { useState } from "react";
import { Button } from 'antd';
import 'antd/dist/antd.css';
import { Input, Space } from 'antd';
import { Select } from 'antd';
import { Table, Tag } from 'antd';
import { FilePdfOutlined, EditOutlined } from '@ant-design/icons';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";


const options = [
    {
        value: 'ธนาภรณ์ ด่านชาญชัย',
    },
    {
        value: 'รัตนากร จงรวยกลาง',
    },
    {
        value: 'ขนิกานต์ ลี้ผดุง',
    },
];

// const Complete = () => (
//     <AutoComplete
//         options={options}
//         filterOption={(inputValue, option) =>
//             option.value.toUpperCase().indexOf(inputValue.toUpperCase()) !== -1
//         }
//     ><Search placeholder="ค้นหาชื่อ" onSearch={onSearch} size="large" style={{ width: 350 }} /></AutoComplete>
// );

const { Search } = Input;

const { Option } = Select;

function handleChange(value) {
    console.log(`selected ${value}`);
}

const onSearch = value => console.log(value);

const columns = [

    {
        title: 'ชื่อไฟล์',
        dataIndex: 'filename',
        key: 'filename',
        render: (text, record) => {
            return (
                <Space size="large">
                    <Link to="/PDF">
                        <div className="color_edit"><FilePdfOutlined /></div>
                        <div className="table_text2" style={{ color: " #191919" }}>{text}</div>
                    </Link>

                </Space>


            );
        },
    }



];

const data = [
    {
        key: '1',
        filename: 'สรุปพัสดุสาขาวิชาวิทยาการคอมพิวเตอร์ประจำเดือน พฤษภาคม 2564',

    },

    {
        key: '2',
        filename: 'สรุปพัสดุสาขาวิชาวิทยาการคอมพิวเตอร์ประจำเดือน เมษายน 2564',

    },

    {
        key: '3',
        filename: 'สรุปพัสดุสาขาวิชาวิทยาการคอมพิวเตอร์ประจำเดือน มีนาคม 2564',

    },




];


function ReportPDFPage() {

    const [editname, setEditname] = useState(false)
    const [headname, setHeadname] = useState('อ.สิริกันยา นิลพานิช')
    const editHandler = () => {
        setEditname(!editname)
    }
    const changeHandler = (event) => {
        setHeadname(event.target.value)
        console.log(headname);
    }


    return (
        <div className="Appmiw">
            <div className="Head1">รายงาน</div>
            <div className="Fillter">
                <div className="Reg_text2"> ปี :</div>
                <div className="pad2"></div>
                <Select defaultValue="2564" size="large" style={{ width: 150 }} onChange={handleChange}>

                    <option value="2564">2564</option>
                    <option value="2563">2563</option>
                    <option value="2562">2562</option>
                    <option value="2561">2561</option>
                    <option value="2560">2560</option>
                    <option value="2559">2559</option>
                    <option value="2558">2558</option>

                </Select>

                <div className="Reg_text" > หัวหน้าสาขา :</div>

                {!editname && <div className="Reg_text2" > {headname}</div>}
                <div className="pad3"></div>
                {!editname && <EditOutlined style={{ color: "#32A05F" }} onClick={editHandler} />}

                {editname && <Input size="large" style={{ width: 280 }} placeholder="กรุณากรอกชื่อ" onChange={changeHandler} />}
                <div className="pad3"></div>
                {editname && <Button type="primary" size="large" onClick={editHandler}> บันทึก </Button>}





            </div>



            <Table columns={columns} dataSource={data} />


        </div>
    )
}

export default ReportPDFPage;