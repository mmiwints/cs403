import './App.css';
import React from "react";
import 'antd/dist/antd.css';
import { DatePicker, Space } from 'antd';
import { Input } from 'antd';
import { Select } from 'antd';
import { Form, Button } from 'antd';
import { Radio } from 'antd';
import { Upload, Modal, Result } from 'antd';
import { UploadOutlined } from '@ant-design/icons';
import { message } from 'antd';
import { BrowserRouter as Link } from "react-router-dom";
import { useState } from 'react';


const success = () => {
    message.success('แก้ไขข้อมูลพัสดุสำเร็จ');
};

class MyUpload extends React.Component {
    state = {
        fileList: [],
    };

    handleChange = info => {
        let fileList = [...info.fileList];

        // 1. Limit the number of uploaded files
        // Only to show two recent uploaded files, and old ones will be replaced by the new
        fileList = fileList.slice(-2);

        // 2. Read from response and show file link
        fileList = fileList.map(file => {
            if (file.response) {
                // Component will show file.url as link
                file.url = file.response.url;
            }
            return file;
        });

        this.setState({ fileList });
    };

    render() {
        const props = {
            action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
            onChange: this.handleChange,
            multiple: true,
        };
        return (
            <Upload {...props} fileList={this.state.fileList}>
                <Button icon={<UploadOutlined />}>Upload</Button>
            </Upload>
        );
    }
}

const App = () => {
    const [value, setValue] = React.useState(1);

    const onChange = e => {
        console.log('radio checked', e.target.value);
        setValue(e.target.value);
    };

    return (
        <Radio.Group onChange={onChange} value={value}   >
            <Radio value={1}>สำเร็จ</Radio>
            <Radio value={2}>พัสดุหมด</Radio>
        </Radio.Group>
    );
};

const { TextArea } = Input;

const areas = [
    { label: 'Beijing', value: 'Beijing' },
    { label: 'Shanghai', value: 'Shanghai' },
];

const sights = {
    Beijing: ['Tiananmen', 'Great Wall'],
    Shanghai: ['Oriental Pearl', 'The Bund'],
};



function onChange(date, dateString) {
    console.log(date, dateString);
}

const { Option } = Select;


function handleChange(value) {
    console.log(`selected ${value}`);
}

function ReciveEditPage() {

    const [inputItem, setInputItem] = useState('')
    const [inputSubUnit, setInputSubUnit] = useState('')
    const [inputAmountSub, setInputAmountSub] = useState('')

    const [statusItem, setStatusItem] = useState({})
    const [statusSubUnit, setStatusSubUnit] = useState({})
    const [statusAmountSub, setStatusAmountSub] = useState({})

    const [inputDate, setDate] = useState(1)
    const [inputNote, setInputNote] = useState('')
    const [inputPic, setPic] = useState('')


    const commitdata = {
        item: inputItem,
        subUnit: inputSubUnit,
        amountSub: inputAmountSub,
        date: inputDate,
        note: inputNote,
        pic: inputPic,
    }

    const itemHandler = (event) => {
        setInputItem(event.target.value)
    }

    const subUnitHandler = (event) => {
        setInputSubUnit(event.target.value)
    }

    const amountSubHandler = (event) => {
        setInputAmountSub(event.target.value)
    }

    const NoteHandler = (event) => {
        setInputNote(event.target.value)
    }

    const PicHandler = (event) => {
        setPic(event.target.value)
    }

    const inputDateHandler = (event) => {
        console.log('date checked', event.target.value);
        setDate(event.target.value);
    };



    const reciveEditHandler = () => {

        var check = false
        if (inputItem.trim().length != 0 && inputSubUnit.trim().length != 0 && inputAmountSub != "0") {
            setStatusItem('')
            setStatusSubUnit('')
            setStatusAmountSub('')

            success()
            console.log(commitdata);
            return true
        } else {
            check = false
            if (inputItem.trim().length == 0) {
                console.log("error");
                setStatusItem("error")
            } else {
                setStatusItem('')
            }

            if (inputSubUnit.trim().length == 0) {
                console.log("error");
                setStatusSubUnit("error")
            } else {
                setStatusSubUnit('')
            }

            if (inputAmountSub.trim().length == 0) {
                console.log("error");
                setStatusAmountSub("error")
            } else {
                setStatusAmountSub('')
            }

            return false
        }
    }
    return (
        <div className="Appmiw">
            <div className="Head2">แก้ไขข้อมูลการรับเข้าพัสดุ</div>
            <div className="font">

                <div className="Container">
                    <div className="Container2">
                        <div className="Reg_text4">วันที่รับเข้า :</div>
                        <DatePicker size="large" onChange={onChange} placeholder="เลือกวันที่" />
                        <div></div>
                    </div>
                    <div>

                    </div>
                </div>


                <div className="Container">
                    <div className="Container2">
                        <div className="Reg_text4">รายการพัสดุ :</div>
                        <Form.Item validateStatus={`${statusItem}`}>
                            <Input size="large" placeholder="กรอกรายการพัสดุ" onChange={itemHandler} />
                        </Form.Item>

                        <div></div>
                    </div>
                    <div className="Container3">
                        <div className="Reg_text4">จำนวนที่เบิก :</div>
                        <Form.Item validateStatus={`${statusAmountSub}`}>
                        <Input size="large" style={{ width: 170, }} placeholder="กรอกจำนวน" onChange={amountSubHandler}/>
                        </Form.Item>
                        <div></div>


                    </div>
                </div>

                <div className="Container">
                    <div className="Container2">
                        <div className="Reg_text4">หน่วย :</div>
                        <Form.Item validateStatus={`${statusSubUnit}`}>
                        <Input size="large"    placeholder="กรอกหน่วยที่เบิก" onChange={subUnitHandler}/>
                        </Form.Item>
                    </div>
                </div>
                {/* <br></br> */}


                {/* <Demo /> */}

                <div className="Container">
                    <div className="Container2">
                        <div className="Reg_text4">หมายเหตุ :</div>
                        <TextArea rows={4} onChange={NoteHandler}/>
                        <div></div>
                    </div>
                    <div></div>
                </div>

                <div className="Container">
                    <div className="Container2">
                        <div className="Reg_text4">แนบรูป :</div>
                        <MyUpload onUploadPic={setPic}/>

                    </div>
                </div>


                
                <App2 onClickReport={reciveEditHandler}
                    onText={statusItem, statusSubUnit,statusAmountSub}/>


            </div>


        </div>
    )
}

const App2 = (props) => {
    const [isModalVisible, setIsModalVisible] = useState(false);

    const showModal = () => {
        // setIsModalVisible(true);
        setIsModalVisible(props.onClickReport)
    };

    const handleOk = () => {
        setIsModalVisible(false);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };

    return (
        <>
            <Button type="primary" onClick={showModal}> บันทึกรายการ </Button>
            {/* <Modal title="" footer={null} visible={isModalVisible} onOk={handleOk} onCancel={handleCancel} width={600}>
                <div className="font">
                    <Result
                        status="success"
                        title="แก้ไขข้อมูลการรับเข้าพัสดุสำเร็จ"
                        subTitle="สามารถตรวจสอบการบันทึกได้ที่หน้าประวัติการรับเข้าพัสดุ"
                        extra={[
                            <Link to="/Dashboard"><Button key="console"> กลับหน้าหลัก </Button></Link>

                        ]}
                    />
            </div>
            </Modal> */}
        </>
    );
};

export default ReciveEditPage;