import './App.css';
import React, { useState } from "react";
import 'antd/dist/antd.css';
import { DatePicker, Space } from 'antd';
import { Input } from 'antd';
import { Select } from 'antd';
import { Form, Button } from 'antd';
import { MinusCircleOutlined, PlusOutlined } from '@ant-design/icons';
import { Radio } from 'antd';
import { Upload } from 'antd';
import { UploadOutlined } from '@ant-design/icons';
import tapewhite from '../Img/tapewhite.jpg';
import tape from '../Img/tape1n.jpg';
import tapered1n from '../Img/tapered1n.jpg';
import tapered2n from '../Img/tapered2n.jpg';
import tapeblack1n from '../Img/tapeblack1n.jpg';
import { Modal } from 'antd';
import { Result } from 'antd';
import { AutoComplete } from 'antd';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";




class MyUpload extends React.Component {
    state = {
        fileList: [],
    };

    handleChange = info => {
        let fileList = [...info.fileList];

        // 1. Limit the number of uploaded files
        // Only to show two recent uploaded files, and old ones will be replaced by the new
        fileList = fileList.slice(-2);

        // 2. Read from response and show file link
        fileList = fileList.map(file => {
            if (file.response) {
                // Component will show file.url as link
                file.url = file.response.url;
            }
            return file;
        });

        this.setState({ fileList });
    };

    render() {
        const props = {
            action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
            onChange: this.handleChange,
            multiple: true,
        };
        return (
            <Upload {...props} fileList={this.state.fileList}>
                <Button icon={<UploadOutlined />}>Upload</Button>
            </Upload>
        );
    }
}


const dataSource = [
    {
        key: '1',
        date: '24 เม.ย. 64',
        Supplies: 'กระดาษกาว 1 นิ้ว (สีขาว)',
        staff: 'ธนาภรณ์ ด่านชาญชัย',
        position: 'บุคลากร',
        tags: ['พัสดุหมด'],
        src: tapewhite,
        lot: 'A0001',
        amount: '2',
        lot2: 'A0002',
        amount2: '24',
        category: 'วัสดุสำนักงาน',
        unit: 'ม้วน',
        last_recive: '12 มีนาคม 2564',
        broken: '0',
        remain: 26,
        phone: 419-233-1719
    },

    {
        key: '2',
        date: '20 เม.ย. 64',
        Supplies: 'เทปผ้าติดสันหนังสือ 2 นิ้ว (สีแดง)',
        staff: 'รัตนากร จงรวยกลาง',
        position: 'บุคลากร',
        tags: ['สำเร็จ'],
        src: tapered2n,
        lot: 'A0001',
        amount: '1',
        lot2: 'A0002',
        amount2: '9',
        category: 'วัสดุสำนักงาน',
        unit: 'ม้วน',
        last_recive: '7 มีนาคม 2564',
        broken: '0',
        remain: 10,
        phone: 419-233-1719
    },

    {
        key: '3',
        date: '20 เม.ย. 64',
        Supplies: 'เทปผ้าติดสันหนังสือ 1 นิ้ว (สีแดง)',
        staff: 'ชนิกานต์ ลี้ผดุง',
        position: 'บุคลากร',
        tags: ['สำเร็จ'],
        src: tapered1n,
        // tag: 'ST0003',
        lot: 'A0001',
        amount: '1',
        lot2: 'A0002',
        amount2: '6',
        category: 'วัสดุสำนักงาน',
        unit: 'ม้วน',
        last_recive: '7 มีนาคม 2564',
        broken: '0',
        remain: 7,
        phone: 419-233-1719
    },

    {
        key: '4',
        date: '17 เม.ย. 64',
        Supplies: 'เทปผ้าติดสันหนังสือ 1 นิ้ว (สีน้ำเงิน)',
        staff: 'สุชานันท์ พงษ์ประดิษฐ',
        position: 'บุคลากร',
        tags: ['พัสดุหมด'],
        src: tape,
        // tag: 'ST0004',
        lot: 'A0001',
        amount: '2',
        lot2: 'A0002',
        amount2: '16',
        category: 'วัสดุสำนักงาน',
        unit: 'ม้วน',
        last_recive: '7 มีนาคม 2564',
        broken: '0',
        remain: 18,
        phone: 419-233-1719
    },

    {
        key: '5',
        date: '15 เม.ย. 64',
        Supplies: 'เทปผ้าติดสันหนังสือ 2 นิ้ว (สีดำ)',
        staff: 'ปภาณิน ลอตระกูล',
        position: 'บุคลากร', 
        tags: ['สำเร็จ'],
        src: tapeblack1n,
        // tag: 'ST0005',
        lot: 'A0001',
        amount: '3',
        lot2: 'A0002',
        amount2: '10',
        category: 'วัสดุสำนักงาน',
        unit: 'ม้วน',
        last_recive: '7 มีนาคม 2564',
        broken: '0',
        remain: 13,
        phone: 419-233-1719
    },

];

const RadioStatus = () => {
    const [value, setValue] = React.useState(1);

    const onChange = e => {
        console.log('radio checked', e.target.value);
        setValue(e.target.value);
    };

    return (
        <Radio.Group onChange={onChange} value={value}   >
            <Radio value={1}>สำเร็จ</Radio>
            <Radio value={2}>พัสดุหมด</Radio>
        </Radio.Group>
    );
};

const { TextArea } = Input;

function handleChangeSelect(value) {
    console.log(`selected ${value}`);
}

function onChangedate(date, dateString) {
    console.log(date, dateString);
}

const { Option } = Select;


const Demo = () => {
    const [form] = Form.useForm()
    const onFinish = values => {
        console.log('Received values of form:', values);
        showModal()
    };

    const [isModalVisible, setIsModalVisible] = useState(false);

    const showModal = () => {
        setIsModalVisible(true);
    };

    const handleOk = () => {
        setIsModalVisible(false);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };

    const staffMock = dataSource.map((value) => {
        return { value: value.staff }
    })

    const suppilesMock = dataSource.map((value) => {
        return { value: value.Supplies }
    })

    // const handleChange = () => {
    //     form.setFieldsValue({ sights: [] });
    // };

    return (
        <Form form={form} name="dynamic_form_nest_item" onFinish={onFinish} >
            <Form.Item rules={[{ required: true }]}>

                <div className="Container">
                    <div className="Container2">

                        <div className="Reg_text4">วันที่เบิก :</div>
                        <Form.Item
                            name='requestDate'
                            fieldKey='requestDate_key'
                            rules={[{ type: 'object', required: true }]}

                        >
                            <DatePicker size="large" style={{ width: 280 }}
                                // onChange={inputDateHandler} 
                                onChange={onChangedate}
                                placeholder="เลือกวันที่" />
                        </Form.Item>
                        <div></div>
                    </div>
                    <div></div>
                </div>


                <div className="Container">
                    <Form.Item
                        name='staff'
                        fieldKey='staff_key'
                        rules={[{ required: true, message: '' }]}
                        autoComplete="on"
                    >
                        <div className="Container2">
                            <div className="Reg_text4">ผู้ขอเบิก :</div>
                            <AutoComplete 
                                size="large" 
                                placeholder="กรอกชื่อผู้เบิก"
                                options={staffMock}
                                filterOption={(inputValue, option) =>
                                    option.value.toUpperCase().indexOf(inputValue.toUpperCase()) !== -1
                                } />
                            <div></div>
                        </div>
                    </Form.Item>


                    <div className="Container3">
                        <div className="Reg_text4">ตำแหน่ง :</div>
                        <Form.Item
                            name='position'
                            fieldKey='position'
                        // rules={[{ required: true, message: '' }]}
                        >
                            <Select
                                defaultValue="อาจารย์"
                                size="large"
                                style={{ width: 170 }}
                            >
                                <Option disabled value="0" key="0">เลือกตำแหน่ง</Option>
                                <Option value="1">อาจารย์</Option>
                                <Option value="2">บุคลากร</Option>
                                <Option value="3">เจ้าหน้าที่</Option>
                                <Option value="4">แม่บ้าน</Option>
                                <Option value="5">คนสวน</Option>
                            </Select>
                        </Form.Item>
                    </div>

                </div>

                <div className="Container">
                    <Form.Item
                        name='tel'
                        fieldKey='tel_key'
                        rules={[{ required: true, message: '' }]}
                    >
                        <div className="Container2">
                            <div className="Reg_text4">เบอร์ติดต่อ :</div>
                            <Input size="large" placeholder="กรอกเบอร์ติดต่อ" />
                            <div></div>
                        </div>
                    </Form.Item>
                </div>




                <div>
                    <div className="Container">
                        <Form.Item
                            name='suppilesStart'
                            fieldKey='suppilesStart_key'
                            rules={[{ required: true, message: '' }]}

                        >
                            <div className="Container2">
                                <div className="Reg_text4">รายการพัสดุ :</div>
                                <AutoComplete 
                                size="large" 
                                placeholder="กรอกรายการพัสดุ"
                                options={suppilesMock}
                                filterOption={(inputValue, option) =>
                                    option.value.toUpperCase().indexOf(inputValue.toUpperCase()) !== -1
                                } />

                                <div></div>
                            </div>
                        </Form.Item>

                        <Form.Item
                            name='amountRequestStart'
                            fieldKey='amountRequestStart_key'
                            rules={[{ required: true, message: '' }]}
                        >
                            <div className="Container3">
                                <div className="Reg_text4">จำนวนที่ขอเบิก :</div>

                                <Input size="large" style={{ width: 170, }} placeholder="กรอกจำนวน" />


                                <div></div>

                            </div>
                        </Form.Item>
                    </div>

                    <div className="Container">

                        <div className="Container2">
                            <div className="Reg_text4">หน่วย :</div>
                            <Form.Item

                                name='unitStart'
                                fieldKey='unitStart_key'
                            // rules={[{ required: true, message: '' }]}

                            >
                                <Select defaultValue="เลือกหน่วย" size="large" style={{ width: 280 }} placeholder="เลือกหน่วย" onChange={handleChangeSelect} >
                                    <Option value="1">ชิ้น</Option>
                                    <Option value="2">กล่อง</Option>
                                </Select>
                            </Form.Item>

                        </div>


                        <Form.Item

                            name='amountGiveStart'
                            fieldKey='amountGiveStart_key'
                            rules={[{ required: true, message: '' }]}
                        >
                            <div className="Container3">
                                <div className="Reg_text4">จำนวนที่จ่าย :</div>
                                <Input size="large" style={{ width: 170, }} placeholder="กรอกจำนวน" />
                                <div></div>
                            </div>
                        </Form.Item>
                    </div>
                </div>
            </Form.Item>

            {/* start list loop */}
            <Form.List name="suppile_list">
                {(fields, { add, remove }) => (
                    <>
                        {fields.map(field => (
                            <div key={field.key} align="baseline">
                                <Form.Item
                                    noStyle
                                    shouldUpdate={(prevValues, curValues) =>
                                        prevValues.area !== curValues.area || prevValues.sights !== curValues.sights
                                    }
                                >
                                    {() => (
                                        <div>
                                            <div className="Container">
                                                <Form.Item
                                                    {...field}
                                                    // label="Sight"
                                                    name={[field.name, 'suppiles']}
                                                    fieldKey={[field.fieldKey, 'suppiles']}
                                                    rules={[{ required: true, message: '' }]}

                                                >
                                                    <div className="Container2">
                                                        <div className="Reg_text4">รายการพัสดุ :</div>
                                                        <Input size="large" placeholder="กรอกรายการพัสดุ" />
                                                        <div></div>
                                                    </div>
                                                </Form.Item>

                                                <Form.Item
                                                    {...field}
                                                    // label="Sight"
                                                    name={[field.name, 'amountRequest']}
                                                    fieldKey={[field.fieldKey, 'amountRequest']}
                                                    rules={[{ required: true, message: '' }]}
                                                >
                                                    <div className="Container3">
                                                        <div className="Reg_text4">จำนวนที่ขอเบิก :</div>

                                                        <Input size="large" style={{ width: 170, }} placeholder="กรอกจำนวน" />
                                                        <div className="iconminus">
                                                            <MinusCircleOutlined onClick={() => remove(field.name)} />
                                                        </div>

                                                        <div></div>

                                                    </div>
                                                </Form.Item>
                                            </div>

                                            <div className="Container">

                                                <div className="Container2">
                                                    <div className="Reg_text4">หน่วย :</div>
                                                    <Form.Item
                                                        {...field}
                                                        // label="Sight"
                                                        name={[field.name, 'unit']}
                                                        fieldKey={[field.fieldKey, 'unit']}
                                                        rules={[{ required: true, message: '' }]}

                                                    >
                                                        <Select defaultValue="ชิ้น" size="large" style={{ width: 280 }} placeholder="เลือกหน่วย" onChange={handleChangeSelect} >
                                                            <Option value="1">ชิ้น</Option>
                                                            <Option value="2">กล่อง</Option>
                                                        </Select>
                                                    </Form.Item>
                                                </div>


                                                <Form.Item
                                                    {...field}
                                                    // label="Sight"
                                                    name={[field.name, 'amountGive']}
                                                    fieldKey={[field.fieldKey, 'amountGive']}
                                                    rules={[{ required: true, message: '' }]}
                                                >
                                                    <div className="Container3">
                                                        <div className="Reg_text4">จำนวนที่จ่าย :</div>
                                                        <Input size="large" style={{ width: 170, }} placeholder="กรอกจำนวน" />
                                                        <div></div>
                                                    </div>
                                                </Form.Item>
                                            </div>
                                        </div>





                                    )}
                                </Form.Item>
                            </div>
                        ))}

                        <Form.Item>
                            <div className="Container">
                                <div className="Container2">
                                    <div></div>
                                    <Button type="dashed" size="large" onClick={() => add()} block icon={<PlusOutlined />}>
                                        เพิ่มรายการ
                                    </Button>
                                </div>
                            </div>
                        </Form.Item>
                    </>
                )}
            </Form.List>
            {/* end list loop */}

            <div className="Container">

                <div className="Container2">
                    <div className="Reg_text4">เพื่อนำไปใช้ :</div>
                    <Form.Item
                        name='note'
                        fieldKey='note_key'
                    // rules={[{ required: true, message: '' }]}
                    >
                        <TextArea rows={4} />
                    </Form.Item>
                    <div></div>
                </div>
                <div></div>


            </div>

            <div className="Container">
                <Form.Item
                    name='status'
                    fieldKey='status_key'
                    rules={[{ required: true, message: 'กรุณาเลือกสถานะ' }]}
                >
                    <div className="Container2">
                        <div className="Reg_text4">สถานะ :</div>
                        <div className="padright">
                            <RadioStatus />
                        </div>

                    </div>
                </Form.Item>
            </div>

            <div className="Container">
                <Form.Item
                    name='pic'
                    fieldKey='pic_key'
                // rules={[{ required: true, message: '' }]}
                >
                    <div className="Container2">
                        <div className="Reg_text4">แนบรูปใบเบิก :</div>
                        <MyUpload
                        // onUploadPic={setPic} 
                        />

                    </div>
                </Form.Item>
            </div>



            <Form.Item>
                <Button type="primary" htmlType="submit">
                    บันทึกรายการ
                </Button>
                <Modal title="" footer={null} visible={isModalVisible} onOk={handleOk} onCancel={handleCancel} width={600}>
                    <div className="font">
                        <Result
                            status="success"
                            title="บันทึกรายการสำเร็จ"
                            subTitle="สามารถตรวจสอบการบันทึกได้ที่หน้าประวัติการรับเข้าพัสดุ"
                            extra={[
                                <Link to="/Dashboard"><Button key="console"> กลับหน้าหลัก </Button></Link>

                            ]}
                        />
                    </div>
                </Modal>
            </Form.Item>
        </Form >
    );
};

function RequestFormPage2() {

    return (
        <div className="Appmiw" >
            <div className="Head2">บันทึกข้อมูลการเบิกพัสดุ</div>
            <div className="font">
                <Demo />
            </div>



        </div>
    )
}

export default RequestFormPage2;