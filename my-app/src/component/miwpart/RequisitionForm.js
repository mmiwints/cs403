import './App.css';
import React from "react";
import 'antd/dist/antd.css';
import { Input } from 'antd';
import { Select } from 'antd';
import { Form, Button } from 'antd';
import { Modal } from 'antd';
import { useState } from 'react';
import { Result } from 'antd';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

const App2 = (props) => {
    const [isModalVisible, setIsModalVisible] = useState(false);

    const showModal = () => {
        // setIsModalVisible(true);
        setIsModalVisible(props.onClickReport)
    };

    const handleOk = () => {
        setIsModalVisible(false);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };

    return (
        <>
            <Button type="primary"
                // onClick={props.onClickReport}
                onClick={showModal}
            >
                บันทึกรายการ
        </Button>
            <Modal title="" footer={null} visible={isModalVisible} onOk={handleOk} onCancel={handleCancel} width={600}>
                <div className="font">
                    <Result
                        status="success"
                        title="ส่งคำขอเพิ่มจำนวนพัสดุสำเร็จ"
                        subTitle="สามารถตรวจสอบคำขอได้ที่หน้าประวัติการขอเพิ่มพัสดุ"
                    // extra={[
                    //     <Link to="/Dashboard"><Button key="console"> กลับหน้าหลัก </Button></Link>

                    // ]}
                    />
                </div>
            </Modal>
        </>
    );
};

const { TextArea } = Input;

const areas = [
    { label: 'Beijing', value: 'Beijing' },
    { label: 'Shanghai', value: 'Shanghai' },
];

const sights = {
    Beijing: ['Tiananmen', 'Great Wall'],
    Shanghai: ['Oriental Pearl', 'The Bund'],
};


function onChange(date, dateString) {
    console.log(date, dateString);
}

const { Option } = Select;


function handleChange(value) {
    console.log(`selected ${value}`);
}

function RequisitionFormPage() {


    const [inputItem, setInputItem] = useState('')
    const [inputAmount, setInputAmount] = useState('')
    const [inputUnit, setInputUnit] = useState('0')

    const [statusItem, setStatusItem] = useState({})
    const [statusAmount, setStatusAmount] = useState({})
    const [statusUnit, setStatusUnit] = useState({})

    const [inputNote, setInputNote] = useState('')

    const commitdata = {
        item: inputItem,
        amount: inputAmount,
        unit: inputUnit,
        note: inputNote,

    }


    const itemHandler = (event) => {
        setInputItem(event.target.value)
    }
    const amountHandler = (event) => {
        setInputAmount(event.target.value)
    }
    const unitHandler = (event) => {
        console.log(event);
        setInputUnit(event)
    }

    const NoteHandler = (event) => {
        setInputNote(event.target.value)
    }

    const requisitionHandler = (event) => {

        var check = false
        if (inputItem.trim().length != 0 && inputAmount.trim().length != 0 && inputUnit != "0") {
            setStatusItem('')
            setStatusAmount('')
            setStatusUnit('')
            console.log(commitdata);
            return true
        } else {
            check = false
            if (inputItem.trim().length == 0) {
                console.log("error");
                setStatusItem("error")
            } else {
                setStatusItem('')
            }
            if (inputAmount.trim().length == 0) {
                console.log("error");
                setStatusAmount("error")
            } else {
                setStatusAmount('')
            }
            if (inputUnit == "0") {
                console.log("error");
                setStatusUnit("error")
            } else {
                setStatusUnit('')
            }
            return false
        }
    }


    return (
        <div className="Appmiw">
            <div className="Head2">ส่งคำขอเพิ่มจำนวนพัสดุ</div>
            <div className="font">


                <div className="Container">
                    <div className="Container2">
                        <div className="Reg_text4">รายการพัสดุ :</div>

                        <Form.Item validateStatus={`${statusItem}`}>
                            <Input size="large" placeholder="กรอกรายการพัสดุ"
                                onChange={itemHandler}
                            />
                        </Form.Item>

                        <div></div>
                    </div>
                    <div className="Container3">
                        <div className="Reg_text4">จำนวนที่เบิก :</div>

                        <Form.Item validateStatus={`${statusAmount}`}>
                            <Input size="large" style={{ width: 170, }} placeholder="20"
                                onChange={amountHandler} />
                        </Form.Item>
                        <div></div>


                    </div>
                </div>

                <div className="Container">
                    <div className="Container2">
                        <div className="Reg_text4">หน่วย :</div>

                        <Form.Item validateStatus={`${statusUnit}`}>
                            <Select defaultValue="ชิ้น" size="large" placeholder="เลือกหน่วย" onChange={unitHandler} value={inputUnit}  >
                            <Option disabled value="0" key="0">เลือกหน่วย</Option>
                                <Option value="1">ชิ้น</Option>
                                <Option value="2">กล่อง</Option>
                            </Select>
                        </Form.Item>
                    </div>
                </div>

                <div className="Container">
                    <div className="Container2">
                        <div className="Reg_text4">หมายเหตุ :</div>
                        <TextArea rows={4} onChange={NoteHandler}/>
                        <div></div>
                    </div>
                    <div></div>
                </div>


                <App2 onClickReport={requisitionHandler}
                    onText={statusItem, statusAmount, statusUnit} />
                {/* <Button type="primary"> บันทึกรายการ </Button> */}




            </div>



        </div>
    )
}

export default RequisitionFormPage;