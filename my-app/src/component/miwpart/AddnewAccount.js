import './App.css';
import 'antd/dist/antd.css';
import { Input } from 'antd';
import { Select } from 'antd';
import { Form, Button } from 'antd';
import { Space } from 'antd';
import { EyeInvisibleOutlined, EyeTwoTone } from '@ant-design/icons';
import React, { useState, useEffect } from 'react';
import { Upload } from 'antd';
import ImgCrop from 'antd-img-crop';
import { Modal } from 'antd';
import { ExclamationCircleOutlined } from '@ant-design/icons';
import { Result, message } from 'antd';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import Axios from 'axios'

const App2 = (props) => {
    const [isModalVisible, setIsModalVisible] = useState(false);

    const showModal = () => {
        setIsModalVisible(props.onClickReport)
    };

    const handleOk = () => {
        setIsModalVisible(false);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };

    return (
        <>
            <Button type="primary" onClick={showModal}>
                บันทึกรายการ
        </Button>
            <Modal title="" footer={null} visible={isModalVisible} onOk={handleOk} onCancel={handleCancel} width={600}>
                <div className="font">
                    <Result
                        status="success"
                        title="เพิ่มบัญชีผู้ใช้สำเร็จ"
                        subTitle="สามารถตรวจสอบบัญชีผู้ใช้ได้ที่หน้าจัดการบัญชีผู้ใช้"
                    // extra={[
                    //     <Link to="/Dashboard"><Button key="console"> กลับหน้าหลัก </Button></Link>

                    // ]}
                    />
                </div>
            </Modal>
        </>
    );
};


function onChange(date, dateString) {
    console.log(date, dateString);
}

const { Option } = Select;


function handleChange(value) {
    console.log(`selected ${value}`);
}


function AddnewAccountPage() {

    const [inputName, setInputName] = useState('')
    const [inputSurname, setInputSurname] = useState('')
    const [inputEmail, setInputEmail] = useState('')
    const [inputPassword, setInputPassword] = useState('')
    const [inputPosition, setInputPosition] = useState('0')

    const [statusName, setStatusName] = useState('')
    const [statusSurname, setStatusSurname] = useState('')
    const [statusEmail, setStatusEmail] = useState('')
    const [statusPassword, setStatusPassword] = useState('')
    const [statusPosition, setStatusPosition] = useState('')

    const commitdata = {
        name: inputName,
        surname: inputSurname,
        email: inputEmail,
        password: inputPassword,
        position: inputPosition,
    }

    const nameHandler = (event) => {
        setInputName(event.target.value)
    }
    const surnameHandler = (event) => {
        setInputSurname(event.target.value)
    }
    const emailHandler = (event) => {
        setInputEmail(event.target.value)
    }

    const passwordHandler = (event) => {
        setInputPassword(event.target.value)
    }

    const positionHandler = (event) => {
        setInputPosition(event)
    }

    const NewaccountHandlere = () => {



        // const requestOptions = {
        //     method: 'POST',
        //     headers: { 'Content-Type': 'application/json' },
        //     body: JSON.stringify({ title: 'React POST Request Example' })
        // };
        // fetch('http://localhost:3000/AddnewAccountPage', requestOptions, {
        //     name: inputName,
        //     surname: inputSurname,
        //     email: inputEmail,
        //     password: inputPassword,
        //     position: inputPosition,
        // })
        //     .then(response => response.json())
        //     .then(data => console.log(data));

        // empty dependency array means this effect will only run once (like componentDidMount in classes)






        // fetch.post("http://localhost:3000/AddnewAccountPage", {
        //     name: inputName,
        //     surname: inputSurname,
        //     email: inputEmail,
        //     password: inputPassword,
        //     position: inputPosition,
        // }).then((response) => {
        //     console.log(response);
        // });

        var check = false
        if (inputName.trim().length != 0 && inputSurname.trim().length != 0 && inputEmail.trim().length != 0 && inputPassword.trim().length != 0 && inputPosition != "0") {
            setStatusName('')
            setStatusSurname('')
            setStatusEmail('')
            setStatusPosition('')
            setStatusPassword('')

            setInputName('')
            setInputSurname('')
            setInputEmail('')
            setInputPosition("0")
            setInputPassword('')
            // console.log(commitdata);
            return true
        } else {
            check = false
            if (inputName.trim().length == 0) {

                setStatusName("error")
            } else {
                setStatusName('')
            }

            if (inputPosition == "0") {
                // console.log("error");
                setStatusPosition("error")
            } else {
                setStatusPosition('')
            }

            if (inputSurname.trim().length == 0) {

                setStatusSurname("error")
            } else {
                setStatusSurname('')
            }

            if (inputEmail.trim().length == 0) {

                setStatusEmail("error")
            } else {
                setStatusEmail('')
            }

            if (inputPassword.trim().length == 0) {

                setStatusPassword("error")
            } else {
                setStatusPassword('')
            }
            return false
        }
    }



    return (
        <div className="Appmiw">
            <div className="Head2">เพิ่มบัญชีใหม่</div>
            <div className="font">

                {/* <form onSubmit={NewaccountHandlere}> */}
                <div className="Container">
                    <div className="Container2">
                        <div className="Reg_text4">ชื่อ :</div>
                        <Form.Item validateStatus={`${statusName}`}
                        >
                            <Input size="large" placeholder="กรอกชื่อผู้ใช้" onChange={nameHandler} value={inputName} />
                        </Form.Item>
                        <div></div>
                    </div>
                    <div className="Container2">
                        <div className="Reg_text4">นามสกุล :</div>

                        <Form.Item validateStatus={`${statusSurname}`}
                        >
                            <Input size="large" placeholder="กรอกนามสกุลผู้ใช้" onChange={surnameHandler} value={inputSurname} />
                        </Form.Item>
                        <div></div>
                    </div>
                </div>

                <div className="Container">
                    <div className="Container2">
                        <div className="Reg_text4">ตำแหน่ง :</div>
                        <Form.Item validateStatus={`${statusPosition}`}>
                            <Select defaultValue="อาจารย์" size="large" style={{ width: 280 }} onChange={positionHandler} value={inputPosition}>
                                <Option disabled value="0" key="0">เลือกตำแหน่ง</Option>
                                <Option value="1">อาจารย์</Option>
                                <Option value="2">บุคลากร</Option>
                                <Option value="3">เจ้าหน้าที่</Option>
                                <Option value="4">แม่บ้าน</Option>
                                <Option value="5">คนสวน</Option>
                            </Select>
                        </Form.Item>
                    </div>
                    <div className="Container2">
                        <div className="Reg_text4">อีเมล/ชื่อผู้ใช้ :</div>

                        <Form.Item validateStatus={`${statusEmail}`}
                        >
                            <Input size="large" placeholder="กรอกอีเมลหรือชื่อผู้ใช้" onChange={emailHandler} value={inputEmail} />
                        </Form.Item>
                        <div></div>
                    </div>
                </div>

                <div className="Container">
                    <div className="Container2">
                        <div className="Reg_text4">รหัสผ่าน :</div>
                        <Space direction="vertical">
                            <Form.Item validateStatus={`${statusPassword}`}
                            >
                                <Input.Password size="large" placeholder="กรอกรหัสผ่าน" onChange={passwordHandler} value={inputPassword} />
                                {/* <Input.Password
                                placeholder="input password"
                                iconRender={visible => (visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />)}
                            /> */}
                            </Form.Item>
                        </Space>

                        <div></div>
                    </div>
                </div>

                {/* มันยังเคลื่อนอยู่ */}

                <App2 onClickReport={NewaccountHandlere}
                    onText={statusName, statusSurname, statusEmail, statusPosition}
                />

                {/* </form> */}

            </div>


        </div>
    )
}

const success = () => {
    message.success('เพิ่มบัญชีสำเร็จ');
};



export default AddnewAccountPage;