import './App.css';
import React from "react";
import 'antd/dist/antd.css';
import tapered1n from '../Img/tapered1n.jpg';
import { Tag, Divider } from 'antd';
import { Image } from 'antd';
import pic2 from './img/pic.jpg'
import { Button } from 'antd';
import { BrowserRouter as Router, Switch, Route, Link , useParams } from "react-router-dom";
import { Modal, Space } from 'antd';
import { ExclamationCircleOutlined } from '@ant-design/icons';





const { confirm } = Modal;
function showDeleteConfirm() {
    confirm({
        title: 'คุณแน่ใจหรือไม่ว่าต้องการลบรายการ',
        icon: <ExclamationCircleOutlined />,
        // content: 'รายการนี้จะถูกลบถาวร',
        okText: 'ลบ',
        okType: 'danger',
        cancelText: 'ยกเลิก',
        onOk() {
            console.log('OK');
        },
        onCancel() {
            console.log('Cancel');
        },
    });
}



function ImageDemo() {
    return (
        <Image
            width={100}
            src={pic2}
        />
    );
}



function RequestRecPage(value) {

    const params = useParams();
    console.log(params.key)

    //เอาค่าจาก database มาใส่่นี้
    const data = [
        {
            key: '1',
            dateRequest: '19 สิงหาคม 2021',
            staff: 'ธนาภรณ์ ด่านชาญชัย',
            position: 'บุคคลากร',
            tel: '0812345678',
            tags: ['พัสดุหมด'],
      
            Supplies: 'น้ำยาลบคำผิด (แบบแท่ง)',
            amountRequest: '2',
            amountGive: '2',
            unit: 'ชิ้น',
      
            Supplies: 'ลวดเสียบกระดาษ',
            amountRequest: '5',
            amountGive: '2',
            unit: 'กล่อง',
      
            src: pic2,
            update_at: '23 สิงหาคม 2564'
          },
      
          {
            key: '2',
            dateRequest: '20 เม.ย. 64',
            staff: 'รัตนากร จงรวยกลาง',
            position: 'บุคคลากร',
            tags: ['สำเร็จ'],
      
      
            Supplies: 'เทปผ้าติดสันหนังสือ 2 นิ้ว (สีแดง)',
            amountRequest: '1',
            amountGive: '1',
            unit: 'ชิ้น',
      
            src: pic2,
            update_at: '20 สิงหาคม 2564'
      
          },
      
          {
            key: '3',
            dateRequest: '20 เม.ย. 64',
            staff: 'ชนิกานต์ ลี้ผดุง',
            position: 'คนสวน',
            tags: ['สำเร็จ'],
      
            Supplies: 'น้ำยาล้างจาน',
            amountRequest: '1',
            amountGive: '1',
            unit: 'ชิ้น',
      
            src: tapered1n,
            update_at: '20 สิงหาคม 2564'
      
          },
    ]


    const selectedData = data.filter(value => value.key==params.key)
    console.log(selectedData)

    

    return (
        <div className="Appmiw">
            <div className="Head2">ประวัติการเบิกพัสดุ</div>

            <div className="grid_container">
                <div className="grid_container6">
                    <div className="Reg_text2">วันที่เบิก :</div>
                    <div className="Reg_text3">{}</div>
                </div>
            </div>
            <div className="grid_container">
                <div className="grid_container6">
                    <div className="Reg_text2">ผู้ขอเบิกพัสดุ :</div>
                    <div className="Reg_text3">{}</div>

                </div>

                <div className="grid_container2">
                    <div className="Reg_text2">ตำแหน่ง : </div>
                    <div className="Reg_text3">{}</div>
                </div>

                <div className="grid_container2">
                    <div className="Reg_text2">เบอร์ติดต่อ :</div>
                    <div className="Reg_text3">{}</div>
                </div>
            </div>

            <div className="grid_container">
                <div className="grid_container6">
                    <div className="Reg_text2">สถานะ :</div>
    <Tag className="font" style={{ width: 65, fontSize: 12 }} color="green">{}</Tag>
                </div>
            </div>

            <div className="line"></div>

            {/* ใต้เส้น */}
            <br></br>
            <br></br>

            {data.map((value) =>{
             
                return (
                    <div className="grid_container">
                    <div className="grid_container6">
                        <div className="Reg_text2">รายการพัสดุ :</div>
                        <div className="Reg_text3">{value.Supplies}</div>
    
                    </div>
    
                    <div className="grid_container2">
                        <div className="Reg_text2">จำนวนที่เบิก :</div>
                        <div className="Reg_text3">{value.amountRequest}</div>
                    </div>
    
                    <div className="grid_container2">
                        <div className="Reg_text2">จำนวนที่จ่าย :</div>
                <div className="Reg_text3">{value.amountGive}</div>
                    </div>
    
                    <div className="grid_container2">
                        <div className="Reg_text2">หน่วย :</div>
                        <div className="Reg_text3">{value.unit}</div>
                    </div>
                </div>
    
                )
            })}
            {/* <div className="grid_container">
                <div className="grid_container6">
                    <div className="Reg_text2">รายการพัสดุ :</div>
                    <div className="Reg_text3">น้ำยาลบคำผิด (แบบแท่ง)</div>

                </div>

                <div className="grid_container2">
                    <div className="Reg_text2">จำนวนที่เบิก :</div>
                    <div className="Reg_text3">2</div>
                </div>

                <div className="grid_container2">
                    <div className="Reg_text2">จำนวนที่จ่าย :</div>
                    <div className="Reg_text3">2</div>
                </div>

                <div className="grid_container2">
                    <div className="Reg_text2">หน่วย :</div>
                    <div className="Reg_text3">ชิ้น</div>
                </div>
            </div> */}

            {/* <div className="grid_container">
                <div className="grid_container6">
                    <div className="Reg_text2">รายการพัสดุ :</div>
                    <div className="Reg_text3">ลวดเสียบกระดาษ</div>

                </div>

                <div className="grid_container2">
                    <div className="Reg_text2">จำนวนที่เบิก :</div>
                    <div className="Reg_text3">3</div>
                </div>

                <div className="grid_container2">
                    <div className="Reg_text2">จำนวนที่จ่าย :</div>
                    <div className="Reg_text3">3</div>
                </div>

                <div className="grid_container2">
                    <div className="Reg_text2">หน่วย :</div>
                    <div className="Reg_text3">กล่อง</div>
                </div>
            </div> */}

            <div className="grid_container">
                <div className="grid_container6">
                    <div className="Reg_text2">เพื่อนำไปใช้ :</div>
                    <div className="Reg_text3">ในการทำเอกสาร</div>
                </div>
            </div>

            <div className="grid_container">
                <div className="grid_container6">
                    <div className="Reg_text2">แนบรูปใบเบิก :</div>
                    <ImageDemo />
                </div>
            </div>

            <div className="Date_text">บันทึกเมื่อวันที่ 23 สิงหาคม 2564</div>

            <Link to="/RequestEditPage"><Button type="primary"> แก้ไขข้อมูล </Button></Link>
            <div className="pad2"> </div>


            <Space wrap>
                <Button onClick={showDeleteConfirm}>
                ลบรายการ
                </Button>
                
            </Space>






        </div>
    )
}

export default RequestRecPage;