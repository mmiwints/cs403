import './App.css';
import React, { useState } from 'react';
import 'antd/dist/antd.css';
import { Input } from 'antd';
import { Modal, Button } from 'antd';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";


const App = () => {
    const [isModalVisible, setIsModalVisible] = useState(false);

    const showModal = () => {
        setIsModalVisible(true);
    };

    const handleOk = () => {
        setIsModalVisible(false);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };

    return (
        <>
            <Button type="primary" style={{ width :100 }} onClick={showModal}>
                อนุมัติ
            </Button>
            <div className="font">
                <Modal footer={null} title="รายการขอเพิ่มพัสดุ" visible={isModalVisible} onOk={handleOk} onCancel={handleCancel} width={1000}>
                    <div className="grid_popup">
                        <div className="grid_container6">
                            <div className="Reg_text2">วันที่ขอเพิ่ม :</div>
                            <div className="Reg_text3">19 สิงหาคม 2021</div>
                        </div>

                    </div>
                    <div className="grid_popup">
                        <div className="grid_container6">
                            <div className="Reg_text2">ผู้ขอเบิกพัสดุ :</div>
                            <div className="Reg_text3">ธนาภรณ์ ด่านชาญชัย</div>

                        </div>

                        <div className="grid_container5">
                            <div className="Reg_text2">ตำแหน่ง : </div>
                            <div className="Reg_text3">เจ้าหน้าที่พัสดุ</div>
                        </div>

                        <div className="grid_container5">
                            <div className="Reg_text2">เบอร์ติดต่อ :</div>
                            <div className="Reg_text3">0812345678</div>
                        </div>
                    </div>

                    <div className="grid_popup">
                        <div className="grid_container6">
                            <div className="Reg_text2">รายการพัสดุ :</div>
                            <div className="Reg_text3">กระดาษกาว 1 นิ้ว (สีขาว)</div>
                        </div>

                        <div className="grid_container5">
                            <div className="Reg_text3" style={{ color: '#32A05F' }}>X 45 ชิ้น</div>
                        </div>


                    </div>

                    <div className="grid_popup">
                        <div className="grid_container6">
                            <div className="Reg_text2">หมายเหตุ :</div>
                            <div className="Reg_text3">ในการทำเอกสาร</div>
                        </div>
                    </div>
                    <br></br>
                    <div className="button_center">
                    
                        <Button style={{ width :100 }}> ยกเลิก </Button>
                        <div className="pad2"> </div>
                        <Link to="/FiscalSuccessPage"><Button type="primary" style={{ width :100 }}> ยืนยัน </Button></Link>
                        
                    </div>
                    <br></br>


                    {/* <div className="grid_popup">
                        <div className="grid_container6">
                            <div className="Reg_text2">รูปพัสดุ :</div>
                            <ImageDemo />
                        </div>
                    </div> */}

                </Modal>
            </div>

        </>
    );
};

const { TextArea } = Input;

function ApprovePage() {
    return (
        <div className="Appmiw">
            <div className="Head2">ประวัติการขอเพิ่มพัสดุ</div>

            <div className="grid_container7">
                <div className="grid_container6">
                    <div className="Reg_text2">วันที่ขอเพิ่ม :</div>
                    <div className="Reg_text3">19 สิงหาคม 2021</div>
                </div>
            </div>
            <div className="grid_container7">
                <div className="grid_container6">
                    <div className="Reg_text2">ผู้ขอเบิกพัสดุ :</div>
                    <div className="Reg_text3">ธนาภรณ์ ด่านชาญชัย</div>

                </div>

                <div className="grid_container5">
                    <div className="Reg_text2">ตำแหน่ง : </div>
                    <div className="Reg_text3">บุคคลากร</div>
                </div>

                <div className="grid_container5">
                    <div className="Reg_text2">เบอร์ติดต่อ :</div>
                    <div className="Reg_text3">0812345678</div>
                </div>
            </div>

            <div className="grid_container7">
                <div className="grid_container6">
                    <div className="Reg_text2">รายการพัสดุ :</div>
                    <div className="Reg_text3">น้ำยาลบคำผิด (แบบแท่ง)</div>

                </div>

                <div className="grid_container5">
                    <div className="Reg_text2">จำนวนที่เบิก :</div>
                    <div className="Reg_text3">20</div>
                </div>

                <div className="grid_container5">
                    <div className="Reg_text2">จำนวนที่จ่าย :</div>
                    <Input size="normal" style={{ width: 120, }} placeholder="30" />
                </div>

                <div className="grid_container5">
                    <div className="Reg_text2">หน่วย :</div>
                    <div className="Reg_text3">ชิ้น</div>
                </div>
            </div>

            <div className="grid_container7">
                <div className="grid_container6">
                    <div className="Reg_text2">หมายเหตุ :</div>
                    <div className="Reg_text3">ในการทำเอกสาร</div>
                </div>
            </div>
            <br></br>
            {/* <div className="grid_container7">
                <div className="grid_container6">
                    <div className="Reg_text4">หมายเหตุ :</div>
                    <TextArea rows={4} />
                    <div></div>
                </div>
                <div></div>
            </div> */}
            

            <App />

            {/* <Button type="primary"> อนุมัติ </Button> */}





        </div>
    )
}

export default ApprovePage;