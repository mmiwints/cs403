// import './Appmiw.css';
import '../miwpart/App.css';
import 'antd/dist/antd.css';
import { Input } from 'antd';
import { Select } from 'antd';
import { Form, Button } from 'antd';
import { Space } from 'antd';
import { EyeInvisibleOutlined, EyeTwoTone } from '@ant-design/icons';
import React, { useState } from 'react';
import { Upload } from 'antd';
import ImgCrop from 'antd-img-crop';
import { message,Modal,Result } from 'antd';

const success = () => {
    message.success('แก้ไขข้อมูลสำเร็จ');
};

const Demo = () => {

    const [fileList, setFileList] = useState([
        //   {
        //     uid: '-1',
        //     name: 'image.png',
        //     status: 'done',
        //     url: 'https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png',
        //   },
    ]);

    const onChange = ({ fileList: newFileList }) => {
        setFileList(newFileList);
    };

    const onPreview = async file => {
        let src = file.url;
        if (!src) {
            src = await new Promise(resolve => {
                const reader = new FileReader();
                reader.readAsDataURL(file.originFileObj);
                reader.onload = () => resolve(reader.result);
            });
        }
        const image = new Image();
        image.src = src;
        const imgWindow = window.open(src);
        imgWindow.document.write(image.outerHTML);

    };

    return (
        <ImgCrop rotate >
            <Upload 
                
                action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
                listType="picture-card"
                fileList={fileList}
                onChange={onChange}
                onPreview={onPreview}
            >
                {fileList.length < 1 && '+ Upload'}
            </Upload>
        </ImgCrop>
    );
};

const { TextArea } = Input;

const areas = [
    { label: 'Beijing', value: 'Beijing' },
    { label: 'Shanghai', value: 'Shanghai' },
];

const sights = {
    Beijing: ['Tiananmen', 'Great Wall'],
    Shanghai: ['Oriental Pearl', 'The Bund'],
};


function onChange(date, dateString) {
    console.log(date, dateString);
}

const { Option } = Select;


function handleChange(value) {
    console.log(`selected ${value}`);
}

function AccountEditPage() {

    const [inputName, setInputName] = useState('')
    const [inputLastName, setInputLastName] = useState('')
    const [inputEmail, setInputEmail] = useState('')
    

    const [statusName, setStatusName] = useState({})
    const [statusLastName, setStatusLastName] = useState({})
    const [statusEmail, setStatusEmail] = useState({})
   

    const [inputPic, setPic] = useState('')

    const commitdata = {
        name: inputName,
        lastName: inputLastName,
        email: inputEmail,
        pic: inputPic,
    }

    const nameHandler = (event) => {
        setInputName(event.target.value)
    }
    const lastnameHandler = (event) => {
        setInputLastName(event.target.value)
    }
    const emailHandler = (event) => {
        setInputEmail(event.target.value)
    }

   

    const PicHandler = (event) => {
        setPic(event.target.value)
    }

    const editaccountHandler = () => {
        var check = false
        if (inputName.trim().length != 0 && inputLastName.trim().length != 0 && inputEmail.trim().length != 0 ) {
            setStatusName('')
            setStatusLastName('')
            setStatusEmail('')
            
            console.log(commitdata);
            return true
        } else {
            check = false
            if (inputName.trim().length == 0) {

                setStatusName("error")
            } else {
                setStatusName('')
            }

            if (inputLastName.trim().length == 0) {

                setStatusLastName("error")
            } else {
                setStatusLastName('')
            }

            if (inputEmail.trim().length == 0) {

                setStatusEmail("error")
            } else {
                setStatusEmail('')
            }

            return false
        }
    }

    return (
        <div className="Appmiw">
            <div className="Head2">แก้ไขบัญชีผู้ใช้</div>
            <div className="font">


                <div className="Container">
                    <div className="Container2">
                        <div className="Reg_text4">ชื่อ :</div>
                        <Form.Item validateStatus={`${statusName}`}
                            >
                                <Input size="large" placeholder="กรอกชื่อผู้ใช้" onChange={nameHandler} />
                            </Form.Item>
                        <div></div>
                    </div>
                    <div className="Container2">
                        <div className="Reg_text4">นามสกุล :</div>
                        <Form.Item validateStatus={`${statusLastName}`}
                            >
                                <Input size="large" placeholder="กรอกนามสกุลผู้ใช้" onChange={lastnameHandler} />
                            </Form.Item>
                        <div></div>
                    </div>
                </div>

                <div className="Container">
                    <div className="Container2">
                        <div className="Reg_text4">ตำแหน่ง :</div>
                        <Select defaultValue="อาจารย์" size="large" onChange={handleChange}>
                            <Option value="jack">อาจารย์</Option>
                            <Option value="lucy">บุคลากร</Option>
                            <Option value="lucy">เจ้าหน้า</Option>
                            <Option value="lucy">แม่บ้าน</Option>
                            <Option value="lucy">คนสวน</Option>
                        </Select>
                    </div>
                    <div className="Container2">
                        <div className="Reg_text4">อีเมล :</div>
                        <Form.Item validateStatus={`${statusEmail}`}
                            >
                                <Input size="large" placeholder="กรอกอีเมล" onChange={emailHandler} />
                            </Form.Item>
                        <div></div>
                    </div>
                </div>

                
                {/* มันยังเคลื่อนอยู่ */}
                <div className="Container">
                    <div className="Container2">
                        <div className="Reg_text4">รูปโปรไฟล์ :</div>
                        <Demo onUploadPic={setPic}/>
                        <div></div>
                    </div>
                </div>
                
                {/* <Button type="primary" onClick={success}>บันทึก</Button> */}
                <App2 onClickReport={editaccountHandler}
                    onText={statusName, statusLastName, statusEmail}
                />

            </div>

        </div>
    )
}


const App2 = (props) => {
    const [isModalVisible, setIsModalVisible] = useState(false);

    const showModal = () => {
        setIsModalVisible(props.onClickReport)
    };

    const handleOk = () => {
        setIsModalVisible(false);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };

    return (
        <>
            <Button type="primary" onClick={showModal}>
                บันทึกรายการ
        </Button>
            <Modal title="" footer={null} visible={isModalVisible} onOk={handleOk} onCancel={handleCancel} width={600}>
                <div className="font">
                    <Result
                        status="success"
                        title="แก้ไขบัญชีผู้ใช้สำเร็จ"
                        subTitle="สามารถตรวจสอบบัญชีผู้ใช้ได้ที่หน้าจัดการบัญชีผู้ใช้"
                    // extra={[
                    //     <Link to="/Dashboard"><Button key="console"> กลับหน้าหลัก </Button></Link>

                    // ]}
                    />
                </div>
            </Modal>
        </>
    );
};


export default AccountEditPage;