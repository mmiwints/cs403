import './App.css';
import { AutoComplete } from 'antd';
import React, { useState } from "react";
import 'antd/dist/antd.css';
import { Input, Space, Table  } from 'antd';
import { Select } from 'antd';
// import { Table, Tag } from 'antd';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";



function handleChange(value) {
  console.log(`selected ${value}`);
}

const columns = [
  {
    title: 'วันที่',
    dataIndex: 'date',
    key: 'date',
  },
  // {
  //   title: 'Lot',
  //   dataIndex: 'lot',
  //   key: 'lot',
  // },
  {
    title: 'รายการ',
    dataIndex: 'Supplies',
    key: 'Supplies',
    render: (text, record) => {
      return (
        <Link to="/ReciveRecPage">
          <div className="table_text" style={{ color: " #191919" }}>
            <div>{text}</div>
          </div>
        </Link>

      );
    }
  },
  {
    title: 'จำนวน',
    dataIndex: 'amount',
    key: 'amount',
    // render: text => <div className="table_text">{text}</div>,
  },
  {
    title: 'หน่วย',
    dataIndex: 'unit',
    key: 'unit',
    // render: text => <div className="table_text">{text}</div>,
  },

];

const data = [
  {
    key: '1',
    lot: 'A0034',
    date: '24 เม.ย. 64',
    Supplies: 'เทปผ้าติดสันหนังสือ 1 นิ้ว (สีน้ำเงิน)',
    amount: 50,
    unit: 'ม้วน',
  },

  {
    key: '2',
    lot: 'A0012',
    date: '20 เม.ย. 64',
    Supplies: 'กระดาษกาว  1  นิ้ว (สีขาว)',
    amount: 3,
    unit: 'ม้วน'
  },

  {
    key: '3',
    lot: 'A0080',
    date: '20 เม.ย. 64',
    Supplies: 'ลวดเสียบกระดาษ',
    amount: 2,
    unit: 'กล่อง'
  },

  {
    key: '4',
    lot: 'A0076',
    date: '17 เม.ย. 64',
    Supplies: 'เทปผ้าติดสันหนังสือ 1 นิ้ว (สีน้ำเงิน)',
    amount: 35,
    unit: 'ม้วน'
  },

  {
    key: '5',
    lot: 'A0029',
    date: '16 เม.ย. 64',
    Supplies: 'ดินสอดำ',
    amount: 3,
    unit: 'กล่อง'
  },

  {
    key: '6',
    lot: 'A0003',
    date: '15 เม.ย. 64',
    Supplies: 'น้ำยาลบคำผิด (แบบแท่ง)',
    amount: 1,
    unit: 'ชิ้น'
  },


];

const { Search } = Input;






function RecivePage() {
  const [search, setSearch] = useState(data)

  const onSearch = (value) => {
    setSearch(data.filter((item) => {
      return item.Supplies.includes(value)
    }))
    // console.log(search)
  };

  const data2 = [
    {
      key: '1',
      lot: 'A0034',
      date: '24 เม.ย. 64',
      Supplies: 'เทปผ้าติดสันหนังสือ 1 นิ้ว (สีน้ำเงิน)',
      amount: 5,
      unit: 'ม้วน',
    },

    {
      key: '2',
      lot: 'A0012',
      date: '20 เม.ย. 64',
      Supplies: 'กระดาษกาว  1  นิ้ว (สีขาว)',
      amount: 3,
      unit: 'ม้วน'
    },

    {
      key: '3',
      lot: 'A0080',
      date: '20 เม.ย. 64',
      Supplies: 'ลวดเสียบกระดาษ',
      amount: 2,
      unit: 'กล่อง'
    },

    {
      key: '4',
      lot: 'A0029',
      date: '16 เม.ย. 64',
      Supplies: 'ดินสอดำ',
      amount: 3,
      unit: 'กล่อง'
    },

    {
      key: '5',
      lot: 'A0003',
      date: '15 เม.ย. 64',
      Supplies: 'น้ำยาลบคำผิด (แบบแท่ง)',
      amount: 1,
      unit: 'ชิ้น'
    },

  ];

  const optionsMock = data2.map((value) => {
    return { value: value.Supplies }
  })

  return (
    <div className="Appmiw">
      <div className="Head1">ประวัติการรับเข้าพัสดุ</div>
      <div className="Fillter">
        <AutoComplete
          options={optionsMock}
          filterOption={(inputValue, option) =>
            option.value.toUpperCase().indexOf(inputValue.toUpperCase()) !== -1
          }
        ><Search placeholder="ค้นหาชื่อพัสดุ" onSearch={onSearch} size="large" style={{ width: 350 }} /></AutoComplete>

        {/* <Search placeholder="ค้นหาชื่อพัสดุหรือชื่อผู้เบิก" onSearch={onSearch} size="large" style={{ width: 350 }} /> */}
        <div className="Reg_text"> เดือน :</div>
        <Select className="Drop_text" defaultValue="มกราคม" size="large" style={{ width: 150 }} onChange={handleChange}>
          <option value="01">มกราคม</option>
          <option Value="02">กุมภาพันธ์</option>
          <option value="03">มีนาคม</option>
          <option value="04">เมษายน</option>
          <option value="05">พฤษภาคม</option>
          <option value="06">มิถุนายน</option>
          <option value="07">กรกฎาคม</option>
          <option value="08">สิงหาคม</option>
          <option value="09">กันยายน</option>
          <option value="10">ตุลาคม</option>
          <option value="11">พฤศจิกายน</option>
          <option value="12">ธันวาคม</option>
        </Select>

        <div className="Reg_text"> ปี :</div>

        <Select defaultValue="2564" size="large" style={{ width: 150 }} onChange={handleChange}>

          <option value="2564">2564</option>
          <option value="2563">2563</option>
          <option value="2562">2562</option>
          <option value="2561">2561</option>
          <option value="2560">2560</option>
          <option value="2559">2559</option>
          <option value="2558">2558</option>

        </Select>

      </div>
      <Table
        columns={columns}
        dataSource={search}
        // pagination={{
        //   pageSize: 4,
        // }}
       
      />

    </div>
  )
}

export default RecivePage;