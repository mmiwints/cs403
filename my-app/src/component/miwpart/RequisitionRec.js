import './App.css';
import React from "react";
import 'antd/dist/antd.css';
import { Steps } from 'antd';
import { Button, Tag } from 'antd';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import { Modal, Space } from 'antd';
import { ExclamationCircleOutlined } from '@ant-design/icons';



const { confirm } = Modal;
function showDeleteConfirm() {
    confirm({
        title: 'คุณแน่ใจหรือไม่ว่าต้องการลบรายการ',
        icon: <ExclamationCircleOutlined />,
        // content: 'รายการนี้จะถูกลบถาวร',
        okText: 'ลบ',
        okType: 'danger',
        cancelText: 'ยกเลิก',
        onOk() {
            console.log('OK');
        },
        onCancel() {
            console.log('Cancel');
        },
    });
}

const { Step } = Steps;

function RequisitionRecPage() {
    return (
        <div className="Appmiw">
            <div className="Head2">ประวัติการขอเพิ่มพัสดุ</div>

            <div className="grid_container">
                <div className="grid_container6">
                    <div className="Reg_text2">วันที่ขอเพิ่ม :</div>
                    <div className="Reg_text3">19 สิงหาคม 2021</div>
                </div>
            </div>
            <div className="grid_container">
                <div className="grid_container6">
                    <div className="Reg_text2">ผู้ขอเบิกพัสดุ :</div>
                    <div className="Reg_text3">ธนาภรณ์ ด่านชาญชัย</div>

                </div>

                <div className="grid_container2">
                    <div className="Reg_text2">ตำแหน่ง : </div>
                    <div className="Reg_text3">บุคคลากร</div>
                </div>

                <div className="grid_container2">
                    <div className="Reg_text2">เบอร์ติดต่อ :</div>
                    <div className="Reg_text3">0812345678</div>
                </div>
            </div>

            <div className="grid_container">
                <div className="grid_container6">
                    <div className="Reg_text2">รายการพัสดุ :</div>
                    <div className="Reg_text3">น้ำยาลบคำผิด (แบบแท่ง)</div>

                </div>

                <div className="grid_container2">
                    <div className="Reg_text2">จำนวนที่เบิก :</div>
                    <div className="Reg_text3">2</div>
                </div>

                <div className="grid_container2">
                    <div className="Reg_text2">จำนวนที่จ่าย :</div>
                    <div className="Reg_text3">2</div>
                </div>

                <div className="grid_container2">
                    <div className="Reg_text2">หน่วย :</div>
                    <div className="Reg_text3">ชิ้น</div>
                </div>
            </div>

            <div className="grid_container">
                <div className="grid_container6">
                    <div className="Reg_text2">เพื่อนำไปใช้ :</div>
                    <div className="Reg_text3">ในการทำเอกสาร</div>
                </div>
            </div>

            <div className="grid_container">
                <div className="grid_container6">
                    <div className="Reg_text2">สถานะ :</div>
                        <Tag className="font" style={{ width: 65, fontSize: 12 }} color="green">{}</Tag>
                </div>
            </div>

            {/* <div className="line"></div>
            <br></br>
            <br></br> */}

            {/* <div className="Reg_text5">สถานะการขอเพิ่มพัสดุ</div>
            <br></br>
            <br></br>
            <br></br>
            <div className="font">
                <Steps  current={1} >
                    <Step title="ขอเพิ่มพัสดุ" description="ส่งคำขอเพิ่มพัสดุสำเร็จ" />
                    <Step title="คำขออนุมัติ" description="ดำเนินการจัดซื้อพัสดุ" />
                    <Step title="จัดซื้อพัสดุสำเร็จ" description="เจ้าหน้าที่การคลังได้รับพัสดุแล้ว" />
                </Steps>
            </div> */}
            {/* direction="vertical" style={{ height: 300, }}  */}
            

            <div className="Date_text">บันทึกเมื่อวันที่ 23 สิงหาคม 2564</div>

            <br></br>

            <Space wrap>
                <Button onClick={showDeleteConfirm}>
                ลบรายการ
                </Button>
                
            </Space>

            {/* <Button>ลบรายการ</Button> */}


        </div>
    )
}

export default RequisitionRecPage;