import './App.css';
import React, { useState } from 'react';
import 'antd/dist/antd.css';
import { Input,Modal,Result } from 'antd';
import { Select } from 'antd';
import { Form, Button, Space } from 'antd';
import { Radio } from 'antd';
import { Upload } from 'antd';
import { UploadOutlined } from '@ant-design/icons';
import { message } from 'antd';

const success = () => {
    message.success('แก้ไขข้อมูลสำเร็จ');
};




class MyUpload extends React.Component {
    state = {
        fileList: [],
    };

    handleChange = info => {
        let fileList = [...info.fileList];

        // 1. Limit the number of uploaded files
        // Only to show two recent uploaded files, and old ones will be replaced by the new
        fileList = fileList.slice(-2);

        // 2. Read from response and show file link
        fileList = fileList.map(file => {
            if (file.response) {
                // Component will show file.url as link
                file.url = file.response.url;
            }
            return file;
        });

        this.setState({ fileList });
    };

    render() {
        const props = {
            action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
            onChange: this.handleChange,
            multiple: true,
        };
        return (
            <Upload {...props} fileList={this.state.fileList}>
                <Button icon={<UploadOutlined />}>Upload</Button>
            </Upload>
        );
    }
}

//bot
const App = () => {
    const [value, setValue] = React.useState(1);

    const onChange = e => {
        console.log('radio checked', e.target.value);
        setValue(e.target.value);
    };

    return (
        <Radio.Group onChange={onChange} value={value}   >
            <Radio value={1}>ชำรุด</Radio>
            <Radio value={2}>สูญหาย</Radio>
        </Radio.Group>
    );
};

const { TextArea } = Input;

const areas = [
    { label: 'Beijing', value: 'Beijing' },
    { label: 'Shanghai', value: 'Shanghai' },
];

const sights = {
    Beijing: ['Tiananmen', 'Great Wall'],
    Shanghai: ['Oriental Pearl', 'The Bund'],
};


function onChange(date, dateString) {
    console.log(date, dateString);
}

const { Option } = Select;


function handleChange(value) {
    console.log(`selected ${value}`);
}



//mainfrom
function ReportEditPage() {

    const [inputItem, setInputItem] = useState('')
    const [inputAmount, setInputAmount] = useState('')
    const [inputUnit, setInputUnit] = useState('0')

    const [statusItem, setStatusItem] = useState({})
    const [statusAmount, setStatusAmount] = useState({})
    const [statusUnit, setStatusUnit] = useState({})

    const [inputCause, setCause] = useState(1)
    const [inputNote, setInputNote] = useState('')
    const [inputPic, setPic] = useState('')

    const commitdata = {
        item: inputItem,
        amount: inputAmount,
        unit: inputUnit,
        cause: inputCause,
        note: inputNote,
        pic: inputPic,
    }


    const itemHandler = (event) => {
        setInputItem(event.target.value)
    }
    const amountHandler = (event) => {
        setInputAmount(event.target.value)
    }
    const unitHandler = (event) => {
        console.log(event);
        setInputUnit(event)
    }

    const PicHandler = (event) => {
        setPic(event.target.value)
    }

    const NoteHandler = (event) => {
        setInputNote(event.target.value)
    }

    const inputCauseHandler = (event) => {
        console.log('radio checked', event.target.value);
        setCause(event.target.value);
    };

    const reportHandler = () => {

        var check = false
        if (inputItem.trim().length != 0 && inputAmount.trim().length != 0 && inputUnit != "0") {
            setStatusItem('')
            setStatusAmount('')
            setStatusUnit('')
            console.log(commitdata);
            return true
        } else {
            check = false
            if (inputItem.trim().length == 0) {
                // console.log("error");
                setStatusItem("error")
            } else {
                setStatusItem('')
            }
            if (inputAmount.trim().length == 0) {
                // console.log("error");
                setStatusAmount("error")
            } else {
                setStatusAmount('')
            }
            if (inputUnit == "0") {
                // console.log("error");
                setStatusUnit("error")
            } else {
                setStatusUnit('')
            }
            return false
        }
    }


    return (
        <div className="Appmiw">
            <div className="Head2">แก้ไขรายการพัสดุเสื่อมสภาพ</div>
            <div className="font">

                <div className="Container">
                    <div className="Container2">
                        <div className="Reg_text4">รายการ :</div>
                        <div className="padright">
                            <App onChangeCause={inputCauseHandler} onInputCause={inputCause} />
                        </div>

                    </div>
                </div>


                <div className="Container">
                    <div className="Container2">
                        <div className="Reg_text4">รายการพัสดุ :</div>

                        <Form.Item validateStatus={`${statusItem}`}
                        >
                            <Input size="large" placeholder="กรอกรายการพัสดุ" onChange={itemHandler} />

                        </Form.Item>

                        <div></div>
                    </div>
                    <div className="Container3">
                        <div className="Reg_text4">จำนวน :</div>

                        <Form.Item validateStatus={`${statusAmount}`}>
                            <Input size="large" style={{ width: 170, }} placeholder="กรอกจำนวน" onChange={amountHandler} />
                        </Form.Item>

                        <div></div>


                    </div>
                </div>

                <div className="Container">
                    <div className="Container2">
                        <div className="Reg_text4">หน่วย :</div>

                        <Form.Item validateStatus={`${statusUnit}`}>
                            <Select defaultValue="ชิ้น" size="large" placeholder="เลือกหน่วย" onChange={unitHandler} value={inputUnit} >
                                <Option disabled value="0" key="0">เลือกหน่วย</Option>
                                <Option value="1">ชิ้น</Option>
                                <Option value="2">กล่อง</Option>
                            </Select>
                        </Form.Item>

                    </div>
                </div>
                <br></br>

                <div className="Container">
                    <div className="Container2">
                        <div className="Reg_text4">หมายเหตุ :</div>

                        <Form.Item>

                            <TextArea rows={4} onChange={NoteHandler} />

                        </Form.Item>
                        <div></div>
                    </div>
                    <div></div>
                </div>



                <div className="Container">
                    <div className="Container2">
                        <div className="Reg_text4">แนบรูปใบเบิก :</div>
                        <MyUpload onUploadPic={setPic} />
                    </div>
                </div>


                {/* <Space> */}

                <App2 onClickReport={reportHandler}
                    onText={statusItem, statusAmount, statusUnit}
                />
                {/* </Space> */}


            </div>

        </div>
    )
}

const App2 = (props) => {

    const [isModalVisible, setIsModalVisible] = useState(false);

    const showModal = () => {

        setIsModalVisible(props.onClickReport)

    };

    const handleOk = () => {
        setIsModalVisible(false);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };

    return (
        <>
            <Button type="primary"
                // onClick={props.onClickReport}
                onClick={showModal}
            >
                บันทึก
        </Button>
            <Modal title="" footer={null} visible={isModalVisible} onOk={handleOk} onCancel={handleCancel} width={600}>
                <div className="font">
                    <Result
                        status="success"
                        title="แก้ไขรายการพัสดุเสื่อมสภาพสำเร็จ"
                        subTitle="สามารถตรวจรายการได้ที่หน้าพัสดุเสื่อมสภาพ"
                   
                    />
                </div>
            </Modal>
        </>
    );
};

export default ReportEditPage;