import './App.css';
import React, { useRef, useState } from 'react';
import 'antd/dist/antd.css';
import { Input, message } from 'antd';
import { Select } from 'antd';
import { Form, Button } from 'antd';
import { Radio } from 'antd';
import { Upload } from 'antd';
import { UploadOutlined } from '@ant-design/icons';
import { Modal } from 'antd';
import { ExclamationCircleOutlined } from '@ant-design/icons';
import { Result } from 'antd';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";



function ReportFormPage() {
    const [inputItem, setInputItem] = useState('')
    const [inputAmount, setInputAmount] = useState('')
    const [inputUnit, setInputUnit] = useState('0')

    const [statusItem, setStatusItem] = useState('')
    const [statusAmount, setStatusAmount] = useState('')
    const [statusUnit, setStatusUnit] = useState('')
    
    const [inputCause, setCause] = useState(1)
    const [inputNote, setInputNote] = useState('')
    const [inputPic, setPic] = useState('')

    const commitdata = {
        item: inputItem,
        amount: inputAmount,
        unit: inputUnit,
        cause: inputCause,
        note: inputNote,
        pic: inputPic,
    }

    const PicHandler = (event) => {
        setPic(event.target.value)
    }

    const NoteHandler = (event) => {
        setInputNote(event.target.value)
    }
    const itemHandler = (event) => {
        setInputItem(event.target.value)
    }
    const amountHandler = (event) => {
        setInputAmount(event.target.value)
    }
    const unitHandler = (event) => {
        console.log(event);
        setInputUnit(event)
        // setInputUnit(event.target.value)
    }
    const inputCauseHandler = (event) => {
        console.log('radio checked', event.target.value);
        setCause(event.target.value);
    };

    const reportHandler = () => {
        
        var check = false
        if (inputItem.trim().length != 0 && inputAmount.trim().length != 0 && inputUnit != "0") {
            setStatusItem('')
            setStatusAmount('')
            setStatusUnit('')

            setInputAmount('')
            setInputItem('')
            setInputUnit("0")
            console.log(commitdata);
            return true
        } else {
            check = false
            if (inputItem.trim().length == 0) {
                setStatusItem("error")
            } else {
                setStatusItem('')
            }

            if (inputAmount.trim().length == 0) {
                setStatusAmount("error")
            } else {
                setStatusAmount('')
            }
            if (inputUnit == "0") {
                setStatusUnit("error")
            } else {
                setStatusUnit('')
            }
            return false
        }


    }

    return (
        <div className="Appmiw">
            <div className="Head2">แจ้งพัสดุเสื่อมสภาพ</div>
            <div className="font">
                <Form>


                    <div className="Container">
                        <div className="Container2">
                            <div className="Reg_text4">รายการ :</div>
                            <div className="padright">
                                <App onChangeCause={inputCauseHandler} onInputCause={inputCause} />
                            </div>

                        </div>
                    </div>


                    <div className="Container">
                        <div className="Container2">
                            <div className="Reg_text4">รายการพัสดุ :</div>

                            <Form.Item validateStatus={`${statusItem}`}
                            >
                                <Input size="large" style={{ width: 280 }} placeholder="กรอกรายการพัสดุ" onChange={itemHandler} value={inputItem}/>
                            </Form.Item>
                            <div></div>


                        </div>
                        <div className="Container3">
                            <div className="Reg_text4">จำนวน :</div>
                            <Form.Item validateStatus={`${statusAmount}`}>
                                <Input size="large" style={{ width: 170, }} placeholder="กรอกจำนวน" onChange={amountHandler} value={inputAmount}/>
                            </Form.Item>

                            <div></div>


                        </div>
                    </div>

                    <div className="Container">
                        <div className="Container2">
                            <div className="Reg_text4">หน่วย :</div>
                            <Form.Item validateStatus={`${statusUnit}`}>
                                <Select defaultValue="1" size="large" placeholder="เลือกหน่วย" onChange={unitHandler} value={inputUnit}>
                                    <Option disabled value="0" key="0">เลือกหน่วย</Option>
                                    <Option value="1" key="1">ชิ้น</Option>
                                    <Option value="2" key="2">กล่อง</Option>
                                </Select>
                            </Form.Item>
                        </div>
                    </div>

                    <div className="Container">
                        <div className="Container2">
                            <div className="Reg_text4">หมายเหตุ :</div>
                            <Form.Item >
                                <TextArea rows={4} onChange={NoteHandler} />
                            </Form.Item>
                            <div></div>
                        </div>
                        <div></div>
                    </div>



                    <div className="Container">
                        <div className="Container2">
                            <div className="Reg_text4">แนบรูป :</div>
                            <MyUpload onUploadPic={setPic} />


                        </div>
                    </div>

                    <App2 onClickReport={reportHandler} onText={statusItem, statusAmount, statusUnit} />

                </Form>


            </div>



        </div>
    )
}


const App2 = (props) => {
    const [isModalVisible, setIsModalVisible] = useState(false);

    const showModal = () => {

        setIsModalVisible(props.onClickReport)

    };

    const handleOk = () => {
        setIsModalVisible(false);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };
    return (
        <>
            <Button type="primary"
                // onClick={props.onClickReport}
                onClick={showModal}
            >
                แจ้งรายการ
        </Button>
            <Modal title="" footer={null} visible={isModalVisible} onOk={handleOk} onCancel={handleCancel} width={600}>
                <div className="font">
                    <Result
                        status="success"
                        title="แจ้งรายการพัสดุเสื่อมสภาพสำเร็จ"
                        subTitle="สามารถตรวจรายการได้ที่หน้าพัสดุเสื่อมสภาพ"
                    // extra={[
                    //     <Link to="/Dashboard"><Button key="console"> กลับหน้าหลัก </Button></Link>

                    // ]}
                    />
                </div>
            </Modal>
        </>
    );
};

const MyUpload = (props) => {
    const propsMyupload = {
        // name: 'file',
        // action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
        // headers: {
        //     authorization: 'authorization-text',
        // },
        onChange(info) {
            props.onUploadPic(info.file) //ลืมอันนี้
            // if (info.file.status !== 'uploading') {
            //     console.log(info.file, info.fileList);
            // }
            // if (info.file.status === 'done') {
            //     message.success(`${info.file.name} file uploaded successfully`);
                
            // } else if (info.file.status === 'error') {
            //     message.error(`${info.file.name} file upload failed.`);
            // }
        },
    };
    return (
        <Upload {...propsMyupload}>
            <Button icon={<UploadOutlined />}>Upload</Button>
        </Upload>

        );

};

const App = (props) => {
    return (
        <Radio.Group onChange={props.onChangeCause} value={props.onInputCause} >
            <Radio value={1}>เสื่อมสภาพ</Radio>
            <Radio value={2}>สูญหาย</Radio>
        </Radio.Group>
    );
};

const { TextArea } = Input;

function onChange(date, dateString) {
    console.log(date, dateString);
}
const { Option } = Select;

function handleChange(value) {
    console.log(`selected ${value}`);
}



export default ReportFormPage;