import './App.css';
import React from "react";
import { useState } from 'react';
import 'antd/dist/antd.css';
import { DatePicker, Space } from 'antd';
import { Input } from 'antd';
import { Select } from 'antd';
import { Form, Button } from 'antd';
import { MinusCircleOutlined, PlusOutlined } from '@ant-design/icons';
import { Radio } from 'antd';
import { Upload,Modal,Result } from 'antd';
import { UploadOutlined } from '@ant-design/icons';
import { BrowserRouter as  Link } from "react-router-dom";


import { message } from 'antd';

const success = () => {
    message.success('แก้ไขข้อมูลสำเร็จ');
};

class MyUpload extends React.Component {
    state = {
        fileList: [],
    };

    handleChange = info => {
        let fileList = [...info.fileList];

        // 1. Limit the number of uploaded files
        // Only to show two recent uploaded files, and old ones will be replaced by the new
        fileList = fileList.slice(-2);

        // 2. Read from response and show file link
        fileList = fileList.map(file => {
            if (file.response) {
                // Component will show file.url as link
                file.url = file.response.url;
            }
            return file;
        });

        this.setState({ fileList });
    };

    render() {
        const props = {
            action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
            onChange: this.handleChange,
            multiple: true,
        };
        return (
            <Upload {...props} fileList={this.state.fileList}>
                <Button    icon={<UploadOutlined />}>Upload</Button>
            </Upload>
        );
    }
}

const App = () => {
    const [value, setValue] = React.useState(1);

    const onChange = e => {
        console.log('radio checked', e.target.value);
        setValue(e.target.value);
    };

    return (
        <Radio.Group onChange={onChange} value={value}   >
            <Radio value={1}>สำเร็จ</Radio>
            <Radio value={2}>พัสดุหมด</Radio>
        </Radio.Group>
    );
};

const { TextArea } = Input;

const areas = [
    { label: 'Beijing', value: 'Beijing' },
    { label: 'Shanghai', value: 'Shanghai' },
];

const sights = {
    Beijing: ['Tiananmen', 'Great Wall'],
    Shanghai: ['Oriental Pearl', 'The Bund'],
};

// const Demo = () => {
//     const [form] = Form.useForm();

//     const onFinish = values => {
//         console.log('Received values of form:', values);
//     };

//     const handleChange = () => {
//         form.setFieldsValue({ sights: [] });
//     };

//     return (
//         <Form form={form} name="dynamic_form_nest_item" onFinish={onFinish} autoComplete="off">
//             <Form.List name="sights">
//                 {(fields, { add, remove }) => (
//                     <>
//                         {fields.map(field => (
//                             <div>
//                                 <Form.Item
//                                     noStyle
//                                     shouldUpdate={(prevValues, curValues) =>
//                                         prevValues.area !== curValues.area || prevValues.sights !== curValues.sights
//                                     }
//                                 >
//                                     {() => (
//                                         <Form.Item

//                                         // {...field}
//                                         // label="Sight"
//                                         // name={[field.name, 'sight']}
//                                         // fieldKey={[field.fieldKey, 'sight']}
//                                         // rules={[{ required: true, message: 'Missing sight' }]}
//                                         >
//                                             <div className="Container">
//                                                 <div className="Container2">
//                                                     <div className="Reg_text4">รายการพัสดุ :</div>
//                                                     <Input size="large"    placeholder="กรอกรายการพัสดุ" />
//                                                     <div></div>
//                                                 </div>
//                                                 <div className="Container3">
//                                                     <div className="Reg_text4">จำนวนที่เบิก :</div>

//                                                     <Input size="large" style={{ width: 170, }} placeholder="กรอกจำนวน" />
//                                                     <div className="iconminus">
//                                                         <MinusCircleOutlined onClick={() => remove(field.name)} />
//                                                     </div>

//                                                     <div></div>

//                                                 </div>
//                                             </div>

//                                             <div className="Container">
//                                                 <div className="Container2">
//                                                     <div className="Reg_text4">หน่วย :</div>
//                                                     <Input size="large"    placeholder="กรอกหน่วยที่เบิก" />

//                                                 </div>
//                                                 <div className="Container3">
//                                                     <div className="Reg_text4">จำนวนที่จ่าย :</div>
//                                                     <Input size="large" style={{ width: 170, }} placeholder="กรอกจำนวน" />

//                                                     <div></div>


//                                                 </div>
//                                             </div>
//                                         </Form.Item>
//                                     )}
//                                 </Form.Item>
//                             </div>
//                         ))}

//                         <Form.Item>
//                             <div className="Container">
//                                 <div className="Container2">
//                                     <div></div>
//                                     <Button type="dashed" size="large"    onClick={() => add()} block icon=        {<PlusOutlined />}>
//                                         เพิ่มรายการ
//                                     </Button>
//                                 </div>
//                             </div>

//                         </Form.Item>
//                     </>
//                 )}
//             </Form.List>
//         </Form>
//     );
// };

function onChange(date, dateString) {
    console.log(date, dateString);
}

const { Option } = Select;


function handleChange(value) {
    console.log(`selected ${value}`);
}

function RequestEditPage() {

    const [inputName, setInputName] = useState('')
    // const [inputNumber, setInputNumber] = useState('')
    const [inputItem, setInputItem] = useState('')


    const [inputAmountAd, setInputAmountAd] = useState('')
    const [inputUnit, setInputUnit] = useState('0')
    const [inputAmount, setInputAmount] = useState('')

    const [statusItem, setStatusItem] = useState('')
    const [statusName, setStatusName] = useState('')
    // const [statusNumber, setStatusNumber] = useState({})

    const [statusUnit, setStatusUnit] = useState('')
    const [statusAmountAd, setStatusAmountAd] = useState('')
    const [statusAmount, setStatusAmount] = useState('')


    const [inputDate, setDate] = useState(1)
    const [inputCause, setCause] = useState(1)
    const [inputNote, setInputNote] = useState('')
    const [inputPic, setPic] = useState('')

    const commitdata = {

        name: inputName,
        // number: inputNumber,
        item: inputItem,
        amount: inputUnit,
        unit: inputAmountAd,
        date: inputDate,
        cause: inputCause,
        note: inputNote,
        pic: inputPic,
    }



    const itemHandler = (event) => {
        setInputItem(event.target.value)
    }

    const nameHandler = (event) => {
        setInputName(event.target.value)
    }

    // const numberHandler = (event) => {
    //     setInputNumber(event.target.value)
    // }



    const unitHandler = (event) => {
        // setInputUnit(event.target.value)
        console.log(event);
        setInputUnit(event)
    }

    const amountAdHandler = (event) => {
        setInputAmountAd(event.target.value)
    }

    const amountHandler = (event) => {
        setInputAmount(event.target.value)
    }

    const NoteHandler = (event) => {
        setInputNote(event.target.value)
    }

    const PicHandler = (event) => {
        setPic(event.target.value)
    }

    const inputDateHandler = (event) => {
        console.log('date checked', event.target.value);
        setDate(event.target.value);
    }

    const inputCauseHandler = (event) => {
        console.log('radio checked', event.target.value);
        setCause(event.target.value);
    };



    const reportHandler = () => {


        // var check = false

        if (inputItem.trim().length !== 0 && inputName.trim().length != 0 && inputUnit != "0"
            && inputAmount.trim().length != 0 && inputAmountAd.trim().length != 0
        ) {
            setStatusItem('')
            setStatusAmount('')
            setStatusUnit('')
            setStatusAmountAd('')
            setStatusName('')
            // setInputAmountAd('')
            // setInputName('')
            success()
            console.log(commitdata);
            return true


            // console.log("error");
            // setStatusItem("error")
        } else {
            // check = false

            if (inputItem.trim().length == 0) {
                setStatusItem("error")
            } else {
                setStatusItem('')
            }

            if (inputName.trim().length == 0) {
                // console.log("error");
                setStatusName("error")
            } else {
                setStatusName('')
            }


            if (inputUnit == "0") {
                // console.log("error");
                setStatusUnit("error")
            } else {
                setStatusUnit('')
            }


            if (inputAmountAd.trim().length == 0) {
                
                setStatusAmountAd("error")
            } else {
                setStatusAmountAd('')
            }


            if (inputAmount.trim().length == 0) {
                
                setStatusAmount("error")
            } else {
                setStatusAmount('')
            }
            return false
        }

    }

    return (
        <div className="Appmiw">
            <div className="Head2">แก้ไขประวัติการเบิกพัสดุ</div>
            <div className="font">

                <div className="Container">
                    <div className="Container2">
                        <div className="Reg_text4">วันที่เบิก :</div>
                        <DatePicker size="large" onChange={onChange} placeholder="เลือกวันที่" />
                        <div></div>
                    </div>
                    <div>

                    </div>
                </div>

                <div className="Container">
                    <div className="Container2">
                        <div className="Reg_text4">ผู้ขอเบิก :</div>
                        <Form.Item validateStatus={`${statusName}`}>
                            <Input size="large" placeholder="กรอกชื่อผู้เบิก" onChange={nameHandler} />
                        </Form.Item>
                        <div></div>
                    </div>
                    <div className="Container3">
                        <div className="Reg_text4">ตำแหน่ง :</div>
                        <Select defaultValue="อาจารย์" size="large" style={{ width: 170 }} onChange={handleChange} disabled>
                            <Option value="jack">อาจารย์</Option>
                            <Option value="lucy">บุคลากร</Option>
                            <Option value="lucy">เจ้าหน้า</Option>
                            <Option value="lucy">แม่บ้าน</Option>
                            <Option value="lucy">คนสวน</Option>
                        </Select>
                    </div>
                </div>


                <div className="Container">
                    <div className="Container2">
                        <div className="Reg_text4">เบอร์ติดต่อ :</div>
                        <Input size="large" placeholder="0812345678" disabled />
                        <div></div>
                    </div>
                    <div>

                    </div>
                </div>


                <div className="Container">
                    <div className="Container2">
                        <div className="Reg_text4">รายการพัสดุ :</div>
                        <Form.Item validateStatus={`${statusItem}`}>
                            <Input size="large" placeholder="กรอกรายการพัสดุ" onChange={itemHandler} />
                        </Form.Item>
                        <div></div>
                    </div>
                    <div className="Container3">
                        <div className="Reg_text4">จำนวนที่เบิก :</div>
                        <Form.Item validateStatus={`${statusAmountAd}`}>
                            <Input size="large" style={{ width: 170, }} placeholder="กรอกจำนวน" onChange={amountAdHandler} />
                        </Form.Item>
                        <div></div>


                    </div>
                </div>

                <div className="Container">
                    <div className="Container2">
                        <div className="Reg_text4">หน่วย :</div>
                        <Form.Item validateStatus={`${statusUnit}`}>
                            <Select defaultValue="1" size="large" placeholder="เลือกหน่วย" onChange={unitHandler} value={inputUnit} >
                                <Option disabled value="0" key="0">เลือกหน่วย</Option>
                                <Option value="1">ชิ้น</Option>
                                <Option value="2">กล่อง</Option>
                            </Select>
                        </Form.Item>
                    </div>
                    <div className="Container3">
                        <div className="Reg_text4">จำนวนที่จ่าย :</div>
                        <Form.Item validateStatus={`${statusAmount}`}>
                            <Input size="large" style={{ width: 170, }} placeholder="กรอกจำนวน" onChange={amountHandler} />
                        </Form.Item>
                        <div></div>

                    </div>
                </div>


                {/* <Demo /> */}
                
                <div className="Container">
                    <div className="Container2">
                        <div className="Reg_text4">เพื่อนำไปใช้ :</div>
                        <TextArea rows={4} onChange={NoteHandler}/>
                        <div></div>
                    </div>
                    <div></div>
                </div>

                <div className="Container">
                    <div className="Container2">
                        <div className="Reg_text4">สถานะ :</div>
                        <div className="padright">
                            <App onChangeCause={inputCauseHandler} onInputCause={inputCause}/>
                        </div>

                    </div>
                </div>

                <div className="Container">
                    <div className="Container2">
                        <div className="Reg_text4">แนบรูปใบเบิก :</div>
                        <MyUpload onUploadPic={setPic}/>

                    </div>
                </div>

                {/* <Space>
                    <Button type="primary" onClick={success}>บันทึก</Button>
                </Space> */}

                <App2 onClickReport={reportHandler}
                    onText={statusItem, statusUnit, statusAmount, statusAmountAd, statusName} />


            </div>

        </div>
    )
}

const App2 = (props) => {
    const [isModalVisible, setIsModalVisible] = useState(false);

    const showModal = () => {
        // setIsModalVisible(true);
        props.onClickReport()
    };

    const handleOk = () => {
        setIsModalVisible(false);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };

    return (
        <>
            <Button type="primary" onClick={showModal}> บันทึก </Button>
            {/* <Modal title="" footer={null} visible={isModalVisible} onOk={handleOk} onCancel={handleCancel} width={600}>
                <div className="font">
                    <Result
                        status="success"
                        title="แก้ไขรายการสำเร็จ"
                        subTitle="สามารถตรวจสอบการบันทึกได้ที่หน้าประวัติการเบิกพัสดุ"
                        extra={[
                            <Link to="/Dashboard"><Button key="console"> กลับหน้าหลัก </Button></Link>

                        ]}
                    />
                </div>
            </Modal> */}
        </>
    );
};

export default RequestEditPage;