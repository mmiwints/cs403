import './App.css';
import React from "react";
import 'antd/dist/antd.css';
import { Result, Button } from 'antd';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";


function FiscalSuccessPage() {
    return (
        <div className="Appmiw">
            <div className="font">
                <br></br>
                <br></br>
                <br></br>
                <br></br>
                <Result
                    status="success"
                    title="อนุมัติรายการขอเพิ่มพัสดุสำเร็จ"
                    subTitle="หากพัสดุจัดส่งแล้วสามารถอัพเดทสถานะพัสดุได้ที่หน้ารายการขอเพิ่มของพัสดุนั้นๆ"
                    extra={[
                        <Link to="/2"><Button key="console"> กลับหน้าหลัก </Button></Link>,
                        
                    ]}
                />,
            </div>













        </div>
    )
}

export default FiscalSuccessPage;