import './App.css';
import { AutoComplete, Input, Button, Select, Tag, Table } from 'antd';
import React, { useState } from "react";
import 'antd/dist/antd.css';
import pic2 from './img/pic.jpg'
import { DownloadOutlined } from '@ant-design/icons';
import tapewhite from '../Img/tapewhite.jpg';
import tape from '../Img/tape1n.jpg';
import tapered1n from '../Img/tapered1n.jpg';
import tapered2n from '../Img/tapered2n.jpg';
import tapeblack1n from '../Img/tapeblack1n.jpg';
import ReactExport from "react-export-excel";

import { BrowserRouter as Router, Switch, Route, Link, useHistory } from "react-router-dom";


function handleChange(value) {
  console.log(`selected ${value}`);
}

const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

const columnsTable = [
  {
    title: 'วันที่',
    dataIndex: 'dateRequest',
    key: 'dateRequest',
  },
  {
    title: 'รายการ',
    dataIndex: 'Supplies',
    key: 'Supplies',
  },
  {
    title: 'ชื่อผู้เบิก',
    dataIndex: 'staff',
    key: 'staff',
  },
  {
    title: 'สถานะ',
    key: 'tags',
    dataIndex: 'tags',
    render: tags => (
      <>
        {tags.map(tag => {
          let color = tag.length > 6 ? 'volcano' : 'green';

          return (
            <Tag color={color} key={tag}>
              {tag.toUpperCase()}
            </Tag>
          );
        })}
      </>
    ),
  },

];

const dataSource = [
  {
    key: '1',
    dateRequest: '19 สิงหาคม 2021',
    staff: 'ธนาภรณ์ ด่านชาญชัย',
    position: 'บุคคลากร',
    tel: '0812345678',
    tags: ['พัสดุหมด'],

    Supplies: 'น้ำยาลบคำผิด (แบบแท่ง)',
    amountRequest: '2',
    amountGive: '2',
    unit: 'ชิ้น',

    Supplies2: 'ลวดเสียบกระดาษ',
    amountRequest2: '5',
    amountGive2: '2',
    unit2: 'กล่อง',

    src: pic2,
    update_at: '23 สิงหาคม 2564'
  },

  {
    key: '2',
    dateRequest: '20 เม.ย. 64',
    staff: 'รัตนากร จงรวยกลาง',
    position: 'บุคคลากร',
    tags: ['สำเร็จ'],


    Supplies: 'เทปผ้าติดสันหนังสือ 2 นิ้ว (สีแดง)',
    amountRequest: '1',
    amountGive: '1',
    unit: 'ชิ้น',

    src: pic2,
    update_at: '20 สิงหาคม 2564'

  },

  {
    key: '3',
    dateRequest: '20 เม.ย. 64',
    staff: 'ชนิกานต์ ลี้ผดุง',
    position: 'คนสวน',
    tags: ['สำเร็จ'],

    Supplies: 'น้ำยาล้างจาน',
    amountRequest: '1',
    amountGive: '1',
    unit: 'ชิ้น',

    src: tapered1n,
    update_at: '20 สิงหาคม 2564'

  },

  // {
  //   key: '4',
  //   dateRequest: '17 เม.ย. 64',
  //   Supplies: 'เทปผ้าติดสันหนังสือ 1 นิ้ว (สีน้ำเงิน)',
  //   staff: 'สุชานันท์ พงษ์ประดิษฐ',
  //   tags: ['พัสดุหมด'],
  //   src: tape,
  //   // tag: 'ST0004',
  //   lot: 'A0001',
  //   amount: '2',
  //   lot2: 'A0002',
  //   amount2: '16',
  //   category: 'วัสดุสำนักงาน',
  //   unit: 'ม้วน',
  //   last_recive: '7 มีนาคม 2564',
  //   broken: '0',
  //   remain: 18,
  // },

];

// const dataSource2 = dataSource.map((value) => {
//   return {
//     key: value.key,
//     date: value.date,
//     Supplies: value.Supplies,
//     staff: value.staff,
//     tags: value.tags,
//   }
// })

// console.log(dataSource2)


function Request() {
  const data = [
    {
      key: '1',
      dateRequest: '19 สิงหาคม 2021',
      staff: 'ธนาภรณ์ ด่านชาญชัย',
      position: 'บุคคลากร',
      tel: '0812345678',
      tags: ['พัสดุหมด'],

      Supplies: 'น้ำยาลบคำผิด (แบบแท่ง)',
      amountRequest: '2',
      amountGive: '2',
      unit: 'ชิ้น',

      Supplies: 'ลวดเสียบกระดาษ',
      amountRequest: '5',
      amountGive: '2',
      unit: 'กล่อง',

      src: pic2,
      update_at: '23 สิงหาคม 2564'
    },

    // {
    //   key: '2',
    //   dateRequest: '20 เม.ย. 64',
    //   staff: 'รัตนากร จงรวยกลาง',
    //   position: 'บุคคลากร',
    //   tags: ['สำเร็จ'],


    //   Supplies: 'เทปผ้าติดสันหนังสือ 2 นิ้ว (สีแดง)',
    //   amountRequest: '1',
    //   amountGive: '1',
    //   unit: 'ชิ้น',

    //   src: pic2,
    //   update_at: '20 สิงหาคม 2564'

    // },

    // {
    //   key: '3',
    //   dateRequest: '20 เม.ย. 64',
    //   staff: 'ชนิกานต์ ลี้ผดุง',
    //   position: 'คนสวน',
    //   tags: ['สำเร็จ'],

    //   Supplies: 'น้ำยาล้างจาน',
    //   amountRequest: '1',
    //   amountGive: '1',
    //   unit: 'ชิ้น',

    //   src: tapered1n,
    //   update_at: '20 สิงหาคม 2564'

    // },
  ]

  const [search, setSearch] = useState(dataSource)

  const onSearch = (value) => {
    setSearch(dataSource.filter((item) => {
      return item.Supplies.includes(value)
    }))
    console.log(search)
  };

  const optionsMock = data.map((value) => {
    return { value: value.Supplies }
  })

  console.log(optionsMock)
  const { Search } = Input;
  const history = useHistory()

  const filterColumns = (optionsMock) => {
    // Get column names
    const columns = Object.keys(optionsMock[0]);

    // Remove by key (firstname)
    const filterColsByKey = columns.filter(c => c !== 'src' && c !== 'tag');

    // OR use the below line instead of the above if you want to filter by index
    // columns.shift()

    return filterColsByKey // OR return columns
  };

  const camelCase = (str) => {
    return str.substring(0, 1).toUpperCase() + str.substring(1);
  };


  return (
    <div className="Appmiw">
      <div className="Head1">ประวัติการเบิกพัสดุ</div>
      <div className="Fillter">
        <AutoComplete
          options={optionsMock}
          filterOption={(inputValue, option) =>
            option.value.toUpperCase().indexOf(inputValue.toUpperCase()) !== -1
          }
        ><Search placeholder="ค้นหาชื่อพัสดุหรือชื่อผู้เบิก" onSearch={onSearch} size="large" style={{ width: 350 }} /></AutoComplete>

        <div className="Reg_text"> เดือน :</div>

        <Select className="Drop_text" defaultValue="มกราคม" size="large" style={{ width: 150, }} onChange={handleChange}>
          <option value="01">มกราคม</option>
          <option Value="02">กุมภาพันธ์</option>
          <option value="03">มีนาคม</option>
          <option value="04">เมษายน</option>
          <option value="05">พฤษภาคม</option>
          <option value="06">มิถุนายน</option>
          <option value="07">กรกฎาคม</option>
          <option value="08">สิงหาคม</option>
          <option value="09">กันยายน</option>
          <option value="10">ตุลาคม</option>
          <option value="11">พฤศจิกายน</option>
          <option value="12">ธันวาคม</option>
        </Select>

        <div className="Reg_text"> ปี :</div>

        <Select defaultValue="2564" size="large" style={{ width: 150 }} onChange={handleChange}>

          <option value="2564">2564</option>
          <option value="2563">2563</option>
          <option value="2562">2562</option>
          <option value="2561">2561</option>
          <option value="2560">2560</option>
          <option value="2559">2559</option>
          <option value="2558">2558</option>

        </Select>

        {/* <div className="pad2"></div>

        <ExcelFile filename="test"
          element={(<Button bsStyle="info" size="large" icon={<DownloadOutlined />}>  นำออก (Excel) </Button>)}>
          <ExcelSheet data={search} name="Test">
            {
              filterColumns(search).map((col) => {
                return <ExcelColumn label={camelCase(col)} value={col} />
              })
            }
          </ExcelSheet>
        </ExcelFile> */}



      </div>
      <Table
        onRow={(record, rowIndex) => {
          console.log(rowIndex)
          return {
            onClick: event => {

              history.push(`/RequestRecPage/${dataSource[rowIndex].key}`)

            }
          };
        }}

        columns={columnsTable}
        dataSource={search}
      // pagination={{
      //   pageSize: 4,
      // }}



      />

    </div>
  )
}

export default Request;
