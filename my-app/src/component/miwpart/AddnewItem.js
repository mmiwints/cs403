
import { Checkbox } from 'antd';
import './App.css';
import 'antd/dist/antd.css';
import { DatePicker, Form } from 'antd';
import { Input } from 'antd';
import { Select } from 'antd';
import { Radio } from 'antd';
import React, { useState } from 'react';
import { Upload } from 'antd';
import ImgCrop from 'antd-img-crop';
import { Modal, Button } from 'antd';
import { Result } from 'antd';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import { Tooltip } from 'antd';
import { InfoCircleOutlined, UserOutlined } from '@ant-design/icons';


function onChangeDate(date, dateString) {
    console.log(date, dateString);
}

function onChange(e) {
    console.log(`checked = ${e.target.checked}`);
}

const PercelPic = () => {
    const [fileList, setFileList] = useState([
        //   {
        //     uid: '-1',
        //     name: 'image.png',
        //     status: 'done',
        //     url: 'https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png',
        //   },
    ]);

    const onChange = ({ fileList: newFileList }) => {
        setFileList(newFileList);
    };

    const onPreview = async file => {
        let src = file.url;
        if (!src) {
            src = await new Promise(resolve => {
                const reader = new FileReader();
                reader.readAsDataURL(file.originFileObj);
                reader.onload = () => resolve(reader.result);
            });
        }
        const image = new Image();
        image.src = src;
        const imgWindow = window.open(src);
        imgWindow.document.write(image.outerHTML);

    };

    return (
        <ImgCrop rotate >
            <Upload

                action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
                listType="picture-card"
                fileList={fileList}
                onChange={onChange}
                onPreview={onPreview}
            >
                {fileList.length < 1 && '+ Upload'}
            </Upload>
        </ImgCrop>
    );
};

const App = () => {
    const [value, setValue] = React.useState(1);

    const onChange = e => {
        console.log('radio checked', e.target.value);
        setValue(e.target.value);
    };

    return (
        <Radio.Group onChange={onChange} value={value} style={{ width: 280, }}>
            <Radio value={1}>สำเร็จ</Radio>
            <Radio value={2}>พัสดุหมด</Radio>
        </Radio.Group>
    );
};

const { TextArea } = Input;
const { Option } = Select;



function AddnewItemPage() {
    const [form] = Form.useForm()
    const onFinish = values => {
        console.log('Received values of form:', values);
        showModal()
    };

    const [isModalVisible, setIsModalVisible] = useState(false);

    const showModal = () => {
        setIsModalVisible(true);
    };

    const handleOk = () => {
        setIsModalVisible(false);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };

    const [disabled, setDisabled] = useState(false);


    return (
        <Form form={form} name="dynamic_form_nest_item" onFinish={onFinish} >
            <Form.Item rules={[{ required: true }]}>
                <div className="Appmiw">
                    <div className="Head2">เพิ่มพัสดุใหม่</div>
                    <div className="font">

                        <div className="Container">
                            <div className="Container5">
                                <div className="Reg_text4">วันที่รับเข้า :</div>
                                <Form.Item
                                    rules={[{ required: true }]}
                                    name='InputDate'
                                    fieldKey='InputDate_key'
                                >
                                    <DatePicker size="large" style={{ width: 280 }} onChange={onChangeDate} placeholder="เลือกวันที่" />
                                </Form.Item>
                                <div></div>
                            </div>
                        </div>

                        <div className="Container">
                            <div className="Container5">
                                <div className="Reg_text4">ชื่อพัสดุ :</div>

                                <Form.Item
                                    name='suppiles'
                                    fieldKey='suppiles_key'
                                    rules={[{ required: true, message: '' }]}
                                >
                                    <Input size="large" placeholder="กรอกชื่อพัสดุ" />
                                </Form.Item>
                                <div></div>
                            </div>
                            <div className="Container3">
                                <div className="Reg_text4">รหัสพัสดุ :</div>
                                <Form.Item
                                    name='suppilesID'
                                    fieldKey='suppilesID_key'
                                >
                                    <Input size="large" style={{ width: 170, }} placeholder="ST106" disabled />
                                </Form.Item>
                            </div>
                        </div>

                        <div className="Container">
                            <div className="Container5">
                                <div className="Reg_text4">หน่วยที่จ่าย :</div>

                                <Form.Item
                                    name='unitGive'
                                    fieldKey='unitGive_key'
                                    rules={[{ required: true, message: '' }]}
                                >
                                    <Input
                                        size="large"
                                        placeholder="กรอกหน่วยที่จ่าย"
                                        suffix={
                                            <Tooltip title="เช่น ด้าม, แพ็ค, ชิ้น, อัน ">
                                                <InfoCircleOutlined style={{ color: 'rgba(0,0,0,.45)' }} />
                                            </Tooltip>
                                        }
                                    />
                                </Form.Item>

                                <div></div>
                            </div>
                            <div className="Container3">
                                <div className="Reg_text4">จำนวนชิ้น :</div>
                                <Form.Item
                                    name='amountGive'
                                    fieldKey='amountGive_key'
                                    rules={[{ required: true, message: '' }]}
                                >

                                    <Input size="large" style={{ width: 170, }} placeholder="กรอกจำนวน" />
                                </Form.Item>
                            </div>
                        </div>

                        <div className="Container">
                            <div className="Container5">
                                <div className="Reg_text4">หน่วยที่รับเข้า :</div>

                                <Form.Item
                                    name='unitReceive'
                                    fieldKey='unitReceive_key'
                                    // rules={[{ required: true, message: '' }]}
                                >
                                    <Input
                                        size="large"
                                        placeholder="กรอกหน่วยรับเข้า"
                                        suffix={
                                            <Tooltip title="เช่น กล่อง, แพ็ค, โหล, รีม ">
                                                <InfoCircleOutlined style={{ color: 'rgba(0,0,0,.45)' }} />
                                            </Tooltip>
                                        }
                                        disabled = {disabled}
                                    />
                                </Form.Item>
                                <div></div>
                            </div>

                            <div className="Container3">
                                <div className="Reg_text4">จำนวนชิ้น :</div>

                                <Form.Item
                                    name='amountReceive'
                                    fieldKey='amountReceive_key'
                                    // rules={[{ required: true, message: '' }]}
                                >
                                    <Input size="large" style={{ width: 170, }} placeholder="กรอกจำนวน" disabled = {disabled}/>
                                </Form.Item>
                            </div>
                        </div>

                        <div className="Container">
                            <div className="Container5">
                                <div></div>
                                <Form.Item>
                                    <Checkbox className="Reg_text4" onClick={()=> setDisabled(true)}>
                                        ใช้หน่วยเดียวกัน
                                    <div className="pad3"></div>
                                    
                                    </Checkbox>
                                </Form.Item>

                            </div>

                        </div>

                        <div className="Container">
                            <div className="Container5">
                                <div className="Reg_text4">จำนวนพัสดุ :</div>
                                <Form.Item
                                    name='amount'
                                    fieldKey='amount_key'
                                    rules={[{ required: true, message: '' }]}
                                >
                                    <Input size="large" style={{ width: 280, }} placeholder="กรอกจำนวน" />
                                </Form.Item>
                                <div></div>
                            </div>

                            <div className="Container3">
                                <div className="Reg_text4">หน่วย :</div>

                                <Form.Item
                                    name='unit'
                                    fieldKey='unit_key'
                                    rules={[{ required: true, message: '' }]}
                                >
                                    <Select defaultValue="ชิ้น" size="large" style={{ width: 170, }} placeholder="เลือกหน่วย" >
                                        <Option disabled value="0" key="0">เลือกหน่วย</Option>
                                        <Option value="1">ชิ้น</Option>
                                        <Option value="2">กล่อง</Option>
                                    </Select>
                                </Form.Item>
                            </div>


                        </div>

                        <div className="Container">
                            <div className="Container5">
                                <div className="Reg_text4">แจ้งพัสดุใกล้หมด :</div>
                                <Form.Item
                                    name='almostOut'
                                    fieldKey='almostOut_key'
                                    rules={[{ required: true, message: '' }]}
                                >
                                    <Input
                                        size="large"
                                        placeholder="กรอกจำนวน"
                                        suffix={
                                            <Tooltip title="กรอกจำนวนพัสดุคงเหลือขั้นต่ำที่ต้องการเพื่อแจ้งเตือนพัสดุใกล้หมด">
                                                <InfoCircleOutlined style={{ color: 'rgba(0,0,0,.45)' }} />
                                            </Tooltip>
                                        }
                                    />
                                </Form.Item>
                                <div></div>
                            </div>
                        </div>


                        {/* มันยังเคลื่อนอยู่ */}
                        <div className="Container">
                            <div className="Container5">
                                <div className="Reg_text4">รูปพัสดุ :</div>
                                <Form.Item
                                    name='pic'
                                    fieldKey='pic_key'
                                // rules={[{ required: true, message: '' }]}
                                >
                                    <PercelPic />
                                </Form.Item>

                            </div>
                        </div>

                        <Form.Item>
                            <Button type="primary" htmlType="submit">
                                เพิ่มพัสดุ
                            </Button>
                            <Modal title="" footer={null} visible={isModalVisible} onOk={handleOk} onCancel={handleCancel} width={600}>
                                <div className="font">
                                    <Result
                                        status="success"
                                        title="เพิ่มพัสดุใหม่สำเร็จ"
                                        subTitle="สามารถตรวจสอบการเพิ่มพัสดุได้ที่หน้ารายการพัสดุ"
                                        extra={[
                                            <Link to="/Dashboard"><Button key="console"> กลับหน้าหลัก </Button></Link>

                                        ]}
                                    />
                                </div>
                            </Modal>
                        </Form.Item>

                    </div>

                </div>


            </Form.Item>
        </Form>

    )
}

export default AddnewItemPage;