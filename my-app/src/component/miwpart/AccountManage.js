import './App.css';
import { AutoComplete } from 'antd';
import React, { useEffect, useState } from "react";
import 'antd/dist/antd.css';
import { Select, Space } from 'antd';
import { Table, Input, InputNumber, Popconfirm, Form, Typography } from 'antd';
import { DeleteOutlined, EditOutlined } from '@ant-design/icons';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import { Button } from 'antd';
import { UserAddOutlined } from '@ant-design/icons';
import { Modal } from 'antd';
import { EditableProTable } from '@ant-design/pro-table';
import { ExclamationCircleOutlined } from '@ant-design/icons';

const originData = [
    {
        key: '1',
        name: 'ธนาภรณ์ ด่านชาญชัย',
        email: 'chaki@staff.tu.ac.th',
        position: 'เจ้าหน้าที่พัสดุ',
    },

    {
        key: '2',
        name: 'รัตนากร จงรวยกลาง',
        email: 'jnolan@staff.tu.ac.th',
        position: 'เจ้าหน้าที่การคลัง',
    },

    {
        key: '3',
        name: 'ขนิกานต์ ลี้ผดุง',
        email: 'sarahs@hotmail.com',
        position: 'แม่บ้าน',
    },
];

const { Option } = Select;

const EditableCell = ({
    editing,
    dataIndex,
    title,
    inputType,
    record,
    index,
    children,
    ...restProps
}) => {
    const inputNode = title === 'ตำแหน่ง' ? <Select size="normal" onChange={handleChange}>
        <Option value="อาจารย์">อาจารย์</Option>
        <Option value="บุคลากร">บุคลากร</Option>
        <Option value="เจ้าหน้าที่">เจ้าหน้าที่</Option>
        <Option value="แม่บ้าน">แม่บ้าน</Option>
        <Option value="คนสวน">คนสวน</Option>
    </Select> : <Input />;
    return (
        <td {...restProps}>
            {editing ? (
                <Form.Item
                    name={dataIndex}
                    style={{
                        margin: 0,
                    }}
                    rules={[
                        {
                            required: true,
                            message: `กรุณากรอก ${title}!`,
                        },
                    ]}
                >
                    {inputNode}
                </Form.Item>
            ) : (
                    children
                )}
        </td>
    );
};

const EditableTable = (props) => {
    const [form] = Form.useForm();

    const [data, setData] = useState(props.data);
    const [editingKey, setEditingKey] = useState('');

    const isEditing = (record) => record.key === editingKey;

    useEffect(() => {
        setData(props.data)
    }, [props.data])

    const edit = (record) => {
        form.setFieldsValue({
            name: '',
            email: '',
            position: '',
            ...record,
        });
        setEditingKey(record.key);
    };

    const cancel = () => {
        setEditingKey('');
    };

    const save = async (key) => {
        try {
            const row = await form.validateFields();
            const newData = [...data];
            const index = newData.findIndex((item) => key === item.key);

            if (index > -1) {
                const item = newData[index];
                newData.splice(index, 1, { ...item, ...row });
                setData(newData);
                setEditingKey('');
            } else {
                newData.push(row);
                setData(newData);
                setEditingKey('');
            }

            console.log(newData);
        } catch (errInfo) {
            console.log('Validate Failed:', errInfo);
        }

    };

    const { confirm } = Modal;




    const columns = [
        {
            title: 'ชื่อ',
            dataIndex: 'name',
            key: 'name',
            editable: true,
        },
        {
            title: 'อีเมล/ชื่อผู้ใช้',
            dataIndex: 'email',
            key: 'email',
            editable: true,
        },
        {
            title: 'ตำแหน่ง',
            dataIndex: 'position',
            key: 'position',
            editable: true,
        },
        {
            title: '',
            dataIndex: '',
            render: (_, record) => {
                const editable = isEditing(record);
                return editable ? (
                    <span>
                        <a
                            href="javascript:;"
                            onClick={() => save(record.key)}
                            style={{
                                marginRight: 8,
                                color : '#32A05F',
                            }}

                            
                        >
                            บันทึก
                        </a>
                        <Popconfirm title="การแก้ไขยังไม่ถูกบันทึกต้องการยกเลิกใช่หรือไม่" onConfirm={cancel} okText="ใช่" cancelText="ยกเลิก" style={{ font : 'Prompt'}}>
                            <a style={{ color : '#6b6b6b'}}>ยกเลิก</a>
                        </Popconfirm>
                    </span>
                ) : (
                        <div>
                            <Typography.Link disabled={editingKey !== ''} onClick={() => edit(record)}>
                                <EditOutlined />
                            </Typography.Link>

                            <div className="pad2"></div>

                            <DeleteOutlined onClick={() => {
                                confirm({
                                    title: 'คุณแน่ใจหรือไม่ว่าต้องการลบบัญชีผู้ใช้',
                                    icon: <ExclamationCircleOutlined />,
                                    // content: 'รายการนี้จะถูกลบถาวร',
                                    okText: 'ลบ',
                                    okType: 'danger',
                                    cancelText: 'ยกเลิก',
                                    onOk() {
                                        console.log(record.key);
                                        setData(data.filter(value => value.key != record.key))
                                    },
                                    onCancel() {
                                        console.log('Cancel');
                                    },
                                });
                            }} />
                        </div>


                    );
            },
        },
    ];


    const mergedColumns = columns.map((col) => {
        if (!col.editable) {
            return col;
        }

        return {
            ...col,
            onCell: (record) => ({
                record,
                inputType: col.dataIndex === 'age' ? 'number' : 'text',
                dataIndex: col.dataIndex,
                title: col.title,
                editing: isEditing(record),
            }),
        };
    });
    return (
        <Form form={form} component={false}>
            <Table
                components={{
                    body: {
                        cell: EditableCell,
                    },
                }}
                // bordered
                dataSource={data}
                columns={mergedColumns}
                rowClassName="editable-row"
                pagination={false}
            // pagination={{
            //     onChange: cancel,
            // }}

            />
        </Form>
    );
};

class ButtonSize extends React.Component {
    state = {
        size: 'large',
    };

    handleSizeChange = e => {
        this.setState({ size: e.target.value });
    };

    render() {
        const { size } = this.state;
        return (
            <>
                <Link to="/AddnewAccountPage">
                    <Button style={{ width: 160 }} type="primary" shape="round" icon={<UserAddOutlined />} size={size}>
                        เพิ่มบัญชีใหม่
                    </Button>
                </Link>
            </>
        );
    }
}



function handleChange(value) {
    console.log(`selected ${value}`);
}





function AccountManagePage() {

    const data = [
        {
            key: '1',
            name: 'ธนาภรณ์ ด่านชาญชัย',
            email: 'chaki@staff.tu.ac.th',
            position: 'เจ้าหน้าที่พัสดุ',
        },

        {
            key: '2',
            name: 'รัตนากร จงรวยกลาง',
            email: 'jnolan@staff.tu.ac.th',
            position: 'เจ้าหน้าที่การคลัง',
        },

        {
            key: '3',
            name: 'ขนิกานต์ ลี้ผดุง',
            email: 'sarahs@hotmail.com',
            position: 'แม่บ้าน',
        },
    ];

    const [search, setSearch] = useState(data)
    const [idCheck, setIdCheck] = useState('')



    const onSearch = (value) => {
        setSearch(data.filter((item) => {
            return item.name.includes(value)
        }))
        console.log(search)
    };

    const optionsMock = data.map((value) => {
        return { value: value.name }
    })
    // console.log(optionsMock)

    const { Search } = Input;


    return (
        <div className="Appmiw">
            <div className="Head1">จัดการบัญชีผู้ใช้</div>
            <div className="itemList-grid">
                <div className="Fillter">
                    <AutoComplete
                        options={optionsMock}
                        filterOption={(inputValue, option) =>
                            option.value.toUpperCase().indexOf(inputValue.toUpperCase()) !== -1
                        }
                    ><Search placeholder="ค้นหาชื่อ" onSearch={onSearch} size="large" style={{ width: 350 }} /></AutoComplete>
                </div>
                <div className="Fillter2">
                    <ButtonSize />
                </div>

            </div>

            {/* <Table columns={columns} dataSource={search} /> */}
            <EditableTable data={search} />
        </div>

    )
}

export default AccountManagePage;