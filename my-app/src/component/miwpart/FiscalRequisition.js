import './App.css';
import React from "react";
import 'antd/dist/antd.css';
import { Input, Space } from 'antd';
import { Select } from 'antd';
import { Table, Tag } from 'antd';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
// import './App.less';

// const styles = StyleSheet.create({
//   container: {
//     justifyContent: 'center',
//     alignItems: 'center',
//     backgroundColor: '#32A05F',
//     width: '100%',
//   },
// })


const { Search } = Input;

const { Option } = Select;

function handleChange(value) {
    console.log(`selected ${value}`);
}

const onSearch = value => console.log(value);

const columns = [
    {
        title: 'วันที่',
        dataIndex: 'date',
        key: 'date',
        render: text => <div className="table_text">{text}</div>,
    },
    {
        title: 'รายการ',
        dataIndex: 'Supplies',
        key: 'Supplies',
        // render: (text, record) => {
        //     return (
        //       <Link to="/ApprovePage">
        //       <div className="table_text" style={{ color:" #191919" }}>
        //         <div className="pad1">{text}</div>
        //       </div>
        //       </Link>

        //     );
        //   },
    },
    {
        title: 'จำนวน',
        dataIndex: 'amount',
        key: 'amount',
        // render: text => <div className="table_text">{text}</div>,
    },
    {
        title: 'หน่วย',
        dataIndex: 'unit',
        key: 'unit',
        render: text => <div className="table_text">{text}</div>,
    },
    {
        title: 'สถานะ',
        key: 'tags',
        dataIndex: 'tags',
        render: tags => (
            <>
                {tags.map(tag => {
                    let color = tag.length > 6 ? 'geekblue' : 'green';
                    if (tag === 'รออนุมัติ') {
                        color = 'volcano';
                    }

                    return (
                        <Tag color={color} key={tag}>
                            {tag.toUpperCase()}
                        </Tag>
                    );
                })}
            </>
        ),
    },

];

const data = [
    {
        key: '1',
        date: '24 เม.ย. 64',
        Supplies: <Link to="/ApprovePage">
            <div className="textcolorblack">
                น้ำยาลบคำผิด (แบบแท่ง)</div></Link>,
        amount: 50,
        unit: 'ม้วน',
        tags: ['รออนุมัติ'],

    },

    {
        key: '2',
        date: '20 เม.ย. 64',
        Supplies: 'กระดาษกาว  1  นิ้ว (สีขาว)',
        amount: 30,
        unit: 'ม้วน',
        tags: ['สำเร็จ'],
    },

    {
        key: '3',
        date: '20 เม.ย. 64',
        Supplies: <Link to="/StatusPage">
            <div className="textcolorblack">
                ลวดเสียบกระดาษ</div></Link>,
        amount: 20,
        unit: 'กล่อง',
        tags: ['กำลังดำเนินการ'],
    },

    {
        key: '4',
        date: '17 เม.ย. 64',
        Supplies: 'เทปผ้าติดสันหนังสือ 1 นิ้ว (สีน้ำเงิน)',
        amount: 50,
        unit: 'ม้วน',
        tags: ['สำเร็จ'],
    },

    {
        key: '4',
        date: '16 เม.ย. 64',
        Supplies: 'ดินสอดำ',
        amount: 30,
        unit: 'กล่อง',
        tags: ['สำเร็จ'],
    },

    {
        key: '5',
        date: '15 เม.ย. 64',
        Supplies: 'น้ำยาลบคำผิด (แบบแท่ง)',
        amount: 10,
        unit: 'ชิ้น',
        tags: ['สำเร็จ'],
    },

];


function FiscalRequisitionPage() {
    return (
        <div className="Appmiw">
            <div className="Head1">รายการขอเพิ่มพัสดุ</div>
            <br></br>

            <Table
                columns={columns}
                dataSource={data}
                pagination={{
                    pageSize: 4,
                }}
            >

            </Table>

        </div>
    )
}

export default FiscalRequisitionPage;