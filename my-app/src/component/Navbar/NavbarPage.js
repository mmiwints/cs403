import React from 'react';
import { Link } from 'react-router-dom';
import { Menu, Button } from 'antd';
import { Avatar } from 'antd';
import {
  HistoryOutlined,
  HomeOutlined,
  ShoppingCartOutlined,
  LogoutOutlined,
  FileTextOutlined,
  UserAddOutlined,
  CodeSandboxOutlined,

} from '@ant-design/icons';

const { SubMenu } = Menu;

class App extends React.Component {
  state = {
    collapsed: false,
  };

  toggleCollapsed = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  };

  render() {
    return (
      // <div style={{ width: 270 }}>
      <Menu
        defaultSelectedKeys={['1']}
        defaultOpenKeys={['sub1']}
        mode="inline"
        theme="light"
        inlineCollapsed={this.state.collapsed}
      >
        <div className="navbar">
          <div className="logonavbar">

            {/* <span >CS</span>
              <CodeSandboxOutlined />
              <span className="navbarnametext">ABCD</span> */}

            <div className="avata">
              <Avatar style={{ color: '#32A05F', backgroundColor: '#32A05F15', fontSize: '20px' }} size={"large"}>ธ</Avatar>
              <div className="avata_name"> ธนาภรณ์ ด่านชาญชัย </div>
              <div className="avata_name2"> เจ้าหน้าที่พัสดุ </div>
            </div>
            <div className="line2"></div>
          </div>
          
          {/* <p className="menutext">เมนู</p> */}
        </div>

        <Menu.Item key="1" icon={<HomeOutlined />}>
          <Link to="/Dashboard">หน้าหลัก</Link>
        </Menu.Item>

        <SubMenu key="sub1" icon={<ShoppingCartOutlined />} title="พัสดุ">
          <Menu.Item key="5"><Link to="/itemlist">รายการพัสดุ</Link></Menu.Item>
          <Menu.Item key="6"><Link to="/itemout">พัสดุหมด</Link></Menu.Item>
          <Menu.Item key="7"><Link to="/itemAlmostout">พัสดุใกล้หมด</Link></Menu.Item>
          <Menu.Item key="8"><Link to="/itemMotionless">พัสดุไม่เคลื่อนไหว</Link></Menu.Item>
          <Menu.Item key="9"><Link to="/itemDeteriorate">พัสดุเสื่อมสภาพ</Link></Menu.Item>
        </SubMenu>

        <SubMenu key="sub2" icon={<HistoryOutlined />} title="ประวัติ">
          <Menu.Item key="10"><Link to="/RequestPage">ประวัติการเบิกพัสดุ</Link></Menu.Item>
          <Menu.Item key="11"><Link to="RecivePage">ประวัติการรับเข้าพัสดุ</Link></Menu.Item>
          <Menu.Item key="12"><Link to="/RequisitionPage">ประวัติการขอเพิ่มพัสดุ</Link></Menu.Item>
        </SubMenu>

        {/* <Menu.Item key="2" icon={<FileTextOutlined />}>
          <Link to="/ReportPDFPage">รายงาน</Link>
        </Menu.Item> */}

        <Menu.Item key="3" icon={<UserAddOutlined />}>
          <Link to="/AccountManagePage">จัดการบัญชีผู้ใช้</Link>
        </Menu.Item>

        <Menu.Item key="4" icon={<LogoutOutlined />}>
          <Link to="/login">ออกจากระบบ</Link>
        </Menu.Item>

      </Menu>
      // </div>
    );
  }
}

function navbar() {
  return (
    <div className="navbar">
      <App />
    </div>
  )
}
export default navbar;


