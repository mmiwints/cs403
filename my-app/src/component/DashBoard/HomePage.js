import React, { useState } from 'react';
import { Button, Modal, Image, Table } from 'antd';

import tapewhite from '../Img/tapewhite.jpg';
import clips from '../Img/clips.jpg';
import lq from '../Img/liq.png';

import picsave from '../Img/picsave.png';
import box from '../Img/box.png';
import { PlusOutlined } from '@ant-design/icons';
import { Link } from 'react-router-dom';
import { Avatar } from 'antd';


const App = () => {
    const [visible, setVisible] = useState(false);
    return (
        <>
            <Button type="primary" shape="round" icon={<PlusOutlined />} size={"large"} onClick={() => setVisible(true)}>
                บันทึกรายการใหม่
            </Button>
            <Modal
                title=""
                centered
                visible={visible}
                onOk={() => setVisible(false)}
                onCancel={() => setVisible(false)}
                width={1000}
                footer={null}
                header={null}
            >
                <div className="modalhomepage-grid">

                    <Link to="/RequestFormPage"><div className="savedatawithdrawal" >
                        <svg xmlns="http://www.w3.org/2000/svg" width="100" height="auto" viewBox="0 0 180.848 168.842">
                            <g id="Group_16026" data-name="Group 16026" transform="translate(0 0)">
                                <path id="Path_121" data-name="Path 121" d="M308.326,99.5l-5.534,3.019-68.179,37.2-5.534,3.019a11.558,11.558,0,0,0-4.6,15.664l50.772,93.053a11.558,11.558,0,0,0,15.664,4.6l.014-.008,79.219-43.223.014-.008a11.558,11.558,0,0,0,4.6-15.664L323.99,104.1A11.558,11.558,0,0,0,308.326,99.5Z" transform="translate(-223.065 -98.089)" fill="#f2f2f2" />
                                <path id="Path_122" data-name="Path 122" d="M320.446,112.942l-6.555,3.576-66.138,36.086-6.554,3.576a8.231,8.231,0,0,0-3.279,11.155l50.772,93.053a8.231,8.231,0,0,0,11.155,3.279l.014-.008,79.219-43.223.014-.008a8.231,8.231,0,0,0,3.279-11.155L331.6,116.222A8.231,8.231,0,0,0,320.446,112.942Z" transform="translate(-233.594 -108.617)" fill="#fff" />
                                <path id="Path_123" data-name="Path 123" d="M487.04,225.081,457.495,241.2a1.923,1.923,0,1,1-1.842-3.375L485.2,221.706a1.923,1.923,0,1,1,1.842,3.375Z" transform="translate(-399.095 -191.872)" fill="rgba(50,160,95,0.56)" />
                                <path id="Path_124" data-name="Path 124" d="M506.957,232.991,470.45,252.91a1.923,1.923,0,0,1-1.842-3.375l36.507-19.919a1.923,1.923,0,0,1,1.842,3.375Z" transform="translate(-408.942 -197.884)" fill="rgba(50,160,95,0.56)" />
                                <path id="Path_125" data-name="Path 125" d="M328.043,275.392,300.8,290.254a.921.921,0,0,0-.367,1.25l11.6,21.259a.921.921,0,0,0,1.25.367l27.239-14.862a.921.921,0,0,0,.367-1.25l-11.6-21.259a.921.921,0,0,0-1.25-.367Z" transform="translate(-281.789 -232.772)" fill="#f2f2f2" />
                                <path id="Path_126" data-name="Path 126" d="M514.787,337.522l-40.075,21.866a1.922,1.922,0,1,1-1.842-3.375l40.075-21.866a1.923,1.923,0,1,1,1.842,3.375Z" transform="translate(-412.183 -277.34)" fill="#f2f2f2" />
                                <path id="Path_127" data-name="Path 127" d="M534.7,345.432,487.668,371.1a1.923,1.923,0,1,1-1.842-3.375l47.036-25.664a1.923,1.923,0,1,1,1.842,3.375Z" transform="translate(-422.03 -283.352)" fill="#f2f2f2" />
                                <circle id="Ellipse_122" data-name="Ellipse 122" cx="8.156" cy="8.156" r="8.156" transform="translate(39.111 83.643)" fill="#f2f2f2" />
                                <path id="Path_128" data-name="Path 128" d="M606.19,263.788H515.914a11.558,11.558,0,0,0-11.545,11.545v106a11.558,11.558,0,0,0,11.545,11.545H606.19a11.558,11.558,0,0,0,11.545-11.545v-106A11.558,11.558,0,0,0,606.19,263.788Z" transform="translate(-436.886 -224.038)" fill="#e6e6e6" />
                                <path id="Path_129" data-name="Path 129" d="M616.72,277.642H526.444a8.231,8.231,0,0,0-8.221,8.221v106a8.231,8.231,0,0,0,8.221,8.221H616.72a8.231,8.231,0,0,0,8.221-8.221v-106A8.231,8.231,0,0,0,616.72,277.642Z" transform="translate(-447.417 -234.569)" fill="#fff" />
                                <path id="Path_130" data-name="Path 130" d="M762.407,373.334H728.75a1.923,1.923,0,1,1,0-3.845h33.657a1.923,1.923,0,0,1,0,3.845Z" transform="translate(-605.978 -304.382)" fill="#32a05f" />
                                <path id="Path_131" data-name="Path 131" d="M770.338,400.382H728.75a1.923,1.923,0,1,1,0-3.845h41.588a1.923,1.923,0,1,1,0,3.845Z" transform="translate(-605.978 -324.941)" fill="#32a05f" />
                                <path id="Path_132" data-name="Path 132" d="M602.554,336.711h-31.03a.921.921,0,0,0-.921.921v24.217a.921.921,0,0,0,.921.921h31.03a.921.921,0,0,0,.921-.921V337.632a.921.921,0,0,0-.921-.921Z" transform="translate(-487.231 -279.467)" fill="#e6e6e6" />
                                <path id="Path_140" data-name="Path 140" d="M724.191,501.308H678.539a1.922,1.922,0,1,1,0-3.845h45.652a1.922,1.922,0,1,1,0,3.845Z" transform="translate(-567.813 -401.656)" fill="#e6e6e6" />
                                <path id="Path_141" data-name="Path 141" d="M732.121,528.356H678.539a1.922,1.922,0,1,1,0-3.845h53.582a1.922,1.922,0,1,1,0,3.845Z" transform="translate(-567.813 -422.215)" fill="#e6e6e6" />
                                <path id="Path_142" data-name="Path 142" d="M724.191,598.522H678.539a1.923,1.923,0,1,1,0-3.845h45.652a1.923,1.923,0,1,1,0,3.845Z" transform="translate(-567.813 -475.549)" fill="#e6e6e6" />
                                <path id="Path_143" data-name="Path 143" d="M732.121,625.57H678.539a1.923,1.923,0,1,1,0-3.845h53.582a1.923,1.923,0,0,1,0,3.845Z" transform="translate(-567.813 -496.108)" fill="#e6e6e6" />
                                <circle id="Ellipse_124" data-name="Ellipse 124" cx="8.156" cy="8.156" r="8.156" transform="translate(84.502 92.745)" fill="#e6e6e6" />
                                <circle id="Ellipse_125" data-name="Ellipse 125" cx="8.156" cy="8.156" r="8.156" transform="translate(84.502 116.255)" fill="#e6e6e6" />
                                <path id="Path_144" data-name="Path 144" d="M724.191,696.522H678.539a1.923,1.923,0,1,1,0-3.845h45.652a1.923,1.923,0,1,1,0,3.845Z" transform="translate(-567.813 -550.04)" fill="#e6e6e6" />
                                <path id="Path_145" data-name="Path 145" d="M732.121,723.57H678.539a1.923,1.923,0,1,1,0-3.845h53.582a1.923,1.923,0,0,1,0,3.845Z" transform="translate(-567.813 -570.599)" fill="#e6e6e6" />
                                <circle id="Ellipse_126" data-name="Ellipse 126" cx="8.156" cy="8.156" r="8.156" transform="translate(84.502 139.764)" fill="#e6e6e6" />
                            </g>
                        </svg>

                        <br></br>
                        <div className="textmodal">
                            <h2>บันทึกข้อมูลการเบิกพัสดุ</h2>
                            <div className="textmodal2">
                                บันทึกข้อมูลรายละเอียดการเบิกพัสดุ<br></br>ตามฟอร์มที่บุคลากรยื่นเบิก
                            </div>
                        </div>
                    </div>
                    </Link>

                    <div />


                    <Link to="/ReciveForm"><div className="savedatareception">
                        <svg xmlns="http://www.w3.org/2000/svg" width="140" height="auto" viewBox="0 0 230.21 98.053">
                            <g id="Group_16027" data-name="Group 16027" transform="translate(0 0.001)">
                                <rect id="Rectangle_16941" data-name="Rectangle 16941" width="85.312" height="70.416" transform="translate(0 27.093)" fill="#e6e6e6" />
                                <rect id="Rectangle_16942" data-name="Rectangle 16942" width="14.587" height="19.735" transform="translate(35.564 77.888)" fill="#2e82f0" />
                                <path id="Path_157" data-name="Path 157" d="M426.953,715.159l-1.49-1.246.11.778S426.1,714.856,426.953,715.159Z" transform="translate(-331.58 -628.661)" fill="none" />
                                <path id="Path_158" data-name="Path 158" d="M488.026,402.275l2.3-6.12,21.422-12.241,14.536-3.06Z" transform="translate(-296.069 -371.673)" fill="#2e82f0" />
                                <path id="Path_159" data-name="Path 159" d="M253.443,402.275l-2.3-6.12-21.422-12.241-14.536-3.06Z" transform="translate(-140.287 -371.673)" fill="#2e82f0" />
                                <path id="Path_160" data-name="Path 160" d="M373.484,391.587H291.622l-6.885-32.133h95.633Z" transform="translate(-179.997 -359.455)" fill="#076df2" />
                                <rect id="Rectangle_16943" data-name="Rectangle 16943" width="82.627" height="67.833" transform="translate(111.242 30.219)" fill="#2e82f0" />
                                <rect id="Rectangle_16944" data-name="Rectangle 16944" width="14.587" height="20.175" transform="translate(145.262 77.878)" fill="#e6e6e6" />
                                <rect id="Rectangle_16945" data-name="Rectangle 16945" width="34.322" height="10.726" transform="translate(135.395 44.102)" fill="#fff" />
                                <rect id="Rectangle_16946" data-name="Rectangle 16946" width="34.322" height="10.726" transform="translate(25.469 44.102)" fill="#fff" />
                            </g>
                        </svg>





                        <div className="textmodal">
                            <h2>บันทึกข้อมูลการรับเข้าพัสดุ</h2>
                            <div className="textmodal2">
                                บันทึกข้อมูลรายละเอียดและวันที่ได้รับพัสดุ<br></br>หากได้รับพัสดุที่ขอเพิ่มแล้ว
                            </div>
                        </div>
                    </div>
                    </Link>
                </div>
            </Modal>
        </>
    );
};



const columns = [
    {
        title: 'รายการ',
        dataIndex: 'name',
        key: 'name',

    },

    {
        title: 'คงเหลือ',
        dataIndex: 'remain',
        key: 'remain',
    },

];

const columns2 = [
    {
        title: 'รายการ',
        dataIndex: 'name',
        key: 'name',
        className: 'table_head'

    },

    {
        title: 'คงเหลือ',
        dataIndex: 'remain',
        key: 'remain',
        className: 'table_head'
    },

];

const dataSource = [


    {
        key: '1',
        name: <div className="table_text">
            <img style={{ width: 35 }} src={tapewhite} />
            <div className="pad1">
                กระดาษกาว 1 นิ้ว (สีขาว)
        </div>
        </div>,

        remain: 0,

    },

    {
        key: '2',
        name: <div className="table_text">
            <img style={{ width: 35 }} src={clips} />
            <div className="pad1">

                ลวดเสียบกระดาษ
        </div>
        </div>,

        remain: 0,

    },

    {
        key: '3',
        name: <div className="table_text">
            <img style={{ width: 35 }} src={lq} />
            <div className="pad1">
                น้ำยาลบคำผิด (แบบแท่ง)
        </div>
        </div>,

        remain: 0,

    },


];

const dataSource2 = [

    {
        key: '1',
        name: 'ยางลบดินสอ',
        remain: 3,
    },

    {
        key: '2',
        name: 'หลอดไฟ LED (ขาว)',
        remain: 1,
    },

    {
        key: '3',
        name: 'แปลงลบกระดาน',
        /* <a className="tag_a">ST0023</a><br></br> */
        remain: 2,
    },

];



const Apphomepage = () => {
    const [visible, setVisible] = useState(false);


    return (

        <div className="Appmiw">
            {/* <div className="itemList-grid2">
                <div></div>
                <div className="avata">
                    <Avatar style={{ color: '#32A05F', backgroundColor: '#32A05F15', fontSize: '20px' }} size={"large"}>ธ</Avatar>
                    <div className="avata_name"> ธนาภรณ์ ด่านชาญชัย </div>
                </div>
                
            </div>
            <br></br>
            <br></br> */}


            <div className="itemList-grid">
                <div className="Head1" >Dashboard</div>
                <div className="Modal"><App /></div>
                <br></br>
            </div>

            <div className="dashbord-grid">
                <div className="DBitemout">
                    <div className="grid-DBitemout">
                        <h2 className="">พัสดุหมด</h2>
                        <Link to="/itemout" className="DBtext2">ดูทั้งหมด</Link>
                    </div>


                    <div className="itemtableout">
                        <Table dataSource={dataSource} columns={columns} pagination={false} />

                    </div>




                </div>  <div />

                <div className="DBitemalmost">
                    <div className="grid-DBitemalmout">
                        <h2 className="table_head">พัสดุใกล้หมด</h2>
                        <Link to="/itemAlmostout" className="DBtext2">ดูทั้งหมด</Link>
                    </div>

                    <div className="itemtableout">
                        <Table dataSource={dataSource2} columns={columns2} pagination={false} />

                    </div>


                </div>


            </div>
        </div>
    )

}

export default Apphomepage;