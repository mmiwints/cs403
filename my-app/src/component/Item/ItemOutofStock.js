import React, { useState } from 'react';
import { Table } from 'antd';
import ReactExport from "react-export-excel";
import { Button } from 'antd';
import tapewhite from '../Img/tapewhite.jpg';
import tape from '../Img/tape1n.jpg';
import clips from '../Img/clips.jpg';
import lq from '../Img/liq.png';


const columnsTable = [
    {
        title: 'รายการ',
        dataIndex: 'name',
        key: 'name',
        // render: text => <a>{text}</a>,
    },
    {
        title: 'ประเภท',
        dataIndex: 'category',
        key: 'category',
    },
    {
        title: 'คงเหลือ',
        dataIndex: 'remain',
        key: 'remain',
    },
    {
        title: 'หน่วยวัด',
        dataIndex: 'unit',
        key: 'unit',
    }
];

const dataSource = [
    {
        key: '1',
        name: 'เทปผ้าติดสันหนังสือ 1 นิ้ว (สีน้ำเงิน)',
        src: tape,
        category: 'วัสดุสำนักงาน',
        remain: 0,
        unit: 'ม้วน'
    },

    {
        key: '2',
        name: 'กระดาษกาว 1 นิ้ว (สีขาว)',
        category: 'วัสดุสำนักงาน',
        src: tapewhite,
        remain: 0,
        unit: 'ม้วน'
    },

    {
        key: '3',
        name: 'ลวดเสียบกระดาษ',
        src: clips,
        category: 'วัสดุสำนักงาน',
        remain: 0,
        unit: 'กล่อง'
    },

    {
        key: '4',
        name: 'น้ำยาลบคำผิด (แบบแท่ง)',
        src: lq,
        category: 'วัสดุสำนักงาน',
        remain: 0,
        unit: 'อัน'
    },


];


function itemOutofStock() {

    const dataSource2 = dataSource.map((value) => {
        return {
            key: value.key,
            name: <div className="table_text">
                <img style={{ width: 40 }} src={value.src} />
                <div className="pad1">
                    {value.name}
                </div>
            </div>,
            category: value.category,
            remain: value.remain,
            unit: value.unit
        }
    })

    return (

        <div className="Appmiw">
            <div className="itemList-grid">
                <p className="itemout-headtext3">พัสดุหมด</p>
                <div />
            </div>

            <div className="itemtableout">
                <Table dataSource={dataSource2} columns={columnsTable} />

            </div>
        </div>
    )
}

export default itemOutofStock;