import React, { useState } from 'react';
import { Table } from 'antd';
import { Link, useHistory } from 'react-router-dom';
import tape from '../Img/tape1n.jpg';
import tapered1n from '../Img/tapered1n.jpg';
import tapered2n from '../Img/tapered2n.jpg';
import tapeblack1n from '../Img/tapeblack1n.jpg';
import tapewhite from '../Img/tapewhite.jpg';



const columns = [
    {
        title: 'รายการ',
        dataIndex: 'Supplies',
        key: 'Supplies',

    },
    {
        title: 'ประเภท',
        dataIndex: 'category',
        key: 'category',
    },
    {
        title: 'คงเหลือ',
        dataIndex: 'remain',
        key: 'remain',
    },
    {
        title: 'หน่วยวัด',
        dataIndex: 'unit',
        key: 'unit',
    }
];

// const dataSource = [
//     {
//         key: '1',
//         Supplies: <Link to="/item">
//             <div className="table_text">
//                 <img style={{ width: 50 }} src={tapewhite} />
//                 <div className="pad1">
//                     <a className="tag_a">ST0001</a><br></br>
//                     <div className="textcolorblack">
//                         กระดาษกาว 1 นิ้ว (สีขาว)</div>
//                 </div>
//             </div>
//         </Link>,
//         category: 'วัสดุสำนักงาน',
//         remain: 0,
//         unit: 'ม้วน'
//     },
//     {
//         key: '2',
//         Supplies: 
//         <div className="table_text">
//             <img style={{ width: 50 }} src={tapered2n} />
//             <div className="pad1">
//                 <a className="tag_a">ST0002</a><br></br>
//                 เทปผ้าติดสันหนังสือ 2 นิ้ว (สีแดง)
//             </div>
//         </div>,
//         category: 'วัสดุสำนักงาน',
//         remain: 10,
//         unit: 'ม้วน'
//     },

//     {
//         key: '3',
//         Supplies: <div className="table_text">
//             <img style={{ width: 50 }} src={tapered1n} />
//             <div className="pad1">
//                 <a className="tag_a">ST0003</a><br></br>
//         เทปผ้าติดสันหนังสือ 1 นิ้ว (สีแดง)
//         </div>
//         </div>,
//         category: 'วัสดุสำนักงาน',
//         remain: 7,
//         unit: 'ม้วน'
//     },

//     {
//         key: '4',
//         Supplies: <div className="table_text">
//             <img style={{ width: 50 }} src={tape} />
//             <div className="pad1">
//                 <a className="tag_a">ST0004</a><br></br>
//         เทปผ้าติดสันหนังสือ 1 นิ้ว (สีน้ำเงิน)
//         </div>
//         </div>,
//         category: 'วัสดุสำนักงาน',
//         remain: 18,
//         unit: 'ม้วน'
//     },

//     {
//         key: '5',
//         Supplies: <div className="table_text">
//             <img style={{ width: 50 }} src={tapeblack1n} />
//             <div className="pad1">
//                 <a className="tag_a">ST0005</a><br></br>
//         เทปผ้าติดสันหนังสือ 2 นิ้ว (สีดำ)
//         </div>
//         </div>,
//         category: 'วัสดุสำนักงาน',
//         remain: 13,
//         unit: 'ม้วน'
//     },


// ];


function Itemtable(props) {
    

    const dataSource2 = props.mockUp.map((value) => {
        return {
            key: value.key,
            Supplies: <div className="table_text">
                <img style={{ width: 40 }} src={value.src} />
                <div className="pad1">
                    {/* <a className="tag_a">{value.tag}</a><br></br> */}
                    {value.Supplies}
                </div>
            </div>,
            category: value.category,
            remain: value.remain,
            unit: value.unit
        }
    })
    console.log(dataSource2)
    const history = useHistory()
    return (
        <div className="itemtable">
            <Table
                onRow={(record, rowIndex) => {
                    console.log(rowIndex)
                    return {
                        onClick: event => {

                            history.push(`/item/${dataSource2[rowIndex].key}`)

                        }
                    };
                }}

                dataSource={dataSource2}
                columns={columns}
            />;

        </div>
    )
}

export default Itemtable;