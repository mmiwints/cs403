import React, { useState } from 'react';
import { Button } from 'antd';
import { Input, AutoComplete } from 'antd';
import { Table } from 'antd';
import { Link } from 'react-router-dom';
import List1 from "./itemtable";
import { PlusOutlined } from '@ant-design/icons';
import tape from '../Img/tape1n.jpg';
import tapered1n from '../Img/tapered1n.jpg';
import tapered2n from '../Img/tapered2n.jpg';
import tapeblack1n from '../Img/tapeblack1n.jpg';
import tapewhite from '../Img/tapewhite.jpg';



class ButtonSize extends React.Component {
    state = {
        size: 'large',
    };

    handleSizeChange = e => {
        this.setState({ size: e.target.value });
    };

    render() {
        const { size } = this.state;
        return (
            <>
                <Link to="/AddnewItemPage">
                    <Button style={{ width: 160 }} type="primary" shape="round" icon={<PlusOutlined />} size={size}>
                        เพิ่มพัสดุใหม่
                    </Button>
                </Link>
            </>
        );
    }
}


function ItemList() {
    const data = [
        {
            key: '1',
            Supplies: 'กระดาษกาว 1 นิ้ว (สีขาว)',
            src: tapewhite,
            tag: 'ST0001',
            lot: 'A0001',
            amount: '2',
            lot2: 'A0002',
            amount2: '24',
            category: 'วัสดุสำนักงาน',
            unit: 'ม้วน',
            last_recive: '12 มีนาคม 2564',
            broken: '0',
            remain: 26,

        },
        {
            key: '2',
            Supplies: 'เทปผ้าติดสันหนังสือ 2 นิ้ว (สีแดง)',
            src: tapered2n,
            tag: 'ST0002',
            lot: 'A0001',
            amount: '1',
            lot2: 'A0002',
            amount2: '9',
            category: 'วัสดุสำนักงาน',
            unit: 'ม้วน',
            last_recive: '7 มีนาคม 2564',
            broken: '0',
            remain: 10,

        },
        {
            key: '3',
            Supplies: 'เทปผ้าติดสันหนังสือ 1 นิ้ว (สีแดง)',
            src: tapered1n,
            tag: 'ST0003',
            lot: 'A0001',
            amount: '1',
            lot2: 'A0002',
            amount2: '6',
            category: 'วัสดุสำนักงาน',
            unit: 'ม้วน',
            last_recive: '7 มีนาคม 2564',
            broken: '0',
            remain: 7,
        },
        {
            key: '4',
            Supplies: 'เทปผ้าติดสันหนังสือ 1 นิ้ว (สีน้ำเงิน)',
            src: tape,
            tag: 'ST0004',
            lot: 'A0001',
            amount: '2',
            lot2: 'A0002',
            amount2: '16',
            category: 'วัสดุสำนักงาน',
            unit: 'ม้วน',
            last_recive: '7 มีนาคม 2564',
            broken: '0',
            remain: 18,
        },
        {
            key: '5',
            Supplies: 'เทปผ้าติดสันหนังสือ 2 นิ้ว (สีดำ)',
            src: tapeblack1n,
            tag: 'ST0005',
            lot: 'A0001',
            amount: '3',
            lot2: 'A0002',
            amount2: '10',
            category: 'วัสดุสำนักงาน',
            unit: 'ม้วน',
            last_recive: '7 มีนาคม 2564',
            broken: '0',
            remain: 13,
        },

    ]
    const [search, setSearch] = useState(data)


    const onSearch = (value) => {
        setSearch(data.filter((item) => {
            return item.Supplies.includes(value)
        }))
        console.log(search)
    };

    const optionsMock = data.map((value) => {
        return { value: value.Supplies }
    })
    console.log(optionsMock)
    const { Search } = Input;
    

    return (
        <div className="Appmiw">
            <div className="Head1" >รายการพัสดุ</div>
            <div className="itemList-grid">
                <div className="Fillter">
                    <AutoComplete
                        options={optionsMock}
                        filterOption={(inputValue, option) =>
                            option.value.toUpperCase().indexOf(inputValue.toUpperCase()) !== -1
                        }
                    ><Search placeholder="ค้นหาชื่อพัสดุ" onSearch={onSearch} size="large" style={{ width: 350 }} /></AutoComplete>
                </div>
                <div className="Fillter2">
                    <ButtonSize />
                </div>

            </div>

            <List1 mockUp={search}></List1>


        </div>

    )
}

export default ItemList;