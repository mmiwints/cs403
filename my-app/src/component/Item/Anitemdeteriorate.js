import React, { useState } from 'react';
import { Image, Button } from 'antd';
import tape from '../Img/tape1n.jpg';
import { Link } from 'react-router-dom';
import { Modal, Space } from 'antd';
import { ExclamationCircleOutlined } from '@ant-design/icons';

const { confirm } = Modal;
function showDeleteConfirm() {
    confirm({
        title: 'คุณแน่ใจหรือไม่ว่าต้องการลบรายการ',
        icon: <ExclamationCircleOutlined />,
        // content: 'รายการนี้จะถูกลบถาวร',
        okText: 'ลบ',
        okType: 'danger',
        cancelText: 'ยกเลิก',
        onOk() {
            console.log('OK');
        },
        onCancel() {
            console.log('Cancel');
        },
    });
}

function ImageDemo() {
    return (
        <Image
            width={100}
            src={tape}
        />
    );
}

function AnitemDeter() {
    return (

        <div className="Appmiw">
            
            <p className="Head1" >พัสดุเสื่อมสภาพ</p>

            <div className="grid_popup">
                <div className="grid_container6">
                    <div className="Reg_text2">รายการ :</div>
                    <div className="Reg_text3">ชำรุด</div>

                </div>
            </div>

            <div className="grid_popup">
                <div className="grid_container6">
                    <div className="Reg_text2">รายการพัสดุ :</div>
                    <div className="Reg_text3">เทปผ้าติดสันหนังสือ 1 นิ้ว (สีน้ำเงิน)</div>

                </div>
            </div>
            <div className="grid_popup">
                <div className="grid_container6">
                    <div className="Reg_text2">จำนวน :</div>
                    <div className="Reg_text3">2 ชิ้น</div>
                </div>
            </div>

            <div className="grid_popup">
                <div className="grid_container6">
                    <div className="Reg_text2">หมายเหตุ :</div>
                    <div className="Reg_text3">เทปกาวเปียกน้ำ</div>
                </div>
            </div>

            <div className="grid_popup">
                <div className="grid_container6">
                    <div className="Reg_text2">แนบรูป :</div>
                    <ImageDemo />
                </div>
            </div>

            <div className="Date_text">บันทึกเมื่อวันที่ 23 สิงหาคม 2564</div>
            <Link to="/ReportEditPage"><Button type="primary">แก้ไขข้อมูล</Button></Link>
            <div className="pad2"> </div>
            <Space wrap>
                <Button onClick={showDeleteConfirm}>
                ลบรายการ
                </Button>
                
            </Space>

        </div>
    )
}

export default AnitemDeter;