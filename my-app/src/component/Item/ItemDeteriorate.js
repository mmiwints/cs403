import React, { useState } from 'react';
import { Button, Table } from 'antd';
import { Select } from 'antd';
import { Link,useHistory} from 'react-router-dom';
import ReactExport from "react-export-excel";
import { DownloadOutlined } from '@ant-design/icons';
import tapewhite from '../Img/tapewhite.jpg';
import tape from '../Img/tape1n.jpg';
import clips from '../Img/clips.jpg';
import lq from '../Img/liq.png';




class ButtonSize extends React.Component {
    state = {
        size: 'large',
    };

    handleSizeChange = e => {
        this.setState({ size: e.target.value });
    };

    render() {
        const { size } = this.state;
        return (
            <>
                <Link to="/ReportFormPage">
                    <Button style={{ width: 180 }} type="primary" shape="round" size={size}>
                        แจ้งพัสดุเสื่อมสภาพ
                    </Button>
                </Link>
            </>
        );
    }
}

const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

const columnsTable = [
    {
        title: 'รายการ',
        dataIndex: 'name',
        key: 'name',
    },
    {
        title: 'ประเภท',
        dataIndex: 'category',
        key: 'category',
    },
    {
        title: 'จำนวน',
        dataIndex: 'amount',
        key: 'amount',
    },
    {
        title: 'หน่วยวัด',
        dataIndex: 'unit',
        key: 'unit',
    },
    {
        title: 'หมายเหตุ',
        dataIndex: 'notice',
        key: 'notice',
    }
];

const dataSource = [
    {
        key: '1',
        name: 'เทปผ้าติดสันหนังสือ 1 นิ้ว (สีน้ำเงิน)',
        src: tape,
        year: '2564',
        category: 'วัสดุสำนักงาน',
        amount: 10,
        unit: 'ม้วน',
        notice: 'เสื่อมสภาพ'
    },

    {
        key: '2',
        name: 'กระดาษกาว 1 นิ้ว (สีขาว)',
        src: tapewhite,
        year: '2564',
        category: 'วัสดุสำนักงาน',
        amount: 12,
        unit: 'ม้วน',
        notice: 'เสื่อมสภาพ'
    },

    {
        key: '3',
        name: 'ลวดเสียบกระดาษ',
        src: clips,
        year: '2563',
        category: 'วัสดุสำนักงาน',
        amount: 20,
        unit: 'กล่อง',
        notice: 'สูญหาย'
    },

    {
        key: '4',
        name: 'น้ำยาลบคำผิด (แบบแท่ง)',
        src: lq,
        year: '2562',
        category: 'วัสดุสำนักงาน',
        amount: 25,
        unit: 'อัน',
        notice: 'เสื่อมสภาพ'
    },


];


function ItemDeteriorate() {

    const [selecteditem, setSelecteditem] = useState(dataSource)
    function handleChange(value) {
        console.log(`selected ${value}`);
        const filter = dataSource.filter(selecteditem => selecteditem.year === value);
        setSelecteditem(filter)
        // console.log(filter);
    }


    const dataSource2 = selecteditem.map((value) => {
        return {
            key: value.key,
            name: <div className="table_text">
                <img style={{ width: 40 }} src={value.src} />
                <div className="pad1">
                    {value.name}
                </div>
            </div>,
            category: value.category,
            amount: value.amount,
            unit: value.unit,
            notice: value.notice
        }
    })

    const filterColumns = (selecteditem) => {
        // Get column names
        const columns = Object.keys(selecteditem[0]);

        // Remove by key (firstname)
        const filterColsByKey = columns.filter(c => c !== 'src' && c !== 'year');

        // OR use the below line instead of the above if you want to filter by index
        // columns.shift()

        return filterColsByKey // OR return columns
    };

    const camelCase = (str) => {
        return str.substring(0, 1).toUpperCase() + str.substring(1);
    };
    const history = useHistory()




    return (
        <div className="Appmiw">


            <div className="Head1" >พัสดุเสื่อมสภาพ</div>

            <div className="itemList-grid3">
                <div className="Fillter">
                    <div className="Reg_text2"> ปี :</div>
                    <div className="pad2"></div>
                    <Select defaultValue="2564" size="large" style={{ width: 180 }} onChange={handleChange}>

                        <option value="2564">2564</option>
                        <option value="2563">2563</option>
                        <option value="2562">2562</option>
                        
                    </Select>

                </div>


                <div className="Fillter2">
                    <ExcelFile filename="test"
                        element={(<Button bsStyle="info" size="large" icon={<DownloadOutlined />}>  นำออก (Excel) </Button>)}>
                        <ExcelSheet data={selecteditem} name="Test">
                            {
                                filterColumns(selecteditem).map((col) => {
                                    return <ExcelColumn label={camelCase(col)} value={col} />
                                })
                            }
                        </ExcelSheet>
                    </ExcelFile>
                </div>

                <div className="Fillter2">
                    <ButtonSize />
                </div>

            </div>

            <div className="itemtableout">
                <Table onRow={(record, rowIndex) => {
                    console.log(rowIndex)
                    return {
                        onClick: event => {

                            history.push(`/anitemDeteriorate/${dataSource[rowIndex].key}`)

                        }
                    };
                }} dataSource={dataSource2} columns={columnsTable} />
            </div>


        </div>
    )
}

export default ItemDeteriorate;