import React, { useState } from 'react';
import { Image, Button } from 'antd';
import tapewhite from '../Img/tapewhite.jpg';
import { Link, useParams } from 'react-router-dom';
import tape from '../Img/tape1n.jpg';
import tapered1n from '../Img/tapered1n.jpg';
import tapered2n from '../Img/tapered2n.jpg';
import tapeblack1n from '../Img/tapeblack1n.jpg';

function Item() {

    const params = useParams();
    console.log(params.key)

    const data = [
        {
            key: '1',
            Supplies: 'กระดาษกาว 1 นิ้ว (สีขาว)',
            src: tapewhite,
            // tag: 'ST0001',
            lot: 'A0001',
            amount: '2',
            lot2: 'A0002',
            amount2: '24',
            category: 'วัสดุสำนักงาน',
            unit: 'ม้วน',
            last_recive: '12 มีนาคม 2564',
            broken: '0',
            remain: 26,

        },
        {
            key: '2',
            Supplies: 'เทปผ้าติดสันหนังสือ 2 นิ้ว (สีแดง)',
            src: tapered2n,
            // tag: 'ST0002',
            lot: 'A0001',
            amount: '1',
            lot2: 'A0002',
            amount2: '9',
            category: 'วัสดุสำนักงาน',
            unit: 'ม้วน',
            last_recive: '7 มีนาคม 2564',
            broken: '0',
            remain: 10,

        },
        {
            key: '3',
            Supplies: 'เทปผ้าติดสันหนังสือ 1 นิ้ว (สีแดง)',
            src: tapered1n,
            // tag: 'ST0003',
            lot: 'A0001',
            amount: '1',
            lot2: 'A0002',
            amount2: '6',
            category: 'วัสดุสำนักงาน',
            unit: 'ม้วน',
            last_recive: '7 มีนาคม 2564',
            broken: '0',
            remain: 7,
        },
        {
            key: '4',
            Supplies: 'เทปผ้าติดสันหนังสือ 1 นิ้ว (สีน้ำเงิน)',
            src: tape,
            // tag: 'ST0004',
            lot: 'A0001',
            amount: '2',
            lot2: 'A0002',
            amount2: '16',
            category: 'วัสดุสำนักงาน',
            unit: 'ม้วน',
            last_recive: '7 มีนาคม 2564',
            broken: '0',
            remain: 18,
        },
        {
            key: '5',
            Supplies: 'เทปผ้าติดสันหนังสือ 2 นิ้ว (สีดำ)',
            src: tapeblack1n,
            // tag: 'ST0005',
            lot: 'A0001',
            amount: '3',
            lot2: 'A0002',
            amount2: '10',
            category: 'วัสดุสำนักงาน',
            unit: 'ม้วน',
            last_recive: '7 มีนาคม 2564',
            broken: '0',
            remain: 13,
        },

    ]
    
    const selectedData = data.filter(value => value.key==params.key)
    console.log(selectedData)


    
        return (

            <div className="Appmiw">
                <p className="itemlist-headtext" >รายการพัสดุ</p>
                <div className="itemgrid">
                    <Image
                        width={200}
                        height={200}
                        
                        src={selectedData[0].src}
                    />
    
                    <div className="item-detail">
                        <div className="item-id">{selectedData[0].tag}</div>
    
                        <div className="item-name">
                            <h2>{selectedData[0].Supplies}</h2>
                        </div>
                        <br></br>
    
    
    
                        <div className="grid_popup">
                            <div className="grid_item">
                                <div>หมวดหมู่ :</div>
                                <div>{selectedData[0].category}</div>
                            </div>
                        </div>
    
                        <div className="grid_popup">
                            <div className="grid_item">
                                <div>หน่วยวัด :</div>
                                <div>{selectedData[0].unit}</div>
                            </div>
                        </div>
    
                        <div className="grid_popup">
                            <div className="grid_item">
                                <div>วันที่รับเข้าล่าสุด :</div>
                                <div>{selectedData[0].last_recive}</div>
                            </div>
                        </div>
    
                        <div className="grid_popup">
                            <div className="grid_item">
                                <div>เสื่อมสภาพ :</div>
                                <div>{selectedData[0].broken}</div>
                            </div>
                        </div>
                        <br></br>
                        <br></br>
    
                        <div className="grid_popup">
                            <div className="grid_item">
                                <div>จำนวนรวม :</div>
                                <div style={{ fontWeight: "bold" }}>{selectedData[0].remain}</div>
                            </div>
                        </div>
                        <br></br>
    
                        <Link to="/RequisitionFormPage"><Button type="primary"> ส่งคำขอเพิ่มพัสดุ </Button></Link>
                        <div className="pad2"></div>
                        <Link to="/ItemeditPage"><Button>แก้ไขข้อมูล</Button></Link>
    
    
    
                    </div>
    
    
                </div>
    
    
    
            </div>
        )
    
}

export default Item;