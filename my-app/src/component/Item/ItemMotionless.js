import React, { useState } from 'react';
import { Table } from 'antd';
import { Select } from 'antd';
import { Input, AutoComplete } from 'antd';
// import { UserOutlined } from '@ant-design/icons';
import tape from '../Img/tape1n.jpg';
import tapered1n from '../Img/tapered1n.jpg';
import tapered2n from '../Img/tapered2n.jpg';
import tapeblack1n from '../Img/tapeblack1n.jpg';
import tapewhite from '../Img/tapewhite.jpg';
import clips from '../Img/clips.jpg';
import lq from '../Img/liq.png';



const columns = [
    {
        title: 'รายการ',
        dataIndex: 'name',
        key: 'name',
    },
    {
        title: 'ประเภท',
        dataIndex: 'category',
        key: 'category',
    },
    {
        title: 'คงเหลือ',
        dataIndex: 'amount',
        key: 'amount',
    },
    {
        title: 'หน่วยวัด',
        dataIndex: 'unit',
        key: 'unit',
    },
    {
        title: 'วันที่ออกล่าสุด',
        dataIndex: 'lastRequisitionDate',
        key: 'lastRequisitionDate',
    }
];

const dataSource = [
    {
        key: '1',
        name: 'เทปผ้าติดสันหนังสือ 1 นิ้ว (สีน้ำเงิน)',
        src: tape,
        year: '2564',
        category: 'วัสดุสำนักงาน',
        amount: 10,
        unit: 'ม้วน',
        lastRequisitionDate: '3 มกราคม 2563'
    },

    {
        key: '2',
        name: 'กระดาษกาว 1 นิ้ว (สีขาว)',
        src: tapewhite,
        year: '2564',
        category: 'วัสดุสำนักงาน',
        amount: 12,
        unit: 'ม้วน',
        lastRequisitionDate: '25 มกราคม 2563'
    },

    {
        key: '3',
        name: 'ลวดเสียบกระดาษ',
        src: clips,
        year: '2563',
        category: 'วัสดุสำนักงาน',
        amount: 20,
        unit: 'กล่อง',
        lastRequisitionDate: '13 มกราคม 2563'
    },

    {
        key: '4',
        name: 'น้ำยาลบคำผิด (แบบแท่ง)',
        src: lq,
        year: '2562',
        category: 'วัสดุสำนักงาน',
        amount: 25,
        unit: 'อัน',
        lastRequisitionDate: '6 มกราคม 2563'
    },


];


function ItemMotionless() {
    const [selecteditem, setSelecteditem] = useState(dataSource)
    function handleChange(value) {
        console.log(`selected ${value}`);
        const filter = dataSource.filter(selecteditem => selecteditem.year === value);
        setSelecteditem(filter)
        // console.log(filter);
    }

    const dataSource2 = selecteditem.map((value) => {
        return {
            key: value.key,
            name: <div className="table_text">
                <img style={{ width: 40 }} src={value.src} />
                <div className="pad1">
                    {value.name}
                </div>
            </div>,
            category: value.category,
            amount: value.amount,
            unit: value.unit,
            lastRequisitionDate: value.lastRequisitionDate
            
        }
    })

    

    
    return (

        <div className="Appmiw">
            <div className="Head1">พัสดุไม่เคลื่อนไหว</div>
            <div className="Fillter">
                <div className="Reg_text2"> ปี :</div>
                <div className="pad2"></div>
                <Select defaultValue="2564" size="large" style={{ width: 180 }} onChange={handleChange}>

                    <option value="2564">2564</option>
                    <option value="2563">2563</option>
                    <option value="2562">2562</option>
                    <option value="2561">2561</option>
                    <option value="2560">2560</option>
                </Select>
            </div>
            <Table dataSource={dataSource2} columns={columns} />



        </div>
    )
}

export default ItemMotionless;