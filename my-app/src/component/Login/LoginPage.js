import React, { useState } from 'react';
import ReactDOM from 'react-dom';
import { Form, Input, Button, Checkbox } from 'antd';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import Axios from 'axios';


const Demo = () => {
  // const onFinish = (values: any) => {
  //   console.log('Success:', values);
  // };

  // const onFinishFailed = (errorInfo: any) => {
  //   console.log('Failed:', errorInfo);
  // };

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");


  const login = () => {

    // fetch("http://localhost:3000/login", {
    //   method: 'POST',
    //   body: JSON.stringify({
    //     Email: email,
    //     Password: password,
    //   }),
    //   headers: {
    //     'Accept': 'application/json',
    //     'Content-type': 'application/json; charset=UTF-8'
    //   }

    // })
    //   .then(res => res.json())
    //   .then(res => console.log(res));

    // fetch("http://localhost:3000/login", {
    //   Email: email,
    //   Password: password,
    // }).then((respose) => {
    //   console.log(respose);
    // })
  }


  return (
    <Form
      name="basic"
      initialValues={{ remember: true }}
      // onFinish={onFinish}
      // onFinishFailed={onFinishFailed}
      autoComplete="off"
    >
      <Form.Item
        name="email"
        rules={[{ required: true }]}
      >
        <Input size="large" style={{ width: 400 }} placeholder="@staff.tu.ac.th"
          onChange={(e) => {
            setEmail(e.target.vaue)
          }}
        />
      </Form.Item>
      <br></br>


      <Form.Item
        name="password"
        rules={[{ required: true, message: 'Please input your password!' }]}
      >
        <Input.Password size="large" style={{ width: 400 }} type="password" placeholder="รหัสผ่าน"
          onChange={(e) => {
            setPassword(e.target.vaue)
          }}
        />
      </Form.Item>
      <br></br>


      <Form.Item name="remember" valuePropName="checked" >
        <Checkbox>ลงชื่อเข้าใช้เสมอ</Checkbox>
        <a className="login-form-forgot" href="https://accounts.tu.ac.th/Login.aspx"> ลืมรหัสผ่าน</a>
      </Form.Item>
      <br></br>


      <Form.Item >
      <Link to="/Dashboard">
        <Button size="large" style={{ width: 400 }} type="primary" htmlType="Login" onClick={login}>
          Submit
        </Button>
        </Link>
      </Form.Item>
    </Form>
  );
};

function Login() {

  return (
    <div className="Applogin">
      <div className="login_decor">
        <svg id="Group_16016" data-name="Group 16016" xmlns="http://www.w3.org/2000/svg" width="163.371" height="400" viewBox="0 0 163.371 400">
          <path id="Path_1" data-name="Path 1" d="M456.811,475.311,340.775,499.769a12.937,12.937,0,0,1-15.31-9.98l-19.912-94.465a12.937,12.937,0,0,1,9.98-15.31l116.036-24.458a12.937,12.937,0,0,1,15.31,9.98L466.791,460A12.937,12.937,0,0,1,456.811,475.311ZM315.846,381.5a11.415,11.415,0,0,0-8.806,13.509l19.912,94.465a11.415,11.415,0,0,0,13.509,8.806L456.5,473.823a11.415,11.415,0,0,0,8.806-13.509l-19.912-94.465a11.415,11.415,0,0,0-13.509-8.806Z" transform="translate(-305.275 -233.332)" fill="#3f3d56" />
          <path id="Path_414" data-name="Path 414" d="M441.392,436.55l-89.376,18.839a2.076,2.076,0,0,1-2.455-1.416,2,2,0,0,1,1.519-2.474l90.427-19.061C443.835,433.75,443.1,436.19,441.392,436.55Z" transform="translate(-315.88 -251.838)" fill="#ccc" />
          <path id="Path_415" data-name="Path 415" d="M444.241,450.067l-89.376,18.839a2.076,2.076,0,0,1-2.455-1.416,2,2,0,0,1,1.519-2.474l90.427-19.06c2.328,1.312,1.592,3.752-.116,4.111Z" transform="translate(-316.564 -255.08)" fill="#ccc" />
          <path id="Path_416" data-name="Path 416" d="M447.088,463.575l-89.376,18.839A2.076,2.076,0,0,1,355.258,481a2,2,0,0,1,1.519-2.474L447.2,459.463C449.532,460.775,448.8,463.215,447.088,463.575Z" transform="translate(-317.246 -258.319)" fill="#ccc" />
          <path id="Path_417" data-name="Path 417" d="M449.937,477.091,360.561,495.93a2.076,2.076,0,0,1-2.455-1.416,2,2,0,0,1,1.519-2.474l90.427-19.06C452.381,474.291,451.645,476.731,449.937,477.091Z" transform="translate(-317.93 -261.561)" fill="#ccc" />
          <path id="Path_418" data-name="Path 418" d="M452.785,490.6l-89.376,18.839a2.076,2.076,0,0,1-2.455-1.416,2,2,0,0,1,1.519-2.474L452.9,486.488c2.328,1.312,1.592,3.752-.116,4.112Z" transform="translate(-318.613 -264.801)" fill="#ccc" />
          <path id="Path_2" data-name="Path 2" d="M483.286,428.743a7.991,7.991,0,0,0-.52,1.148l-36.589,9.08-6.365-6.585-11.193,8.307,10.135,12.753a6.081,6.081,0,0,0,7.061,1.846l39.7-16.224a7.98,7.98,0,1,0-2.232-10.325Z" transform="translate(-334.857 -249.973)" fill="#ffb8b8" />
          <circle id="Ellipse_22" data-name="Ellipse 22" cx="4.916" cy="4.916" r="4.916" transform="translate(115.033 0)" fill="#f2f2f2" />
          <circle id="Ellipse_23" data-name="Ellipse 23" cx="4.916" cy="4.916" r="4.916" transform="translate(115.033 19.591)" fill="#f2f2f2" />
          <circle id="Ellipse_24" data-name="Ellipse 24" cx="4.916" cy="4.916" r="4.916" transform="translate(115.033 39.183)" fill="#f2f2f2" />
          <circle id="Ellipse_25" data-name="Ellipse 25" cx="4.916" cy="4.916" r="4.916" transform="translate(115.033 58.775)" fill="#f2f2f2" />
          <path id="Path_411" data-name="Path 411" d="M442.658,395.321,384.1,407.665a2.076,2.076,0,0,1-2.455-1.416,2,2,0,0,1,1.519-2.474l59.612-12.565c2.328,1.312,1.592,3.752-.116,4.111Z" transform="translate(-323.574 -241.95)" fill="#ccc" />
          <path id="Path_412" data-name="Path 412" d="M445.506,408.834l-58.562,12.344a2.076,2.076,0,0,1-2.455-1.416,2,2,0,0,1,1.519-2.474l59.612-12.565c2.328,1.312,1.592,3.752-.116,4.111Z" transform="translate(-324.257 -245.191)" fill="#ccc" />
          <path id="Path_413" data-name="Path 413" d="M364.811,429.516l-17.873,3.767a2.252,2.252,0,0,1-2.665-1.737l-4.446-21.091a2.252,2.252,0,0,1,1.737-2.665l17.873-3.767a2.252,2.252,0,0,1,2.665,1.737l4.446,21.091A2.251,2.251,0,0,1,364.811,429.516Z" transform="translate(-313.55 -245.011)" fill="#ccc" />
          <path id="Path_3" data-name="Path 3" d="M332.866,509.621a8.166,8.166,0,0,0,4.849-11.545l25.18-69.986-17.487-3.1L326.156,494.76a8.21,8.21,0,0,0,6.711,14.862Z" transform="translate(-309.33 -250.052)" fill="#ffb8b8" />
          <path id="Path_4" data-name="Path 4" d="M365.344,434.556l-17.265-6.626a3.662,3.662,0,0,1-1.877-5.217l9.057-16.059A10.169,10.169,0,1,1,374.225,414l-4,17.938a3.662,3.662,0,0,1-4.886,2.621Z" transform="translate(-314.978 -244.095)" fill="#32a05f" />
          <path id="Path_5" data-name="Path 5" d="M191.493,519.834h9.319l4.434-35.947H191.491Z" transform="translate(-102.156 -128.868)" fill="#ffb8b8" />
          <path id="Path_6" data-name="Path 6" d="M419.671,705.173h18.354a11.7,11.7,0,0,1,11.7,11.7v.38h-30.05Z" transform="translate(-332.711 -317.249)" fill="#2f2e41" />
          <path id="Path_7" data-name="Path 7" d="M122.493,519.834h9.319l4.434-35.947H122.491Z" transform="translate(-85.607 -128.868)" fill="#ffb8b8" />
          <path id="Path_8" data-name="Path 8" d="M350.671,705.173h18.354a11.7,11.7,0,0,1,11.7,11.7v.38h-30.05Z" transform="translate(-316.163 -317.249)" fill="#2f2e41" />
          <path id="Path_9" data-name="Path 9" d="M361.85,650.567H350.057a3.421,3.421,0,0,1-3.417-3.583l5.6-120.358.353-.01,56.951-1.485L420.69,645.558a3.421,3.421,0,0,1-3.133,3.733l-13.247,1.059a3.422,3.422,0,0,1-3.624-2.723L383.742,561.9a1.105,1.105,0,0,0-1.117-.912h0a1.106,1.106,0,0,0-1.115.921l-16.3,85.9A3.428,3.428,0,0,1,361.85,650.567Z" transform="translate(-315.195 -274.069)" fill="#2f2e41" />
          <circle id="Ellipse_1" data-name="Ellipse 1" cx="18.67" cy="18.67" r="18.67" transform="translate(49.589 107.871)" fill="#ffb8b8" />
          <path id="Path_10" data-name="Path 10" d="M378.394,509.207c-8.9,0-18.039-2.339-25.616-9.255a3.46,3.46,0,0,1-1.117-2.926c.811-7.287,4.745-44.911,2.53-70.044a30.421,30.421,0,0,1,9.848-25.317,30.1,30.1,0,0,1,25.817-7.475h0c.233.042.465.085.7.13A30.242,30.242,0,0,1,414.712,426.5c-1.715,22.547-3.252,53.044-.838,70.291a3.415,3.415,0,0,1-1.836,3.525C406.546,503.088,392.786,509.207,378.394,509.207Z" transform="translate(-316.396 -242.547)" fill="#32a05f" />
          <path id="Path_11" data-name="Path 11" d="M417.539,436.528a3.657,3.657,0,0,1-1.6-2.593L413.661,415.7a10.169,10.169,0,1,1,19.577-5.51l7.492,16.846a3.661,3.661,0,0,1-2.364,5.015l-17.816,4.958a3.658,3.658,0,0,1-3.011-.48Z" transform="translate(-331.178 -244.723)" fill="#32a05f" />
          <path id="Path_12" data-name="Path 12" d="M380.788,372.012a13.377,13.377,0,0,1-5.167-.935c-.723-.279-1.474-.508-2.2-.787-6.375-2.463-10.575-9.253-10.725-16.085s3.388-13.465,8.642-17.835,12.06-6.614,18.875-7.133c7.341-.559,15.606,1.3,19.708,7.416a6.439,6.439,0,0,1,1.208,5.566,3.562,3.562,0,0,1-1,1.542c-1.832,1.63-3.658.4-5.525.3-2.567-.149-4.872,1.929-5.7,4.363a13.147,13.147,0,0,0,.205,7.6,18.971,18.971,0,0,1,.956,4.619,4.643,4.643,0,0,1-1.924,4.089c-1.6.969-3.71.408-5.315-.56s-2.992-2.314-4.725-3.026-4.062-.554-5.071,1.025a5.608,5.608,0,0,0-.641,1.851c-.9,4.121-.7,3.878-1.6,8Z" transform="translate(-319.047 -227.064)" fill="#2f2e41" />
          <circle id="Ellipse_2" data-name="Ellipse 2" cx="4.916" cy="4.916" r="4.916" transform="translate(12.411 0)" fill="#f2f2f2" />
          <circle id="Ellipse_3" data-name="Ellipse 3" cx="4.916" cy="4.916" r="4.916" transform="translate(12.411 19.591)" fill="#f2f2f2" />
          <circle id="Ellipse_4" data-name="Ellipse 4" cx="4.916" cy="4.916" r="4.916" transform="translate(12.411 39.183)" fill="#f2f2f2" />
          <circle id="Ellipse_5" data-name="Ellipse 5" cx="4.916" cy="4.916" r="4.916" transform="translate(12.411 58.775)" fill="#f2f2f2" />
          <circle id="Ellipse_6" data-name="Ellipse 6" cx="4.916" cy="4.916" r="4.916" transform="translate(32.936 0)" fill="#f2f2f2" />
          <circle id="Ellipse_7" data-name="Ellipse 7" cx="4.916" cy="4.916" r="4.916" transform="translate(32.936 19.591)" fill="#f2f2f2" />
          <circle id="Ellipse_8" data-name="Ellipse 8" cx="4.916" cy="4.916" r="4.916" transform="translate(32.936 39.183)" fill="#f2f2f2" />
          <circle id="Ellipse_9" data-name="Ellipse 9" cx="4.916" cy="4.916" r="4.916" transform="translate(32.936 58.775)" fill="#f2f2f2" />
          <circle id="Ellipse_10" data-name="Ellipse 10" cx="4.916" cy="4.916" r="4.916" transform="translate(53.46 0)" fill="#f2f2f2" />
          <circle id="Ellipse_11" data-name="Ellipse 11" cx="4.916" cy="4.916" r="4.916" transform="translate(53.46 19.591)" fill="#f2f2f2" />
          <circle id="Ellipse_12" data-name="Ellipse 12" cx="4.916" cy="4.916" r="4.916" transform="translate(53.46 39.183)" fill="#f2f2f2" />
          <circle id="Ellipse_13" data-name="Ellipse 13" cx="4.916" cy="4.916" r="4.916" transform="translate(53.46 58.775)" fill="#f2f2f2" />
          <circle id="Ellipse_14" data-name="Ellipse 14" cx="4.916" cy="4.916" r="4.916" transform="translate(73.984 0)" fill="#f2f2f2" />
          <circle id="Ellipse_15" data-name="Ellipse 15" cx="4.916" cy="4.916" r="4.916" transform="translate(73.984 19.591)" fill="#f2f2f2" />
          <circle id="Ellipse_16" data-name="Ellipse 16" cx="4.916" cy="4.916" r="4.916" transform="translate(73.984 39.183)" fill="#f2f2f2" />
          <circle id="Ellipse_17" data-name="Ellipse 17" cx="4.916" cy="4.916" r="4.916" transform="translate(73.984 58.775)" fill="#f2f2f2" />
          <circle id="Ellipse_18" data-name="Ellipse 18" cx="4.916" cy="4.916" r="4.916" transform="translate(94.509 0)" fill="#f2f2f2" />
          <circle id="Ellipse_19" data-name="Ellipse 19" cx="4.916" cy="4.916" r="4.916" transform="translate(94.509 19.591)" fill="#f2f2f2" />
          <circle id="Ellipse_20" data-name="Ellipse 20" cx="4.916" cy="4.916" r="4.916" transform="translate(94.509 39.183)" fill="#f2f2f2" />
          <circle id="Ellipse_21" data-name="Ellipse 21" cx="4.916" cy="4.916" r="4.916" transform="translate(94.509 58.775)" fill="#f2f2f2" />
        </svg>

        <div className="login_decor2">
          <svg xmlns="http://www.w3.org/2000/svg" width="316.432" height="358.83" viewBox="0 0 316.432 358.83">
            <g id="Group_16069" data-name="Group 16069" transform="translate(-893.568 58)">
              <g id="Group_16014" data-name="Group 16014" transform="translate(893.568 -58)">
                <path id="Path_13" data-name="Path 13" d="M685.605,313.76H566.657A12.977,12.977,0,0,1,553.695,300.8V203.962A12.977,12.977,0,0,1,566.657,191H685.605a12.977,12.977,0,0,1,12.962,12.962V300.8a12.977,12.977,0,0,1-12.962,12.962ZM566.657,192.525a11.45,11.45,0,0,0-11.437,11.437V300.8a11.45,11.45,0,0,0,11.437,11.437H685.605A11.45,11.45,0,0,0,697.043,300.8V203.962a11.45,11.45,0,0,0-11.437-11.437Z" transform="translate(-553.695 -181.088)" fill="#3f3d56" />
                <path id="Path_411" data-name="Path 411" d="M689.6,227.888H629.572a2.082,2.082,0,0,1-2.116-1.9,2.006,2.006,0,0,1,2-2.114h61.108c2.013,1.769.786,4.011-.964,4.011Z" transform="translate(-571.213 -188.896)" fill="#ccc" />
                <path id="Path_412" data-name="Path 412" d="M689.6,241.7H629.572a2.082,2.082,0,0,1-2.116-1.9,2.006,2.006,0,0,1,2-2.114h61.108c2.013,1.769.786,4.011-.964,4.011Z" transform="translate(-571.213 -192.177)" fill="#ccc" />
                <path id="Path_413" data-name="Path 413" d="M606.8,242.671H588.481a2.258,2.258,0,0,1-2.256-2.256v-21.62a2.259,2.259,0,0,1,2.256-2.256H606.8a2.259,2.259,0,0,1,2.256,2.256v21.62A2.259,2.259,0,0,1,606.8,242.671Z" transform="translate(-561.421 -187.153)" fill="#3f3d56" />
                <path id="Path_414" data-name="Path 414" d="M679.535,269.974H587.916a2.082,2.082,0,0,1-2.116-1.9,2.006,2.006,0,0,1,2-2.114h92.7C682.512,267.732,681.286,269.974,679.535,269.974Z" transform="translate(-561.319 -198.892)" fill="#ccc" />
                <path id="Path_415" data-name="Path 415" d="M679.535,283.788H587.916a2.082,2.082,0,0,1-2.116-1.9,2.006,2.006,0,0,1,2-2.114h92.7C682.512,281.546,681.286,283.788,679.535,283.788Z" transform="translate(-561.319 -202.174)" fill="#ccc" />
                <path id="Path_416" data-name="Path 416" d="M679.535,297.593H587.916a2.082,2.082,0,0,1-2.116-1.9,2.006,2.006,0,0,1,2-2.114h92.7C682.512,295.351,681.286,297.593,679.535,297.593Z" transform="translate(-561.319 -205.452)" fill="#ccc" />
                <path id="Path_417" data-name="Path 417" d="M679.535,311.406H587.916a2.082,2.082,0,0,1-2.116-1.9,2.006,2.006,0,0,1,2-2.114h92.7C682.512,309.164,681.286,311.406,679.535,311.406Z" transform="translate(-561.319 -208.733)" fill="#ccc" />
                <path id="Path_418" data-name="Path 418" d="M679.535,325.212H587.916a2.082,2.082,0,0,1-2.116-1.9,2.006,2.006,0,0,1,2-2.114h92.7C682.512,322.97,681.286,325.212,679.535,325.212Z" transform="translate(-561.319 -212.012)" fill="#ccc" />
                <path id="Path_14" data-name="Path 14" d="M670.606,178a3.817,3.817,0,0,1,3.813,3.812v12.2a3.817,3.817,0,0,1-3.812,3.812h-67.1a3.817,3.817,0,0,1-3.812-3.812v-12.2A3.817,3.817,0,0,1,603.507,178" transform="translate(-564.62 -178)" fill="#32a05f" />
              </g>
              <g id="Group_16015" data-name="Group 16015" transform="translate(1065.127 85.551)">
                <path id="Path_15" data-name="Path 15" d="M910.605,470.76H791.657A12.977,12.977,0,0,1,778.695,457.8V360.962A12.977,12.977,0,0,1,791.657,348H910.605a12.977,12.977,0,0,1,12.962,12.962V457.8A12.977,12.977,0,0,1,910.605,470.76ZM791.657,349.525a11.45,11.45,0,0,0-11.437,11.437V457.8a11.45,11.45,0,0,0,11.437,11.437H910.605A11.45,11.45,0,0,0,922.042,457.8V360.962a11.45,11.45,0,0,0-11.437-11.437Z" transform="translate(-778.695 -338.087)" fill="#3f3d56" />
                <path id="Path_411-2" data-name="Path 411" d="M914.6,384.888H854.572a2.082,2.082,0,0,1-2.116-1.9,2.006,2.006,0,0,1,2-2.114h61.108c2.013,1.769.786,4.011-.964,4.011Z" transform="translate(-796.213 -345.896)" fill="#3f3d56" />
                <path id="Path_412-2" data-name="Path 412" d="M914.6,398.7H854.572a2.082,2.082,0,0,1-2.116-1.9,2.006,2.006,0,0,1,2-2.114h61.108c2.013,1.769.786,4.011-.964,4.011Z" transform="translate(-796.213 -349.176)" fill="#3f3d56" />
                <path id="Path_413-2" data-name="Path 413" d="M831.8,399.671H813.482a2.258,2.258,0,0,1-2.256-2.256v-21.62a2.258,2.258,0,0,1,2.256-2.256H831.8a2.259,2.259,0,0,1,2.256,2.256v21.62a2.258,2.258,0,0,1-2.256,2.256Z" transform="translate(-786.421 -344.153)" fill="#ccc" />
                <path id="Path_413-3" data-name="Path 413" d="M926.8,479.521H908.482a2.259,2.259,0,0,1-2.256-2.256v-12.47a2.259,2.259,0,0,1,2.256-2.256H926.8a2.259,2.259,0,0,1,2.256,2.256v12.471A2.259,2.259,0,0,1,926.8,479.521Z" transform="translate(-808.985 -365.292)" fill="#3f3d56" />
                <path id="Path_414-2" data-name="Path 414" d="M904.535,426.974H812.916a2.082,2.082,0,0,1-2.116-1.9,2.006,2.006,0,0,1,2-2.114h92.7C907.512,424.732,906.286,426.974,904.535,426.974Z" transform="translate(-786.319 -355.892)" fill="#ccc" />
                <path id="Path_415-2" data-name="Path 415" d="M904.535,440.788H812.916a2.082,2.082,0,0,1-2.116-1.9,2.006,2.006,0,0,1,2-2.114h92.7C907.512,438.546,906.286,440.788,904.535,440.788Z" transform="translate(-786.319 -359.173)" fill="#ccc" />
                <path id="Path_416-2" data-name="Path 416" d="M904.535,454.593H812.916a2.082,2.082,0,0,1-2.116-1.9,2.006,2.006,0,0,1,2-2.114h92.7C907.512,452.351,906.286,454.593,904.535,454.593Z" transform="translate(-786.319 -362.452)" fill="#ccc" />
                <path id="Path_417-2" data-name="Path 417" d="M856.5,468.407H812.916a2.082,2.082,0,0,1-2.116-1.9,2.006,2.006,0,0,1,2-2.114h44.66C859.476,466.164,858.249,468.407,856.5,468.407Z" transform="translate(-786.319 -365.733)" fill="#ccc" />
                <path id="Path_418-2" data-name="Path 418" d="M856.5,482.212H812.916a2.082,2.082,0,0,1-2.116-1.9,2.006,2.006,0,0,1,2-2.114h44.66C859.476,479.97,858.249,482.212,856.5,482.212Z" transform="translate(-786.319 -369.011)" fill="#ccc" />
                <path id="Path_16" data-name="Path 16" d="M895.606,354.825h-67.1a3.817,3.817,0,0,1-3.812-3.812v-12.2A3.817,3.817,0,0,1,828.507,335h67.1a3.817,3.817,0,0,1,3.813,3.812v12.2a3.817,3.817,0,0,1-3.812,3.812Z" transform="translate(-789.62 -335)" fill="#32a05f" />
                <ellipse id="Ellipse_26" data-name="Ellipse 26" cx="4.931" cy="4.931" rx="4.931" ry="4.931" transform="translate(36.529 166.114)" fill="#f2f2f2" />
                <ellipse id="Ellipse_27" data-name="Ellipse 27" cx="4.931" cy="4.931" rx="4.931" ry="4.931" transform="translate(36.529 185.766)" fill="#f2f2f2" />
                <ellipse id="Ellipse_28" data-name="Ellipse 28" cx="4.931" cy="4.931" rx="4.931" ry="4.931" transform="translate(36.529 205.417)" fill="#f2f2f2" />
                <ellipse id="Ellipse_29" data-name="Ellipse 29" cx="4.931" cy="4.931" rx="4.931" ry="4.931" transform="translate(57.116 166.114)" fill="#f2f2f2" />
                <ellipse id="Ellipse_30" data-name="Ellipse 30" cx="4.931" cy="4.931" rx="4.931" ry="4.931" transform="translate(57.116 185.766)" fill="#f2f2f2" />
                <ellipse id="Ellipse_31" data-name="Ellipse 31" cx="4.931" cy="4.931" rx="4.931" ry="4.931" transform="translate(57.116 205.417)" fill="#f2f2f2" />
                <ellipse id="Ellipse_32" data-name="Ellipse 32" cx="4.931" cy="4.931" rx="4.931" ry="4.931" transform="translate(77.703 166.114)" fill="#f2f2f2" />
                <ellipse id="Ellipse_33" data-name="Ellipse 33" cx="4.931" cy="4.931" rx="4.931" ry="4.931" transform="translate(77.703 185.766)" fill="#f2f2f2" />
                <ellipse id="Ellipse_34" data-name="Ellipse 34" cx="4.931" cy="4.931" rx="4.931" ry="4.931" transform="translate(77.703 205.417)" fill="#f2f2f2" />
                <ellipse id="Ellipse_35" data-name="Ellipse 35" cx="4.931" cy="4.931" rx="4.931" ry="4.931" transform="translate(98.29 166.114)" fill="#f2f2f2" />
                <ellipse id="Ellipse_36" data-name="Ellipse 36" cx="4.931" cy="4.931" rx="4.931" ry="4.931" transform="translate(98.29 185.766)" fill="#f2f2f2" />
                <ellipse id="Ellipse_37" data-name="Ellipse 37" cx="4.931" cy="4.931" rx="4.931" ry="4.931" transform="translate(98.29 205.417)" fill="#f2f2f2" />
              </g>
            </g>
          </svg>

        </div>
      </div>


      <div className="font">
        <div className="Head3">เข้าสู่ระบบ</div>
        <div className="login_grid">
          <div></div>
          {/* <NormalLoginForm /> */}
          <Demo />
          <div></div>
        </div>

      </div>

    </div>




  );

}

export default Login;
