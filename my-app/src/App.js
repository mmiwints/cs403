
// import './App.css';
import './component/miwpart/App.css';
import React from "react";

import { BrowserRouter as Router, Route, Link, Switch } from 'react-router-dom'
import Dashboard from './component/DashBoard/HomePage';
import Navbar from "./component/Navbar/NavbarPage";
import ItemList from "./component/Item/ItemList";
import Itemout from "./component/Item/ItemOutofStock";
import ItemAout from "./component/Item/AlmostOut";
import Itemless from "./component/Item/ItemMotionless";
import ItemDe from "./component/Item/ItemDeteriorate";
import Item from "./component/Item/item";
import Anitem from "./component/Item/Anitemdeteriorate";
import Apprize from "./component/Item/apprizeitemdeter";
import savewithdraw from "./component/Item/SaveDatawithdraw";
import savereceive from "./component/Item/SaveDataReceive";
import Login from './component/Login/LoginPage';
import Test from './component/miwpart/test'


//miwpart


import Request from "./component/miwpart/Request";
import RecivePage from "./component/miwpart/Recive"
import RequisitionPage from "./component/miwpart/Requisition"
import RequestRecPage from "./component/miwpart/RequestRec"
import RequestEditPage from "./component/miwpart/RequestEdit"
import ReciveRecPage from "./component/miwpart/ReciveRec"
import RequestFormPage from "./component/miwpart/RequestForm"
import ReciveFormPage from "./component/miwpart/ReciveForm"
import RequisitionFormPage from "./component/miwpart/RequisitionForm"
import RequisitionRecPage from "./component/miwpart/RequisitionRec"
import ReportFormPage from "./component/miwpart/ReportForm"
import AddnewItemPage from "./component/miwpart/AddnewItem"
import ItemeditPage from "./component/miwpart/Itemedit"
import AddnewAccountPage from "./component/miwpart/AddnewAccount"
import AccountManagePage from "./component/miwpart/AccountManage"
import AccountEditPage from "./component/miwpart/AccountEdit"
import ReportPDFPage from "./component/miwpart/ReportPDF"
import FiscalRequisitionPage from "./component/miwpart/FiscalRequisition"
import ApprovePage from "./component/miwpart/FiscalApprove"
import StatusPage from "./component/miwpart/FiscalStatus"
import FiscalSuccessPage from "./component/miwpart/FiscalSuccess"
import ReciveEditPage from "./component/miwpart/ReciveEdit"
import ReportEditPage from "./component/miwpart/ReportEdit"

//fiscal Officer

import NavbarOF from "./FiscalOfficer/navbarOF";
import DashBoradOF from "./FiscalOfficer/dashboardOF";
import RequistionOF from "./FiscalOfficer/requistionOF";
import RequistionConfirm from "./FiscalOfficer/requistionConfirm";
import RequistionUpdate from "./FiscalOfficer/requistionUpdate";
// import Layout from 'antd/lib/layout/layout';

import { Layout, Menu } from 'antd';
import { useContext, useState, Component, useEffect } from 'react';

const { Header, Content, Footer, Sider } = Layout;

window.sessionStorage.setItem('isLogin',false)

function App() {
  return (
    
    

    <Router>
      <Switch>
      <Route path='/login' component={Login} />
      <div>
      <Sider
        style={{
          overflow: 'auto',
          height: '100vh',
          position: 'fixed',
          left: 0,
          backgroundColor: 'white',

        }}
        width="270"
      ><Navbar /></Sider>
      <Layout className="site-layout" style={{ marginLeft: 270 }}>
        <Content style={{ overflow: 'initial', backgroundColor: 'white' }}>
          <Route exact path="/Dashboard">
            <Dashboard />
          </Route>

          <Route exact path="/itemList">
            <ItemList />
          </Route>

          <Route exact path="/itemout">
            <Itemout />
          </Route>

          <Route exact path="/itemAlmostout">
            <ItemAout />
          </Route>

          <Route exact path="/itemMotionless">
            <Itemless />
          </Route>

          <Route exact path="/itemDeteriorate">
            <ItemDe />
          </Route>

          <Route path='/NavbarOF' component={NavbarOF} />
          <Route path='/anitemDeteriorate' component={Anitem} />
          <Route path='/apprize' component={Apprize} />
          <Route path='/savedatawithdraw' component={savewithdraw} />
          <Route path='/savedatareceive' component={savereceive} />




          <Route exact path="/RequestPage/" component={Request} />
          <Route path="/RecivePage" component={RecivePage} />
          <Route path="/RequisitionPage" component={RequisitionPage} />
          <Route path="/item/:key" component={Item} />
          <Route path="/RequestRecPage/:key" component={RequestRecPage} />
          <Route path="/RequestEditPage" component={RequestEditPage} />
          <Route path="/ReciveRecPage" component={ReciveRecPage} />
          <Route path="/RequestFormPage" component={RequestFormPage} />
          <Route path="/ReciveForm" component={ReciveFormPage} />
          <Route path="/RequisitionFormPage" component={RequisitionFormPage} />
          <Route path="/RequisitionRecPage" component={RequisitionRecPage} />
          <Route path="/ReportFormPage" component={ReportFormPage} />
          <Route path="/ReportEditPage" component={ReportEditPage} />
          <Route path="/AddnewItemPage" component={AddnewItemPage} />
          <Route path="/ItemeditPage" component={ItemeditPage} />
          <Route path="/AddnewAccountPage" component={AddnewAccountPage} />
          <Route path="/AccountManagePage" component={AccountManagePage} />
          <Route path="/AccountEditPage" component={AccountEditPage} />
          <Route path="/ReportPDFPage" component={ReportPDFPage} />
          <Route path="/ReciveEditPage" component={ReciveEditPage} />
          




          {/*เจ้าหน้าที่การคลัง*/}

          <Route path="/FiscalRequisitionPage" component={FiscalRequisitionPage} />
          <Route path="/ApprovePage" component={ApprovePage} />
          <Route path="/StatusPage" component={StatusPage} />
          <Route path="/FiscalSuccessPage" component={FiscalSuccessPage} />
          <Route path="/5" component={Test} />


          <Route path='/2' exact component={DashBoradOF} />
          {/* <Route path='/RequistionFiscalOfficer' exact component={RequistionOF} /> */}
          {/* <Route path='/RequistionConfirmFiscalOfficer' exact component={RequistionConfirm} />
          <Route path='/RequistionUpdateFiscalOfficer' exact component={RequistionUpdate} /> */}


        </Content>
      </Layout>
     
      </div>

    </Switch>
      
     


      
      

      
      
      
    </Router>
    







  );
}

export default App;
