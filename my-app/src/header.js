import React from 'react';
// import './App.css';
import './component/miwpart/App.css';
import { BrowserRouter as Router, Route, Link, Switch } from 'react-router-dom'
//fiscal Officer

import NavbarOF from "./FiscalOfficer/navbarOF";
import DashBoradOF from "./FiscalOfficer/dashboardOF";
import RequistionOF from "./FiscalOfficer/requistionOF";
import RequistionConfirm from "./FiscalOfficer/requistionConfirm";
import RequistionUpdate from "./FiscalOfficer/requistionUpdate";

function header() {
return(
  <Router>

    <div className="AppOF">
      
   
        <NavbarOF/>
        <Route exact path="/DashBoradFiscalOfficer">
          <DashBoradOF />
        </Route>
        <Route path='/DashBoradFiscalOfficer' exact component={DashBoradOF} /> 
        <Route path='/RequistionFiscalOfficer' exact component={RequistionOF} /> 
        <Route path='/RequistionConfirmFiscalOfficer' exact component={RequistionConfirm} /> 
        <Route path='/RequistionUpdateFiscalOfficer' exact component={RequistionUpdate} /> 
     


    </div>

  </Router>
  )

}
export default header;